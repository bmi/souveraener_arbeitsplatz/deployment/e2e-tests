:: SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
:: SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
:: SPDX-License-Identifier: Apache-2.0

@echo off

:: VIRTUALENV=$(command -v virtualenv)
SET VENV=venv
python -m venv %VENV%
call "%VENV%/Scripts/activate.bat"

set user_importer_path=git_clone/user-import

:: use this for debugging:
:: pytest -s tests/ --headed --config config.ini -v "."
:: Parameter n for parallel execution.
pytest -n 0 "." --config config.ini --order-dependencies --dist=loadfile --alluredir=allure_result %*
