# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page
from tests.config.config import Config
from tests.steps import steps_ox_contacts
import allure
import pytest

@allure.epic("Smoke")
@allure.feature("OX", "Contacts")
@allure.story("User", "UI")
@allure.title("OXcontacts: General availability")
@pytest.mark.user("user")
def test_ox_contacts_general_availability(logged_in_page: Page, url_portal: str):
    steps_ox_contacts.step_open_contacts_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_contacts.step_check_contacts_side_panel_available(page=logged_in_page)
    steps_ox_contacts.step_check_ox_top_left_bar(page=logged_in_page)
    steps_ox_contacts.step_check_ox_search_bar(page=logged_in_page)
    steps_ox_contacts.step_check_ox_top_right_bar(page=logged_in_page)

@allure.epic("Smoke")
@allure.feature("OX", "Contacts")
@allure.story("User", "UI")
@allure.title("OXcontacts: Check Collected Addresses available")
@pytest.mark.user("user")
def test_ox_contacts_page_collected_addresses_available(logged_in_page: Page, url_portal: str):
    steps_ox_contacts.step_open_contacts_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_contacts.step_check_contacts_collected_addresses_available(page=logged_in_page)

@allure.epic("Smoke")
@allure.feature("OX", "Contacts")
@allure.story("User", "Create contact")
@allure.title("OXcontacts: Create new contact")
@pytest.mark.user("user")
@pytest.mark.dependency(name="create_contact", scope="session")
@pytest.mark.xdist_group("contact")
def test_ox_contacts_create_contact(logged_in_page: Page, url_portal: str, run_id: str, config: Config):
    first_name: str = "NewContact"
    last_name: str = run_id
    
    steps_ox_contacts.step_open_contacts_page(page=logged_in_page, url_portal=url_portal)
    folder_id: str = steps_ox_contacts.step_click_collected_addresses(page=logged_in_page)
    steps_ox_contacts.step_click_add_contact(page=logged_in_page)
    steps_ox_contacts.step_fill_first_and_last_name(page=logged_in_page, first_name=first_name, last_name=last_name)
    steps_ox_contacts.step_save_contact(page=logged_in_page)
    steps_ox_contacts.step_check_contact_exists(page=logged_in_page, first_name=first_name, last_name=last_name)
    
    config.save("NewContactFirstname", first_name)
    config.save("NewContactLastname", last_name)
    config.save("NewContactFolderId", folder_id)

@allure.epic("Smoke")
@allure.feature("OX", "Contacts")
@allure.story("User", "Edit contact")
@allure.title("OXcontact: Edit contact")
@pytest.mark.user("user")
@pytest.mark.dependency(name="edit_contact", depends=["create_contact"], scope="session")
@pytest.mark.xdist_group("contact")
def test_ox_contacts_edit_contact(logged_in_page: Page, url_portal: str, config: Config):
    first_name: str = config.load("NewContactFirstname")
    last_name: str = config.load("NewContactLastname")
    assert first_name is not None and last_name is not None, "No contact created"
    email: str = "user@mail.io"
    
    steps_ox_contacts.step_open_contacts_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_contacts.step_click_collected_addresses(page=logged_in_page)
    steps_ox_contacts.step_check_contact_exists(page=logged_in_page, first_name=first_name, last_name=last_name)
    steps_ox_contacts.step_click_contact_list_item(page=logged_in_page, first_name=first_name, last_name=last_name)
    steps_ox_contacts.step_check_contact_name_in_main_window(page=logged_in_page, first_name=first_name, last_name=last_name)
    steps_ox_contacts.step_click_edit_contact(page=logged_in_page)
    steps_ox_contacts.step_check_edit_contact_modal(page=logged_in_page)
    steps_ox_contacts.step_fill_email_in_edit_contact_modal(page=logged_in_page, email_address=email)
    steps_ox_contacts.step_save_edit_contact_modal(page=logged_in_page)
    steps_ox_contacts.step_check_main_window_email_address(page=logged_in_page, email_address=email)
    
    config.save("NewContactEmail", email)

@allure.epic("Smoke")
@allure.feature("OX", "Contacts")
@allure.story("User", "Create contact", "Delete contact")
@allure.title("OXcontacts: Create and delete contact")
@pytest.mark.user("user")
def test_ox_contacts_create_delete_contact(logged_in_page: Page, url_portal: str, run_id: str):
    first_name: str = "TempContact"
    last_name: str = run_id
    
    steps_ox_contacts.step_open_contacts_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_contacts.step_click_collected_addresses(page=logged_in_page)
    steps_ox_contacts.step_click_add_contact(page=logged_in_page)
    steps_ox_contacts.step_fill_first_and_last_name(page=logged_in_page, first_name=first_name, last_name=last_name)
    steps_ox_contacts.step_save_contact(page=logged_in_page)
    steps_ox_contacts.step_check_contact_exists(page=logged_in_page, first_name=first_name, last_name=last_name)
    steps_ox_contacts.step_click_contact_list_item(page=logged_in_page, first_name=first_name, last_name=last_name)
    steps_ox_contacts.step_click_delete_contact(page=logged_in_page)
    steps_ox_contacts.step_check_confirmation_modal(page=logged_in_page)
    steps_ox_contacts.step_click_confirm_delete_contact(page=logged_in_page)
    steps_ox_contacts.step_check_contact_not_exists(page=logged_in_page, first_name=first_name, last_name=last_name)