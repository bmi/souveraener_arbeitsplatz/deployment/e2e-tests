# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

import datetime
import pathlib, shutil

import dateutil
import pandas as pd

import pytest
import requests
import yaml
from playwright.sync_api import Page, Browser, BrowserContext
from typing import Generator

from tests.config.config import Config
from tests.steps import steps_login, steps_ox, steps_nextcloud, steps_element, steps_admin_ox_resources, steps_xwiki, \
    steps_openproject

"""
File to host all the functions for populating a demo instance of openDesk.

The logic to actually create all the different data in the different openDesk modules is still
missing as for now. Could be constructed of the existing `steps` from `tests/steps`, or they could
be more generic functions to be used in the real test cases as well.
"""

# some of these could maybe become env variables?
# TODO: to be adjusted with merge into main (of demo data repo)
INPUT_FILES_URL = 'https://gitlab.opencode.de/bmi/opendesk/tooling/demo-data/-/raw/structured_and_compatible_input_data/'
TIMEZONE_CODE = 'Europe/Berlin' # TODO: could maybe be site-wide variable?
DOWNLOAD_CHUNK_SIZE = 32 * 1024  # for now only used to download blobs
ELEMENT_KEYS_DIR = 'element_keys'

test_user = 'helga.henkel'
test_user2 = 'aleksandra.berlin'
test_user3 = 'imre.hill'

class __User:
    def __init__(self, first_name: str, last_name: str, full_name: str, user_name: str, browser_context: BrowserContext | None):
        self.first_name = first_name
        self.last_name = last_name
        self.full_name = full_name
        self.user_name = user_name
        self.browser_context = browser_context


# could maybe be moved to utils
def __import_yaml_from_url(url: str) -> dict:
    """
    Helper function to load YAML file from URL.
    Fails if URL is not valid or reachable, or if there is no valid YAML there.

    :param url: URL that leads directly to the YAML content

    :return: a dictionary with the loaded YAML
    """
    try:
        response = requests.get(url, allow_redirects=True)
        content = response.content.decode('utf-8')
        return yaml.safe_load(content)
    except:
        pytest.fail(f'Import of necessary data from {url} failed')


# could maybe be moved to utils
def __get_dates_of_current_week() -> dict[str: datetime.date]:
    """
    Helper function to return dates for all working days of current week (at time of execution)

    :return: dictionary with short form of weekday as key and date objects as value
    """
    current_day = datetime.date.today()
    if current_day.weekday() > 4: # if current day is on the weekend (after day 4, which is Friday)
        current_day += datetime.timedelta(days=2) # skip ahead two days to get into next week
    current_week = {}
    for i in range(0 - current_day.weekday(), 5 - current_day.weekday()):
        day_i = current_day + datetime.timedelta(days=i)
        current_week.update({day_i.strftime('%a'): day_i})

    return current_week


@pytest.fixture(scope='session')
def temp_folder() -> Generator[pathlib.Path, None, None]:
    """
    Creates a folder in /tmp to temporarily host all the files that shall be uploaded to the
    demo instance, as well as the .ods file with the demo users.

    :return: fixture with path object of the temporary folder
    """
    temp_folder = pathlib.Path('/tmp/demo-instance-files')
    temp_folder.mkdir(parents=True, exist_ok=True)
    yield temp_folder

    shutil.rmtree(temp_folder) # runs after tests have finished


def __download_file_if_not_exists(temp_folder: pathlib.Path, file_name: str) -> pathlib.Path:
    """
    Helper function for downloading demo documents from the demo-data repo, if they don't exist
    already in the specified local folder.

    :param temp_folder: local folder to store demo documents during demo instance creation
    :param file_name: relative path of the file to download and store under

    :return: complete path to file after its download
    """
    file_path = temp_folder / file_name
    if not file_path.exists():
        file_path.parent.mkdir(exist_ok=True, parents=True)
        url = f'{INPUT_FILES_URL}/{file_name}?inline=false'
        response = requests.get(url, stream=True)
        if response.ok:
            with open(file_path, 'wb') as file:
                file.writelines(response.iter_content(DOWNLOAD_CHUNK_SIZE))
        else:
            raise Exception(f'No file under {url}')
    return file_path


@pytest.fixture(scope='session')
def project_manager(
        temp_folder: pathlib.Path, users_file_name: str = 'users.ods'
) -> Generator[str, None, None]:
    """
    Extracts username of project manager from users file,
    using LDAP groups attribute "Projektleitung".

    :param temp_folder: fixture of temporary folder to download users file to
    :param users_file_name: string with file name or relative path of users .ods file

    :return: fixture with project manager's username
    """
    users_file = __download_file_if_not_exists(temp_folder, file_name=users_file_name)
    users = pd.read_excel(users_file, engine='odf')

    project_manager_df = users[users['LDAP-Gruppen'].str.contains('Projektleitung')]
    project_manager = project_manager_df['Username'].to_string(index=False)
    yield project_manager


@pytest.fixture(scope='session')
def user_contexts(
        #config: Config, browser: Browser, browser_context_args: dict, url_portal: str,
        temp_folder: pathlib.Path, users_file_name: str = 'users.ods'
) -> Generator[dict[str, __User], None, None]:
    """
    Logs in all users from users file and creates dictionary with their browser contexts,
    so that they only have to be logged in once. The dictionary can then be used to open new
    pages for a specified user.

    :param temp_folder: fixture of temporary folder to access users file from
    :param users_file_name: string with file name or relative path of users .ods file

    :return: fixture of a dictionary with usernames and their respective browser contexts
    """
    users_file = __download_file_if_not_exists(temp_folder, file_name=users_file_name)
    users: pd.DataFrame = pd.read_excel(users_file, engine='odf')

    user_contexts: dict[str: __User] = {}
    for index, row in users.iterrows():
        user_name = row['Username']
        # browser_context = __log_in_user(
        #     config=config, browser=browser, browser_context_args=browser_context_args, url_portal=url_portal,
        #     user_name=user_name
        # )
        user_contexts[user_name] = __User(
            first_name=row['Vorname'], last_name=row['Nachname'], full_name=f"{row['Vorname']} {row['Nachname']}",
            user_name=user_name, browser_context=None
        )

    return user_contexts

@pytest.fixture(scope='session')
def get_user_context(browser: Browser, browser_context_args: dict, config: Config):
    def get_logged_in_context(user_name: str) -> BrowserContext:
        return __log_in_user(
            config=config, browser=browser, browser_context_args=browser_context_args, url_portal=config.url_portal, user_name=user_name
        )
    return get_logged_in_context

class UserPage:
    def __init__(self, user_name: str, get_user_context):
        self.user_name: str = user_name
        self.context: BrowserContext = get_user_context(user_name)
        self.page: Page = None

    def __enter__(self):
        self.page = self.context.new_page()
        return self.page

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.page:
            self.page.close()

@pytest.fixture(scope='session')
def get_user_page(get_user_context):

    def get_page(user_name: str):
        return UserPage(user_name, get_user_context)

    return get_page

def __log_in_user(
    config: Config, browser: Browser, browser_context_args: dict, url_portal: str, user_name: str
) -> BrowserContext:
    """
    Helper method to log in a specified user by username.

    :param user_name: username of user that shall be logged in

    :return: browser context of the now logged in user
    """
    context: BrowserContext = browser.new_context(**browser_context_args)
    page: Page = context.new_page()
    steps_login.step_open_page(page=page, url_portal=url_portal)
    steps_login.step_fill_login(page=page, username=user_name, password=config.default_passwd)
    steps_login.step_click_login(page=page)
    steps_login.step_check_login_successful(page=page)
    page.close()

    return context

@pytest.fixture(scope='session')
def initial_element_logins(user_contexts: dict[str, __User], get_user_page, url_portal: str, config: Config):
    element_keys_path = pathlib.Path(ELEMENT_KEYS_DIR)
    element_keys_path.mkdir(parents=True, exist_ok=True)

    for user_name in user_contexts.keys():
        with get_user_page(user_name) as logged_in_page:
            steps_element.step_open_page(page=logged_in_page, url_portal=url_portal)
            steps_element.step_click_user_menu(page=logged_in_page)
            steps_element.step_click_user_menu_security_button(page=logged_in_page)
            steps_element.step_click_setup_security_key_button(page=logged_in_page)
            steps_element.step_check_security_key_selected(page=logged_in_page)
            steps_element.step_click_next(page=logged_in_page)
            security_key: str = steps_element.step_download_key_to_permanent_dir(
                page=logged_in_page, directory=element_keys_path, username=user_name
            )
            steps_element.step_click_next(page=logged_in_page)
            steps_element.step_check_setup_successful(page=logged_in_page)
            steps_element.step_close_page(page=logged_in_page)
            config.save(f"{user_name}_ElementSecret", security_key)


def test_populate_data_shares(
        url_portal: str, base_domain: str, config: Config,
        get_user_page, temp_folder: pathlib.Path,
        data_share_data_path: str = 'input_demo_data/datashares_data.yaml',
):
    """
    Fill the demo users' Nextcloud data shares with the specified files from demo data YAML.

    :param base_domain: domain of demo instance
    :param data_share_data_path: relative path to where the data share demo data lies
    :param temp_folder: folder to temporarily store the to be uploaded files,
        is deleted in the end of this function
    """
    data_share_data = __import_yaml_from_url(INPUT_FILES_URL + data_share_data_path)
    domain = base_domain

    for user_name, folders in data_share_data.items():
        with get_user_page(user_name) as logged_in_page:
            steps_nextcloud.step_open_page(page=logged_in_page, url_portal=url_portal)
            for folder_name, attributes in folders.items():
                create_folder(logged_in_page=logged_in_page, folder_name=folder_name)

                for entity in attributes.get('shared_with', []):
                    readonly = entity.get('readonly', False)
                    share_with = None
                    mail_or_group = None
                    if group := entity.get('group', None):
                        share_with = group
                        mail_or_group = group
                    elif user := entity.get('user', None):
                        share_with = user
                        mail_or_group = f'{user}@{domain}'
                    share_folder(
                        logged_in_page=logged_in_page, folder_name=folder_name,
                        share_with=share_with, mail_or_group=mail_or_group, readonly=readonly
                    )

                upload_files_to_folder(
                    logged_in_page=logged_in_page, config=config, temp_folder=temp_folder,
                    files=attributes.get('files', []), folder_name=folder_name
                )

def create_folder(logged_in_page: Page, folder_name: str):
    steps_nextcloud.step_create_folder(page=logged_in_page, folder_name=folder_name)

def share_folder(
        logged_in_page: Page, folder_name: str, share_with: str, mail_or_group: str = None,
        readonly: bool = False
):
    print(f'Sharing folder {folder_name} with {share_with}, readonly={readonly}')
    steps_nextcloud.step_open_share_menu(page=logged_in_page, filename=folder_name)
    steps_nextcloud.step_select_share_mail(page=logged_in_page, share_input=share_with, mail=mail_or_group)
    if readonly:
        steps_nextcloud.step_sharing_readonly(page=logged_in_page)
    steps_nextcloud.step_click_save_share_button(page=logged_in_page)
    steps_nextcloud.step_intern_shared_entry_visible(page=logged_in_page, mail=mail_or_group)
    steps_nextcloud.step_close_sidebar_right(page=logged_in_page)

def upload_files_to_folder(
        logged_in_page: Page, config: Config, temp_folder: pathlib.Path,
        files: list[dict], folder_name: str
):
    print(f'Uploading files {files} to {folder_name}')
    steps_nextcloud.step_open_folder(page=logged_in_page, folder_name=folder_name)
    for file in files:
        file_name = file['name']
        parent_folder = 'documents'
        __download_file_if_not_exists(temp_folder, f'{parent_folder}/{file_name}')
        steps_nextcloud.step_upload_file(page=logged_in_page, folder=f'{temp_folder}/{parent_folder}', filename=file_name)
        steps_nextcloud.step_file_exists(page=logged_in_page, filename=file_name)
        print(logged_in_page.url)
        config.save(f'{folder_name}/{file_name}', logged_in_page.url)
    steps_nextcloud.step_go_to_home_folder(page=logged_in_page)

def test_populate_mailboxes(
        browser_context_args: dict, url_portal: str, base_domain: str,
        get_user_page, temp_folder: pathlib.Path,
        mail_list_data_path: str = 'input_demo_data/mails_sending_list.yaml',
        mail_folders_data_path: str = 'input_demo_data/mail_folders.yaml'
):
    """
    Send mails from specified demo users and move them to the folders as specified in the demo
    data for OX Mail.

    :param base_domain: domain of demo instance
    :param mail_list_data_path: relative path to where the list of mails to send is
    :param mail_folders_data_path: relative path to the data with the folder structure for each user
    """
    mailbox_data = __import_yaml_from_url(INPUT_FILES_URL + mail_folders_data_path)
    mails_for_sending = __import_yaml_from_url(INPUT_FILES_URL + mail_list_data_path)
    domain = base_domain
    
    for user_name, mails in mails_for_sending.items():
        with get_user_page(user_name) as logged_in_page:
            for mail in mails:
                try:
                    send_mail(
                        logged_in_page=logged_in_page, url_portal=url_portal, temp_folder=temp_folder,
                        sender=f'{mail["from"]}@{domain}',
                        recipients=[f'{username}@{domain}' for username in mail['to']],
                        subject=mail['subject'], body=mail['body'],
                        attachments=mail.get('attachments', None)
                    )
                except Exception as exc:
                    print(exc)

    for user_name, folder in mailbox_data.items():
        with get_user_page(user_name) as logged_in_page:
            for folder_name, mails in folder.items():
                for mail in mails:
                    if folder_name != 'Posteingang':
                        move_mail_to_folder(
                            logged_in_page=logged_in_page, url_portal=url_portal,
                            mail_sender=f'{mail["from"]}@{domain}', mail_subject=mail['subject'], folder_name=folder_name
                        )


def send_mail(
        logged_in_page: Page, url_portal: str, temp_folder: pathlib.Path,
        sender: str, recipients: list[str], subject: str, body: str, attachments: list[str] = None
):
    print(f'Sending mail with subject {subject} from {sender} to {recipients}')
    steps_ox.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_ox.step_click_new_mail(page=logged_in_page)
    for recipient in recipients:
        steps_ox.step_fill_internal_recipient(page=logged_in_page, username=recipient)
        steps_ox.step_click_recipient(page=logged_in_page, username=recipient)
    steps_ox.step_fill_subject(page=logged_in_page, subject=subject)
    steps_ox.step_fill_body(page=logged_in_page, body=body)

    if attachments:
        steps_ox.step_click_attachments_button(page=logged_in_page)
        for attachment in attachments:
            __download_file_if_not_exists(temp_folder, f'documents/{attachment}')
        steps_ox.step_mail_upload_attachments(page=logged_in_page, folder=f'{temp_folder}/documents', files=attachments)
    steps_ox.step_send_mail(page=logged_in_page)


def move_mail_to_folder(
        logged_in_page: Page, url_portal: str, mail_subject: str, mail_sender: str, folder_name: str,
):
    print(f'Moving mail to folder {folder_name}')
    steps_ox.step_open_page(page=logged_in_page, url_portal=url_portal)

    folder_exists = steps_ox.step_check_folder_exists(page=logged_in_page, folder_name=folder_name)
    if not folder_exists:
        steps_ox.step_click_create_folder(page=logged_in_page)
        steps_ox.step_create_new_folder(page=logged_in_page, folder=folder_name)

    try:
        mail = steps_ox.step_receive_mail_by_subject_and_sender(
            page = logged_in_page, subject=mail_subject, sender=mail_sender
        )
    except:
        print(f'Mail {mail_subject} from {mail_sender} not found.')
        return
    steps_ox.step_open_mail(page=logged_in_page, mail=mail)
    steps_ox.step_open_move_mail_modal(page=logged_in_page)
    steps_ox.step_move_opened_mail_to_folder(page=logged_in_page, folder_name=folder_name)


@pytest.mark.dependency("test_populate_data_shares")
def test_populate_private_chat_rooms(
        base_domain: str, url_portal: str, config: Config, user_contexts, get_user_page,
        temp_folder: pathlib.Path, chats_data_path: str = 'input_demo_data/private_chats_data.yaml'
):
    """
    Create specified private chat rooms and send messages from input YAML data.

    :param chats_data_path: relative path to chats input data
    :param temp_folder: folder to temporarily store files for attachments
    """
    chats_data = __import_yaml_from_url(INPUT_FILES_URL + chats_data_path)

    for chat_name, messages in chats_data.items():
        first_msg = messages.pop(0)
        creator = first_msg['from'] # test_user
        addressee = first_msg['to'] # test_user2
        create_private_chat(
            domain=base_domain, url_portal=url_portal, user_contexts=user_contexts, get_user_page=get_user_page,
            addressee_username=addressee, creator_username=creator,
            first_message=first_msg['content']
        )
        __send_messages_to_chat(
            get_user_page=get_user_page, url_portal=url_portal, config=config,
            messages=messages, temp_folder_for_attachments=temp_folder
        )

def create_private_chat(
        domain: str, url_portal: str, user_contexts: dict[str, __User], get_user_page,
        creator_username: str, addressee_username: str, first_message: str
):
    # for now only has the case of text message as first message, which is what is in the data
    # starting with attachment would be more complex and perhaps unnecessary for this use case
    print(f'Creating new private chat between {creator_username} and {addressee_username}')

    addressee_matrix_handle = f'@{addressee_username}:{domain}'
    addressee_user: __User = user_contexts.get(addressee_username)
    invitor_user: __User = user_contexts.get(creator_username)

    logged_in_page: Page = get_user_page(creator_username)
    steps_element.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_element.step_click_new_conversation_button(page=logged_in_page)
    steps_element.step_fill_invite_username(page=logged_in_page, username=addressee_matrix_handle)
    steps_element.step_select_user_by_username(page=logged_in_page, username=addressee_matrix_handle)
    steps_element.step_click_invite_user(page=logged_in_page)
    steps_element.step_check_room_header_name(
        page=logged_in_page, firstname=addressee_user.first_name, lastname=addressee_user.last_name
    )
    steps_element.step_send_message_in_room(page=logged_in_page, message=first_message)
    steps_element.step_check_message_in_chat(page=logged_in_page, message=first_message)
    steps_element.step_close_page(page=logged_in_page)

    logged_in_page_addressee: Page = get_user_page(
        user_name=addressee_username
    )
    steps_element.step_open_page(page=logged_in_page_addressee, url_portal=url_portal)
    steps_element.step_open_chat_invitation(
        page=logged_in_page_addressee, firstname=invitor_user.first_name, lastname=invitor_user.last_name
    )
    steps_element.step_click_join_room(page=logged_in_page_addressee)
    steps_element.step_close_page(page=logged_in_page_addressee)


@pytest.mark.dependency("test_populate_data_shares")
def test_populate_group_chat_rooms(
        url_portal: str, base_domain: str, config: Config, get_user_page,
        temp_folder: pathlib.Path, chats_data_path: str = 'input_demo_data/group_chats_data.yaml'
):
    """
    Create group chats and send messages specified in input YAML.

    :param base_domain: domain of demo instance
    :param chats_data_path: relative path to group chat data
    :param temp_folder: folder to temporarily store files for attachments
    """
    chats_data = __import_yaml_from_url(INPUT_FILES_URL + chats_data_path)
    domain = base_domain

    for group_name, attr in chats_data.items():
        group_members = attr['participants'] # [test_user, test_user2, test_user3]
        creator = group_members.pop(0)
        member_matrix_names = [f'@{name}:{domain}' for name in group_members]
        logged_in_page: Page = get_user_page(creator)
        create_group_chat(
            logged_in_page_creator=logged_in_page, url_portal=url_portal, get_user_page=get_user_page,
            name=group_name, description=attr['description'], member_usernames=group_members,
            member_matrix_handles=member_matrix_names
        )
        __send_messages_to_chat(
            get_user_page=get_user_page, url_portal=url_portal, config=config,
            messages=attr['messages'], temp_folder_for_attachments=temp_folder
        )

def create_group_chat(
        logged_in_page_creator: Page, url_portal: str, get_user_page,
        name: str, description: str, member_matrix_handles: list[str], member_usernames: list[str]
):
    print(f'Creating group chat {name} with members {member_usernames}')
    steps_element.step_open_page(page=logged_in_page_creator, url_portal=url_portal)
    steps_element.step_open_new_room_menu(page=logged_in_page_creator)
    steps_element.step_click_new_room(page=logged_in_page_creator)
    steps_element.step_fill_room_name(page=logged_in_page_creator, room_name=name)
    steps_element.step_fill_room_topic_description(page=logged_in_page_creator, description=description)
    steps_element.step_select_join_rule_invite(page=logged_in_page_creator)
    steps_element.step_click_create_room(page=logged_in_page_creator)
    steps_element.step_check_room_header(page=logged_in_page_creator, room_name=name)

    for matrix_handle in member_matrix_handles:
        steps_element.step_click_invite(page=logged_in_page_creator)
        steps_element.step_fill_invite_username(page=logged_in_page_creator, username=matrix_handle)
        steps_element.step_select_user_by_username(page=logged_in_page_creator, username=matrix_handle)
        steps_element.step_click_invite_user(page=logged_in_page_creator)
    steps_element.step_close_page(page=logged_in_page_creator)

    for username in member_usernames:
        logged_in_page_invitee = get_user_page(username)
        steps_element.step_open_page(page=logged_in_page_invitee, url_portal=url_portal)
        steps_element.step_open_room_invitation(page=logged_in_page_invitee, room_name=name)
        steps_element.step_click_join_room(page=logged_in_page_invitee)
        steps_element.step_check_room_header(page=logged_in_page_invitee, room_name=name)
        steps_element.step_close_page(page=logged_in_page_invitee)

def __send_messages_to_chat(
        get_user_page, url_portal: str, config: Config,
        messages: list[dict], temp_folder_for_attachments: pathlib.Path
):
    """
    Helper method to loop through chat messages and send them one by one.
    Can handle text, files, polls and poll choice events.

    :param url_portal:
    :param messages: list of messages in consistent format, extracted from chat input YAML files
    :param temp_folder_for_attachments: folder to temporarily store files for attachments
    """
    for message in messages:
        logged_in_page = get_user_page(message['from'])
        steps_element.step_open_page(page=logged_in_page, url_portal=url_portal)
        chat_name = message['to']
        steps_element.step_open_chat_room(page=logged_in_page, room_name=chat_name)

        msg_content = message.get('content', '')
        match message['type']:
            case 'text':
                if file_path_for_link := message.get('file_path_for_link', ''):
                    file_link = get_link_for_file(config=config, file_path_for_link=file_path_for_link)
                    if not file_link:
                        print(f"Couldn't get Nextcloud link for {file_path_for_link}")
                    else:
                        msg_content += file_link
                send_element_text_message(
                    logged_in_page=logged_in_page, chat_name=chat_name, text=msg_content
                )
            case 'attachment':
                file_path = __download_file_if_not_exists(
                    temp_folder=pathlib.Path(temp_folder_for_attachments),
                    file_name='documents/' + msg_content
                )
                send_element_attachment(logged_in_page=logged_in_page, chat_name=chat_name, file_path=file_path)
            case 'poll':
                send_element_poll(
                    logged_in_page=logged_in_page, chat_name=chat_name,
                    question=message['title'], options=message['options']
                )
            case 'poll_choice':
                participate_in_element_poll(
                    logged_in_page=logged_in_page, poll_title=message['poll_title'], answer=message['content']
                )

        steps_element.step_close_page(page=logged_in_page)

def send_element_text_message(logged_in_page: Page, chat_name: str, text: str):
    print(f'Sending text to {chat_name}: {text}')
    steps_element.step_send_message_in_room(page=logged_in_page, message=text)

def get_link_for_file(config: Config, file_path_for_link: str) -> str:
    return config.load(file_path_for_link)

def send_element_attachment(
        logged_in_page: Page, chat_name: str, file_path: pathlib.Path
):
    print(f'Sending file {file_path} to {chat_name}')
    steps_element.step_send_file_in_room(page=logged_in_page, file_path=file_path)
    steps_element.step_check_file_in_room(page=logged_in_page, filename=file_path.name)

def send_element_poll(
        logged_in_page: Page, chat_name: str, question: str, options: list[str]
):
    print(f'Sending poll to {chat_name}, for question: {question}')
    steps_element.step_open_other_room_events_composer(page=logged_in_page)
    steps_element.step_open_poll(page=logged_in_page)
    steps_element.step_fill_poll_question(page=logged_in_page, question=question)
    steps_element.step_fill_poll_choices(page=logged_in_page, choices=options)
    steps_element.step_click_create_poll(page=logged_in_page)

def participate_in_element_poll(
        logged_in_page: Page, poll_title: str, answer: str
):
    print(f'Choosing poll option {answer} in poll {poll_title}')
    steps_element.step_vote_poll(page=logged_in_page, question=poll_title, choice=answer)


@pytest.mark.user("udm_admin")
def test_populate_resources(
        logged_in_page: Page, url_portal: str, base_domain: str, config: Config,
        project_manager: str, resources_data_path: str = 'input_demo_data/resources_data.yaml'
):
    """
    Create LDAP resources to be used in calendar appointments.

    :param base_domain: domain of demo instance
    :param resources_data_path: relative path to resource input data
    """
    resources_data = __import_yaml_from_url(INPUT_FILES_URL + resources_data_path)
    domain = base_domain

    for resource_id, attr in resources_data.items():
        create_resource(
            logged_in_page, url_portal=url_portal, base_domain=base_domain, config=config,
            project_manager=project_manager,
            name=resource_id, display_name=attr['display_name'],
            resource_manager=f'{attr["resource_manager"]}@{domain}',
        )

def create_resource(
        logged_in_page: Page, url_portal: str, base_domain: str, config: Config, project_manager: str,
        name: str, display_name: str, resource_manager: str
):
    print(f'Creating resource {display_name}, managed by {resource_manager}')

    steps_admin_ox_resources.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_admin_ox_resources.step_click_add_object(page=logged_in_page)

    steps_admin_ox_resources.step_create_object(
        page=logged_in_page, name=name, resource_email=f"{name}@{base_domain}",
        resource_manager=project_manager, display_name=display_name
    )

    config.save("OXResourceName", name)


@pytest.mark.dependency(name='test_populate_resources')  # for reserving rooms and such
def test_populate_calendars(
        url_portal: str, base_domain: str,
        get_user_page, calendar_data_path: str = 'input_demo_data/calendar_data.yaml'
):
    """
    Create calendar appointments for all the demo users, as specified in input YAML file.
    Dates are calculated dynamically based on given weekday and current week, as of time of execution.
    Appointments usually have attendees, and attached meeting rooms, links or resources, depending
    on the type of meeting (physical, video-conference or collaboration session)

    :param base_domain: domain of demo instance
    :param calendar_data_path: relative path to calendar input data
    """
    calendar_data = __import_yaml_from_url(INPUT_FILES_URL + calendar_data_path)
    current_week = __get_dates_of_current_week()
    domain = base_domain

    for user_name, calendar in calendar_data.items():
        with get_user_page(user_name) as logged_in_page:
            for appointment in calendar:
                start = __calc_appointment_datetime(
                    current_week, appointment['weekday'], appointment['start']
                )
                end = __calc_appointment_datetime(
                    current_week, appointment['weekday'], appointment['end']
                )
                if appointment['location'] == 'generate_jitsi_link':
                    appointment['location'] = __generate_jitsi_meeting_link(appointment['title'], domain)

                create_appointment(
                    logged_in_page=logged_in_page, url_portal=url_portal,
                    title=appointment['title'], start=start, end=end,
                    attendees=[f'{name}@{domain}' for name in appointment.get('attendees', [])],
                    resources=[f'{name}@{domain}' for name in appointment.get('resources', [])],
                    location=appointment['location'],
                    create_conference=appointment.get('create_conference', None)
                )

def create_appointment(
        logged_in_page: Page, url_portal: str, title: str, start: datetime.datetime, end: datetime.datetime,
        attendees: list[str], resources: list[str], location: str, create_conference: bool
):
    print(f'Creating appointment {title} in {location},\n  starting at {start}, with {attendees} and {resources}')

    start_day = start.date().strftime('%d.%m.%Y')
    start_time = start.time().strftime('%H:%M')
    end_day = end.date().strftime('%d.%m.%Y')
    end_time = end.time().strftime('%H:%M')

    steps_ox.step_open_calendar_page(page=logged_in_page, url_portal=url_portal)
    steps_ox.step_click_new_appointment(page=logged_in_page)
    steps_ox.step_fill_title(page=logged_in_page, title=title)
    if create_conference:
        conference_link = steps_ox.step_click_enable_vc(page=logged_in_page)
        steps_ox.step_fill_location(page=logged_in_page, location=conference_link)
    else:
        steps_ox.step_fill_location(page=logged_in_page, location=location)
    steps_ox.step_fill_start_day(page=logged_in_page, day=start_day)
    steps_ox.step_fill_start_time(page=logged_in_page, time=start_time)
    steps_ox.step_fill_end_day(page=logged_in_page, day=end_day)
    steps_ox.step_fill_end_time(page=logged_in_page, time=end_time)

    for attendee in attendees:
        steps_ox.step_add_attendee(page=logged_in_page, attendee_mail=attendee)
    for resource in resources:
        steps_ox.step_add_attendee(page=logged_in_page, attendee_mail=resource)
    steps_ox.step_click_create_appointment(page=logged_in_page)

def __calc_appointment_datetime(
        current_week: dict[str, datetime.date], weekday: str, time: str
) -> datetime.datetime:
    """
    Construct appointment time from the given weekday and time.

    :param current_week: dictionary of weekday short forms and dates of the current week
    :param weekday: weekday of appointment in short form (Mon, Tue, Wed, Thu, Fri)
    :param time: time from calendar YAML file

    :return: date-time in current week and time in globally set timezone
    """
    timezone = dateutil.tz.gettz(TIMEZONE_CODE)

    return datetime.datetime.combine(
        current_week[weekday], datetime.datetime.strptime(time, '%H:%M').time(), tzinfo=timezone
    )

def __generate_jitsi_meeting_link(meeting_title: str, domain: str) -> str:
    """
    Helper method to create a meeting link with the meeting title in hyphenated format,
    so that Jitsi can reconstruct the meeting title correctly.

    :param meeting_title: meeting title from appointment data
    :param domain: domain of demo instance

    :return: the generated meeting link
    """
    return (
        f'https://meet.{domain}/{meeting_title}'
        .replace('(Videokonferenz)', '')
        .strip().replace(' ', '-')
    )


def test_populate_wiki(
        url_portal: str, get_user_page, project_manager: str,
        wiki_data_path: str = 'input_demo_data/wiki_data.yaml'
):
    """
    Create wiki pages as specified in demo data YAML file.
    There is no page hierarchy in the demo data, therefore the pages are all created at the same level.

    :param wiki_data_path: relative path to wiki input data
    """
    wiki_pages = __import_yaml_from_url(INPUT_FILES_URL + wiki_data_path)

    logged_in_page: Page = get_user_page(project_manager)
    for page_title, content in wiki_pages.items():
        create_wiki_page(
            logged_in_page=logged_in_page, url_portal=url_portal, title=page_title, content=content
        )

def create_wiki_page(logged_in_page: Page, url_portal: str, title: str, content: str, parent: str = None):
    # TODO: add logic for creating a wiki page with title and content, optionally under a specific super-page
    # (for the demo data, the parent page is not necessary, because there are no sub pages,
    #  adding it to make it a more generic function)
    print(f'Creating wiki page for {title}')
    steps_xwiki.step_open_page(page=logged_in_page, url_portal=url_portal)
    if title == "Startseite":
        steps_xwiki.step_click_edit_button(page=logged_in_page)
        steps_xwiki.step_focus_edit_page_start(page=logged_in_page)
    else:
        steps_xwiki.step_click_create_button(page=logged_in_page)
        steps_xwiki.step_new_page_fill_title(page=logged_in_page, title=title)
        steps_xwiki.step_new_page_click_create(page=logged_in_page)
    steps_xwiki.step_switch_to_source_edit_view(page=logged_in_page)
    steps_xwiki.step_edit_page_fill_content(page=logged_in_page, content=content)
    steps_xwiki.step_edit_page_click_save_and_view(page=logged_in_page)
    steps_xwiki.step_check_page_edit_successful(page=logged_in_page, title=title, content=content)


@pytest.mark.dependency(name='test_populate_data_shares')
def test_populate_projects(
        url_portal: str, base_domain: str,
        get_user_page, project_manager: str,
        projects_data_path: str = 'input_demo_data/projects_data.yaml'
):
    """
    Add demo work packages with appended files to the existing demo projects.

    :param base_domain: domain of demo instance
    :param projects_data_path: relative path to input demo data for openProject
    """
    projects_data = __import_yaml_from_url(INPUT_FILES_URL + projects_data_path)
    # TODO: create tasks, some with nextcloud file attachments, to assign to projects
    logged_in_page: Page = get_user_page(project_manager)
    __prepare_demo_project(logged_in_page=logged_in_page, url_portal=url_portal, project_name='Demo-Projekt')
    __prepare_demo_project(logged_in_page=logged_in_page, url_portal=url_portal, project_name='Scrum-Projekt')

def __prepare_demo_project(logged_in_page: Page, url_portal: str, project_name: str):
    """
    Helper function to add demo users to pre-existing demo projects,
    and to add Nextcloud as a file storage to the projects.

    :param logged_in_page:
    :param url_portal: 
    """
    steps_openproject.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_openproject.step_click_through_welcome_modals(page=logged_in_page)
    steps_openproject.step_open_project_select(page=logged_in_page)
    steps_openproject.step_open_project(page=logged_in_page, project_name=project_name)
    group_for_adding_all_users = 'managed-by-attribute-Projectmanagement'
    add_member_to_project(
        logged_in_page=logged_in_page, project_name=project_name, member=group_for_adding_all_users
    )
    add_nextcloud_to_project(logged_in_page=logged_in_page, project_name=project_name)

def add_member_to_project(logged_in_page: Page, project_name: str, member: str):
    print(f'Adding {member} to project {project_name}')
    steps_openproject.step_open_members_menu(page=logged_in_page)
    steps_openproject.step_click_add_member(page=logged_in_page)
    steps_openproject.step_fill_member(page=logged_in_page, name=member)
    steps_openproject.step_click_submit_member(page=logged_in_page)
    steps_openproject.step_leave_member_settings(page=logged_in_page)

def add_nextcloud_to_project(logged_in_page: Page, project_name: str):
    print(f'Adding nextcloud for files to project {project_name}')
    steps_openproject.step_open_files_menu(page=logged_in_page)
    steps_openproject.step_click_add_storage(page=logged_in_page)
    steps_openproject.step_click_submit_button(page=logged_in_page)
    steps_openproject.step_check_automatic_checked(page=logged_in_page)
    steps_openproject.step_click_submit_button(page=logged_in_page)
    steps_openproject.step_click_request_access(page=logged_in_page)
    steps_openproject.step_click_grant_access_login(page=logged_in_page)
    steps_openproject.step_click_grant_access(page=logged_in_page)
