# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

import pytest
from tests.config.config import Config
from playwright.sync_api import BrowserContext
from playwright._impl._page import Page as AsyncPage
from playwright._impl._connection import Channel
import json
import os

class ExportData:
    def __init__(self):
        self.test_cases: list = []
        self.current_steps: list = []
        self.contexts: dict = {}
        self.context_map: dict = {}
        self.page_map: dict = {}

@pytest.fixture(scope="session")
def export_data():
    yield ExportData()

@pytest.fixture(autouse=True, scope="session")
def init_test_export(config: Config, export_data: ExportData):
    new_inner_send = Channel.inner_send

    def inner_send(self, method: str, params, return_as_dict):
        object = self._object
        parent = object._parent
        page_name: str | None = None
        context_name: str | None = None
        
        if method == "newPage":
            context: BrowserContext = object
            
            if context not in export_data.context_map.keys():
                context_name = f"context{len(export_data.context_map)}"
                options = context._options
                export_data.contexts[context_name] = {
                    "pages": [],
                    "options": options
                }
                export_data.context_map[context] = context_name
            else:
                context_name = export_data.context_map[context]
        
        if isinstance(parent, AsyncPage):
            context = parent._browser_context
            
            if context not in export_data.context_map.keys():
                context_name = f"context{len(export_data.context_map)}"
                options = context._options
                export_data.contexts[context_name] = {
                    "pages": [],
                    "options": options
                }
                export_data.context_map[context] = context_name
            else:
                context_name = export_data.context_map[context]
            
            pages = export_data.contexts[context_name].get("pages", [])
            
            if parent not in export_data.page_map.keys():
                name = f"page{len(export_data.page_map)}"
                export_data.page_map[parent] = name
            
            page_name = export_data.page_map[parent]
            
            if not page_name in pages:
                pages.append(page_name)
            
            export_data.contexts[context_name]["pages"] = pages
        
        if method in ["expect", "goto", "click", "type", "waitForTimeout", "newPage", "press", "setInputFiles", "ownerFrame", "close", "keyboardPress", "keyboardType"]:
            export_data.current_steps.append({
                "page": page_name,
                "context": context_name,
                "action": {
                    method: params
                }
            })
        else:
            print("Method not implemented:", method)
        
        return new_inner_send(self, method, params, return_as_dict)
        
    Channel.inner_send = inner_send
    
@pytest.fixture(autouse=True, scope="function")
def test_report(request: pytest.FixtureRequest, config: Config, run_id: str, export_data: ExportData):
    export_data.page_map.clear()
    export_data.context_map.clear()
    export_data.current_steps.clear()
    export_data.contexts.clear()
    
    fixtures: dict = {
        name: request.getfixturevalue(name)
        for name in request.fixturenames if name != "test_report"
    }
    fixtures["base_domain"] = request.getfixturevalue("base_domain")
    fixtures = {
        key: value
        for key, value in fixtures.items() if value and (key not in ["worker", "base_url", "browser_name"])
    }
    str_fixtures = {
        key: value
        for key, value in fixtures.items() if isinstance(value, str)
    }
    for user in config.test_users:
        key = user.key.replace("-", "_")
        str_fixtures[f"{key}_username"] = user.username
        str_fixtures[f"{key}_firstname"] = user.firstname
        str_fixtures[f"{key}_lastname"] = user.lastname
        str_fixtures[f"{key}_email"] = user.email
        str_fixtures[f"{key}_password"] = user.password
    yield
    if export_folder := config.test_export_folder:
        if not request.node.rep_call.failed:
            export_data.test_cases.append({
                "testcase": request.node.name,
                "steps": export_data.current_steps,
                "contexts": export_data.contexts,
                "fixtures": str_fixtures
            })
        write_test_cases(run_id=run_id, export_folder=export_folder, test_cases=export_data.test_cases)
    
    export_data.page_map.clear()
    export_data.context_map.clear()
    export_data.current_steps.clear()
    export_data.contexts.clear()

def write_test_cases(run_id: str, export_folder: str, test_cases: list):
    os.makedirs(export_folder, exist_ok=True)
    
    json_str: str = json.dumps(test_cases, indent=3)
    
    with open(f"{export_folder}{os.sep}testcases-{run_id}.json", "w", encoding="utf-8") as f:
        f.write(json_str)