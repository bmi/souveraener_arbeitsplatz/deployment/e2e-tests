# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from functools import wraps
from typing import Callable
from playwright.sync_api import Page, BrowserContext
import allure
import os
import pytest
from time import sleep

def retry(trys: int = 10, retry_timeout: float = 5, on_fail=None, exception_types: list[type] = [Exception]):
    
    def func_wrapper(func):
        @wraps(func)
        def wrapper(*a, **p):
            uncaught_exception = None
            
            for current_try in range(trys):
                try:
                    try:
                        with allure.step(f"Try {current_try+1}:"):
                            val = func(*a, **p)
                        return val
                    except Exception as e:
                        caught = False
                        for exception_type in exception_types:
                            if isinstance(e, exception_type):
                                caught = True
                                break
                        if not caught:
                            uncaught_exception = e
                            break
                        if current_try < trys - 1:
                            if retry_timeout > 0:
                                with allure.step(f"Retry timeout: {retry_timeout} seconds..."):
                                    sleep(retry_timeout)
                            if on_fail and callable(on_fail):
                                try:
                                    with allure.step("On fail"):
                                        on_fail(*a, **p)
                                except: pass
                        raise e
                except: pass
            
            if uncaught_exception:
                raise uncaught_exception
            
            pytest.fail(f"Maximum amount of retries ({trys}) exceeded.")
        return wrapper
    
    return func_wrapper

def screenshot_step(func: Callable=None, *, timeout: int=None):
    if func is None:
        return lambda func: screenshot_step(func, timeout=timeout)
    
    @wraps(func)
    def wrapper(*params, **positional_params):
        before = os.environ["DO_SCREENSHOT_BEFORE_STEP"] == "yes"
        after = os.environ["DO_SCREENSHOT_AFTER_STEP"] == "yes"
        redirect = os.environ["DO_SCREENSHOT_REDIRECT_STEP"] == "yes"
        values = []
        values.extend(params)
        values.extend(positional_params.values())
        
        if before:
            for value in values:
                if isinstance(value, Page):
                    screenshot_page(value, f"before:{func.__name__}", timeout=timeout)

        return_value = None
        raised_exception = None
        try:
            return_value = func(*params, **positional_params)
        except Exception as e:
            raised_exception = e
        
        if redirect and return_value and isinstance(return_value, Page):
            screenshot_page(return_value, f"redirect:{func.__name__}", timeout=timeout)
        
        if after:
            for value in values:
                if isinstance(value, Page):
                    screenshot_page(value, f"after:{func.__name__}", timeout=timeout)
        
        if raised_exception:
            raise raised_exception
        
        return return_value
    
    return wrapper

def screenshot_context(context: BrowserContext, prefix: str | None = None):
    for page in context.pages:
        screenshot_page(page, prefix)

def screenshot_page(page: Page, prefix: str | None = None, timeout: float | None = None):
    try:
        if page.is_closed():
            return
        title = page.title()
        png_bytes = page.screenshot(timeout=timeout)
        allure.attach(
            png_bytes,
            name=("" if not prefix else f"{prefix} - ") + title,
            attachment_type=allure.attachment_type.PNG
        )
    except:
        pass
