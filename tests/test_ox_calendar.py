# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page
from tests.config.config import Config
from tests.steps import steps_ox_calendar
import allure
import pytest

@allure.epic("Smoke", "Load")
@allure.feature("OX", "Calendar")
@allure.story("User", "Create appointment")
@allure.title("OXcalendar: Create appointment")
@pytest.mark.user("component_admin")
def test_ox_create_appointment_entry(logged_in_page: Page, url_portal: str, run_id: str):
    title: str = f"appointment-{run_id}"
    
    steps_ox_calendar.step_open_calendar_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_calendar.step_click_new_appointment(page=logged_in_page)
    steps_ox_calendar.step_fill_title(page=logged_in_page, title=title)
    steps_ox_calendar.step_click_create_appointment(page=logged_in_page)
    steps_ox_calendar.step_check_appointment_exists(page=logged_in_page, title=title)

@allure.epic("Smoke")
@allure.feature("OX", "Calendar")
@allure.story("User", "Create appointment")
@allure.title("OXcalendar: Create appointment (With video conference)")
@pytest.mark.user("component_admin")
def test_ox_create_appointment_entry_vc(logged_in_page: Page, url_portal: str, run_id: str):
    title: str = f"appointmentvc-{run_id}"
    
    steps_ox_calendar.step_open_calendar_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_calendar.step_click_new_appointment(page=logged_in_page)
    steps_ox_calendar.step_fill_title(page=logged_in_page, title=title)
    steps_ox_calendar.step_click_enable_vc(page=logged_in_page)
    steps_ox_calendar.step_click_create_appointment(page=logged_in_page)
    steps_ox_calendar.step_check_appointment_exists(page=logged_in_page, title=title)

@allure.epic("Smoke")
@allure.feature("OX", "Calendar")
@allure.story("User", "Create appointment")
@allure.title("OXcalendar: Create appointment (Add attachment)")
@pytest.mark.files({"odp": 1})
@pytest.mark.user("component_admin")
def test_ox_create_appointment_with_attachment(logged_in_page: Page, url_portal: str, temp_files: tuple[str, list], run_id: str):
    temp_folder: str = temp_files[0]
    files: list = temp_files[1]
    title: str = f"appointment-files-{run_id}"
    
    steps_ox_calendar.step_open_calendar_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_calendar.step_click_new_appointment(page=logged_in_page)
    steps_ox_calendar.step_fill_title(page=logged_in_page, title=title)
    steps_ox_calendar.step_appointment_upload_attachments(page=logged_in_page, folder=temp_folder, files=files)
    steps_ox_calendar.step_appointment_attachments_visible(page=logged_in_page, files=files)
    steps_ox_calendar.step_click_create_appointment(page=logged_in_page)
    steps_ox_calendar.step_check_appointment_exists(page=logged_in_page, title=title)
    steps_ox_calendar.step_check_appointment_has_attachment(page=logged_in_page, title=title)
    
@allure.epic("Smoke")
@allure.feature("OX", "Calendar")
@allure.story("User", "Create appointment")
@allure.title("OXcalendar: Drag and drop appointment")
@pytest.mark.user("user")
def test_ox_drag_drop_appointment(logged_in_page: Page, url_portal: str, run_id: str):
    title: str = f"drag-drop-{run_id}"
    
    steps_ox_calendar.step_open_calendar_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_calendar.step_click_new_appointment(page=logged_in_page)
    steps_ox_calendar.step_fill_title(page=logged_in_page, title=title)
    # steps_ox_calendar.step_set_start_time(page=logged_in_page, hours=15, minutes=0) # Uncomment if moving into view does still fail 11PM
    steps_ox_calendar.step_click_create_appointment(page=logged_in_page)
    steps_ox_calendar.step_check_appointment_exists(page=logged_in_page, title=title)
    start_before: int = steps_ox_calendar.step_get_appointment_start_timestamp(page=logged_in_page, title=title)
    end_before: int = steps_ox_calendar.step_get_appointment_end_timestamp(page=logged_in_page, title=title)
    appointment_duration: int = end_before - start_before
    
    steps_ox_calendar.step_drag_drop_appointment(page=logged_in_page, title=title)
    start_after: int = steps_ox_calendar.step_get_appointment_start_timestamp(page=logged_in_page, title=title)
    steps_ox_calendar.step_check_appointment_timestamp(start_before=start_before, start_after=start_after, duration=appointment_duration)
    
@allure.epic("Smoke")
@allure.feature("OX", "Calendar")
@allure.story("User", "Create appointment")
@allure.title("OXcalendar: Create appointment (Invite participant)")
@pytest.mark.files({"odp": 1})
@pytest.mark.user("component_admin")
@pytest.mark.users_dependency(["user"])
@pytest.mark.xdist_group("ox_appointment_participant")
@pytest.mark.dependency(name="create_appointment_participant", scope="session")
def test_ox_create_appointment_with_participant(logged_in_page: Page, url_portal: str, user: Config.User, run_id: str, config: Config):
    title: str = f"appointment-part-{run_id}"
    email: str = user.email
    
    steps_ox_calendar.step_open_calendar_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_calendar.step_click_new_appointment(page=logged_in_page)
    steps_ox_calendar.step_fill_title(page=logged_in_page, title=title)
    steps_ox_calendar.step_fill_participant_or_resource(page=logged_in_page, name=email)
    steps_ox_calendar.step_select_participant(page=logged_in_page, email=email)
    steps_ox_calendar.step_click_create_appointment(page=logged_in_page)
    steps_ox_calendar.step_check_appointment_exists(page=logged_in_page, title=title)
    steps_ox_calendar.step_check_appointment_has_participant(page=logged_in_page, title=title)
    
    config.save("ParticipantAppointmentTitle", title)

@allure.epic("Smoke")
@allure.feature("OX", "Calendar")
@allure.story("User")
@allure.title("OXcalendar: Appointment invitation visible in participant calendar")
@pytest.mark.user("user")
@pytest.mark.dependency(name="appointment_invitation_visible", depends=["create_appointment_participant"], scope="session")
@pytest.mark.xdist_group("ox_appointment_participant")
def test_ox_appointment_invitation_visible_in_participant_calendar(logged_in_page: Page, url_portal: str, config: Config):
    title: str | None = config.load("ParticipantAppointmentTitle")
    assert title is not None
    
    steps_ox_calendar.step_open_calendar_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_calendar.step_check_appointment_exists(page=logged_in_page, title=title)
    steps_ox_calendar.step_check_appointment_has_participant(page=logged_in_page, title=title)
    steps_ox_calendar.step_check_appointment_needs_action(page=logged_in_page, title=title)

@allure.epic("Smoke")
@allure.feature("OX", "Calendar")
@allure.story("User")
@allure.title("OXcalendar: Appointment invitation status (Accept)")
@pytest.mark.users(["component_admin", "user"])
@pytest.mark.dependency(name="accept_appointment_invitation", depends=["appointment_invitation_visible"], scope="session")
@pytest.mark.xdist_group("ox_appointment_participant")
def test_ox_accept_appointment_invitation(logged_in_pages: list[Page], url_portal: str, user: Config.User, config: Config):
    title: str | None = config.load("ParticipantAppointmentTitle")
    assert title is not None
    
    email: str = user.email
    
    creator_page: Page = logged_in_pages[0]
    participant_page: Page = logged_in_pages[1]
    
    steps_ox_calendar.step_open_calendar_page(page=participant_page, url_portal=url_portal)
    steps_ox_calendar.step_check_appointment_exists(page=participant_page, title=title)
    steps_ox_calendar.step_check_appointment_has_participant(page=participant_page, title=title)
    steps_ox_calendar.step_open_appointment_details(page=participant_page, title=title)
    steps_ox_calendar.step_accept_appointment(page=participant_page)
    steps_ox_calendar.step_check_appointment_accepted_by_user(page=participant_page, email=email)
    steps_ox_calendar.step_close_appointment_details(page=participant_page)
    steps_ox_calendar.step_check_appointment_is_accepted(page=participant_page, title=title)
    
    steps_ox_calendar.step_open_calendar_page(page=creator_page, url_portal=url_portal)
    steps_ox_calendar.step_check_appointment_exists(page=creator_page, title=title)
    steps_ox_calendar.step_check_appointment_has_participant(page=creator_page, title=title)
    steps_ox_calendar.step_open_appointment_details(page=creator_page, title=title)
    steps_ox_calendar.step_check_appointment_accepted_by_user(page=creator_page, email=email)

@allure.epic("Smoke")
@allure.feature("OX", "Calendar")
@allure.story("User")
@allure.title("OXcalendar: Appointment invitation status (Decline)")
@pytest.mark.users(["component_admin", "user"])
@pytest.mark.dependency(name="decline_appointment_invitation", depends=["appointment_invitation_visible"], scope="session")
@pytest.mark.xdist_group("ox_appointment_participant")
def test_ox_decline_appointment_invitation(logged_in_pages: list[Page], url_portal: str, user: Config.User, config: Config):
    title: str | None = config.load("ParticipantAppointmentTitle")
    assert title is not None
    
    email: str = user.email
    
    creator_page: Page = logged_in_pages[0]
    participant_page: Page = logged_in_pages[1]
    
    steps_ox_calendar.step_open_calendar_page(page=participant_page, url_portal=url_portal)
    steps_ox_calendar.step_check_appointment_exists(page=participant_page, title=title)
    steps_ox_calendar.step_check_appointment_has_participant(page=participant_page, title=title)
    steps_ox_calendar.step_open_appointment_details(page=participant_page, title=title)
    steps_ox_calendar.step_decline_appointment(page=participant_page)
    steps_ox_calendar.step_check_appointment_declined_by_user(page=participant_page, email=email)
    steps_ox_calendar.step_close_appointment_details(page=participant_page)
    steps_ox_calendar.step_check_appointment_is_declined(page=participant_page, title=title)
    
    steps_ox_calendar.step_open_calendar_page(page=creator_page, url_portal=url_portal)
    steps_ox_calendar.step_check_appointment_exists(page=creator_page, title=title)
    steps_ox_calendar.step_check_appointment_has_participant(page=creator_page, title=title)
    steps_ox_calendar.step_open_appointment_details(page=creator_page, title=title)
    steps_ox_calendar.step_check_appointment_declined_by_user(page=creator_page, email=email)

@allure.epic("Smoke")
@allure.feature("OX", "Calendar")
@allure.story("User")
@allure.title("OXcalendar: Appointment invitation status (Tentative)")
@pytest.mark.users(["component_admin", "user"])
@pytest.mark.dependency(name="tentative_appointment_invitation", depends=["appointment_invitation_visible"], scope="session")
@pytest.mark.xdist_group("ox_appointment_participant")
def test_ox_tentative_appointment_invitation(logged_in_pages: list[Page], url_portal: str, user: Config.User, config: Config):
    title: str | None = config.load("ParticipantAppointmentTitle")
    assert title is not None
    
    email: str = user.email
    
    creator_page: Page = logged_in_pages[0]
    participant_page: Page = logged_in_pages[1]
    
    steps_ox_calendar.step_open_calendar_page(page=participant_page, url_portal=url_portal)
    steps_ox_calendar.step_check_appointment_exists(page=participant_page, title=title)
    steps_ox_calendar.step_check_appointment_has_participant(page=participant_page, title=title)
    steps_ox_calendar.step_open_appointment_details(page=participant_page, title=title)
    steps_ox_calendar.step_tentative_appointment(page=participant_page)
    steps_ox_calendar.step_check_appointment_tentative_by_user(page=participant_page, email=email)
    steps_ox_calendar.step_close_appointment_details(page=participant_page)
    steps_ox_calendar.step_check_appointment_is_tentative(page=participant_page, title=title)
    
    steps_ox_calendar.step_open_calendar_page(page=creator_page, url_portal=url_portal)
    steps_ox_calendar.step_check_appointment_exists(page=creator_page, title=title)
    steps_ox_calendar.step_check_appointment_has_participant(page=creator_page, title=title)
    steps_ox_calendar.step_open_appointment_details(page=creator_page, title=title)
    steps_ox_calendar.step_check_appointment_tentative_by_user(page=creator_page, email=email)
