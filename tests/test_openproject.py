# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page
from tests.config.config import Config
from tests.steps import steps_openproject, steps_ox_mail, steps_portal
import allure
import pytest

@allure.epic("Smoke")
@allure.feature("OpenProject")
@allure.story("User", "UI")
@allure.title("OpenProject: General availability")
@pytest.mark.dependency(name="check_openproject_site")
@pytest.mark.user("user")
def test_openproject_general_availability(logged_in_page: Page, url_portal: str):
    steps_openproject.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_openproject.step_click_through_welcome_modals(page=logged_in_page)
    steps_openproject.step_open_project_select(page=logged_in_page)
    steps_openproject.step_open_demo_project(page=logged_in_page)
    steps_openproject.step_check_openproject_header(page=logged_in_page)

@allure.epic("Smoke")
@allure.feature("OpenProject")
@allure.story("User", "File Storage")
@allure.title("OpenProject: Create new project")
@pytest.mark.dependency(name="create_project")
@pytest.mark.user("component_admin")
@pytest.mark.xdist_group("file_storage")
def test_openproject_create_project(logged_in_page: Page, url_portal: str, run_id: str, config: Config):
    project_name: str = f"project-{run_id}" 
    
    steps_openproject.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_openproject.step_click_through_welcome_modals(page=logged_in_page)
    steps_openproject.step_open_project_select(page=logged_in_page)
    steps_openproject.step_click_new_project(page=logged_in_page)
    
    steps_openproject.step_new_project_fill_name(page=logged_in_page, name=project_name)
    steps_openproject.step_new_project_unselect_parent_project(page=logged_in_page)
    steps_openproject.step_new_project_check_no_parent_selected(page=logged_in_page)
    steps_openproject.step_new_project_click_create(page=logged_in_page)
    steps_openproject.step_check_project_open(page=logged_in_page, project_name=project_name)
    
    config.save("ProjectName", project_name)

@allure.epic("Smoke")
@allure.feature("OpenProject")
@allure.story("User", "File Storage")
@allure.title("OpenProject: Add file storage to project")
@pytest.mark.dependency(name="create_file_storage", depends=["create_project"])
@pytest.mark.user("component_admin")
@pytest.mark.xdist_group("file_storage")
def test_openproject_create_file_storage(logged_in_page: Page, url_portal: str, config: Config):
    project_name: str | None = config.load("ProjectName")
    assert project_name is not None, "Project was not created"
    
    steps_openproject.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_openproject.step_click_through_welcome_modals(page=logged_in_page)
    steps_openproject.step_open_project_select(page=logged_in_page)
    steps_openproject.step_open_project(page=logged_in_page, project_name=project_name)
    
    steps_openproject.step_open_files_menu(page=logged_in_page)
    steps_openproject.step_click_add_storage(page=logged_in_page)
    steps_openproject.step_click_submit_button(page=logged_in_page)
    steps_openproject.step_check_automatic_checked(page=logged_in_page)
    steps_openproject.step_click_submit_button(page=logged_in_page)
    steps_openproject.step_grant_access_to_nextcloud(page=logged_in_page)
    steps_openproject.step_check_file_storage_exists(page=logged_in_page)
    
@allure.epic("Smoke")
@allure.feature("OpenProject")
@allure.story("User")
@allure.title("OpenProject: Create task (Via quick add)")
@pytest.mark.dependency(depends=["create_file_storage"])
@pytest.mark.user("component_admin")
@pytest.mark.files({".odt": 1})
@pytest.mark.xdist_group("file_storage")
def test_openproject_create_task_quick_add(logged_in_page: Page, url_portal: str, run_id: str, config: Config, temp_files: tuple[str, list]):
    project_name: str | None = config.load("ProjectName")
    assert project_name is not None, "Project was not created"
    task_subject: str = f"task-quick-add-{run_id}"
    folder, files = temp_files
    
    # Open OpenProject via PortalPage to trigger a bug
    steps_portal.step_open_page(page=logged_in_page, url_portal=url_portal)
    projects_page: Page = steps_portal.step_projects_tile_redirect(page=logged_in_page)
    steps_openproject.step_click_through_welcome_modals(page=projects_page)
    
    steps_openproject.step_click_quick_add(page=projects_page)
    steps_openproject.step_click_quick_add_task(page=projects_page)
    steps_openproject.step_check_edit_work_package_view(page=projects_page)
    steps_openproject.step_fill_work_package_subject(page=projects_page, subject=task_subject)
    steps_openproject.step_fill_work_package_project(page=projects_page, project_name=project_name)
    steps_openproject.step_select_work_package_project(page=projects_page, project_name=project_name)
    steps_openproject.step_upload_attachments(page=projects_page, folder=folder, files=files)
    steps_openproject.step_click_save(page=projects_page)
    steps_openproject.step_check_work_package_created(page=projects_page)
    steps_openproject.step_open_project_select(page=projects_page)
    steps_openproject.step_open_project(page=projects_page, project_name=project_name)
    steps_openproject.step_check_work_package_exists(page=projects_page, subject=task_subject)
    
    projects_page.close()

@allure.epic("Smoke")
@allure.feature("OpenProject")
@allure.story("User")
@allure.title("OpenProject: Create task")
@pytest.mark.dependency(depends=["create_file_storage"])
@pytest.mark.user("component_admin")
@pytest.mark.files({".odt": 1})
@pytest.mark.xdist_group("file_storage")
def test_openproject_create_task(logged_in_page: Page, url_portal: str, run_id: str, config: Config, temp_files: tuple[str, list]):
    project_name: str | None = config.load("ProjectName")
    assert project_name is not None, "Project was not created"
    task_subject: str = f"task-{run_id}"
    folder, files = temp_files
    
    steps_openproject.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_openproject.step_click_through_welcome_modals(page=logged_in_page)
    steps_openproject.step_open_project_select(page=logged_in_page)
    steps_openproject.step_open_project(page=logged_in_page, project_name=project_name)
    
    steps_openproject.step_open_work_packages(page=logged_in_page)
    steps_openproject.step_click_new_work_package(page=logged_in_page)
    steps_openproject.step_click_work_package_task(page=logged_in_page)
    steps_openproject.step_check_edit_work_package_view(page=logged_in_page)
    steps_openproject.step_fill_work_package_subject(page=logged_in_page, subject=task_subject)
    steps_openproject.step_upload_attachments(page=logged_in_page, folder=folder, files=files)
    steps_openproject.step_click_save(page=logged_in_page)
    steps_openproject.step_check_work_package_created(page=logged_in_page)
    steps_openproject.step_check_work_package_exists(page=logged_in_page, subject=task_subject)

@allure.epic("Smoke")
@allure.feature("OpenProject")
@allure.story("User")
@allure.title("OpenProject: File storage health check")
@pytest.mark.user("component_admin")
def test_openproject_file_storage_health_check(logged_in_page: Page, url_portal: str):
    steps_openproject.step_open_admin_page(page=logged_in_page, url_portal=url_portal)
    steps_openproject.step_expand_admin_files_settings(page=logged_in_page)
    steps_openproject.step_open_external_file_storages(page=logged_in_page)
    steps_openproject.step_open_nextcloud_storage(page=logged_in_page)
    steps_openproject.step_check_storage_health(page=logged_in_page)

@allure.epic("Smoke")
@allure.feature("OpenProject")
@allure.story("User")
@allure.title("OpenProject: LDAP health check")
@pytest.mark.user("component_admin")
def test_openproject_ldap_health_check(logged_in_page: Page, url_portal: str):
    ldap_entry_name: str = "opendesk"
    
    steps_openproject.step_open_admin_page(page=logged_in_page, url_portal=url_portal)
    steps_openproject.step_expand_admin_authentication_settings(page=logged_in_page)
    steps_openproject.step_open_ldap_sources(page=logged_in_page)
    steps_openproject.step_check_ldap_visible(page=logged_in_page, name=ldap_entry_name)
    steps_openproject.step_check_ldap_health(page=logged_in_page, name=ldap_entry_name)

@allure.epic("Smoke")
@allure.feature("OpenProject")
@allure.story("User")
@allure.title("OpenProject: Send test mail")
@pytest.mark.user("component_admin")
def test_openproject_send_test_mail(logged_in_page: Page, url_portal: str, base_domain: str):
    test_mail_subject: str = "openDesk Test"
    test_mail_sender: str = f"no-reply@{base_domain}"
    
    steps_openproject.step_open_admin_page(page=logged_in_page, url_portal=url_portal)
    steps_openproject.step_expand_admin_mail_and_notifications_settings(page=logged_in_page)
    steps_openproject.step_open_mail_notifications(page=logged_in_page)
    steps_openproject.step_send_test_mail(page=logged_in_page)
    steps_ox_mail.step_open_mail_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_mail.step_receive_mail(page=logged_in_page, contains_subject=test_mail_subject, sender=test_mail_sender)