# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page
from tests.config.config import Config
from tests.steps import steps_ox_calendar
import allure
import pytest

@allure.epic("Smoke", "Integration")
@allure.feature("OX", "Calendar")
@allure.story("OX Resource")
@allure.title("OXcalendar: Create appointment (Add resource)")
@pytest.mark.dependency(depends=["create_ox_resource"], scope="session")
@pytest.mark.user("component_admin")
@pytest.mark.xdist_group("ox_resource")
def test_ox_resource_available(logged_in_page: Page, url_portal: str, run_id: str, config: Config):
    ox_resource_name: str | None = config.load("OXResourceName")
    assert ox_resource_name != None
    title: str = f"appointment-resource-{run_id}"
    
    steps_ox_calendar.step_wait(page=logged_in_page, timeout=60000)
    steps_ox_calendar.step_open_calendar_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_calendar.step_click_new_appointment(page=logged_in_page)
    steps_ox_calendar.step_fill_title(page=logged_in_page, title=title)
    steps_ox_calendar.step_find_resource(page=logged_in_page, resource_name=ox_resource_name)
    steps_ox_calendar.step_select_resource(page=logged_in_page, resource_name=ox_resource_name)
    steps_ox_calendar.step_click_create_appointment(page=logged_in_page)
    steps_ox_calendar.step_check_appointment_exists(page=logged_in_page, title=title)
    steps_ox_calendar.step_check_appointment_has_participant(page=logged_in_page, title=title)

@allure.epic("Smoke", "Integration")
@allure.feature("OX", "Calendar")
@allure.story("User", "Create appointment")
@allure.title("OXcalendar: Create appointment (Nextcloud attachment)")
@pytest.mark.files({"odp": 1})
@pytest.mark.user("component_admin")
def test_ox_create_appointment_with_nc_attachment(logged_in_page: Page, url_portal: str, nextcloud_files: tuple[str, list], run_id: str):
    files: list = nextcloud_files[1]
    filename: str = files[0]
    title: str = f"appointment-nc-file-{run_id}"
    
    steps_ox_calendar.step_open_calendar_page(page=logged_in_page, url_portal=url_portal)
    steps_ox_calendar.step_click_new_appointment(page=logged_in_page)
    steps_ox_calendar.step_fill_title(page=logged_in_page, title=title)
    steps_ox_calendar.step_click_attachment_from_files(page=logged_in_page)
    steps_ox_calendar.step_select_file(page=logged_in_page, filename=filename)
    steps_ox_calendar.step_save_file_links(page=logged_in_page)
    steps_ox_calendar.step_check_save_success(page=logged_in_page)
    steps_ox_calendar.step_appointment_attachments_visible(page=logged_in_page, files=files)
    steps_ox_calendar.step_click_create_appointment(page=logged_in_page)
    steps_ox_calendar.step_check_appointment_exists(page=logged_in_page, title=title)
    steps_ox_calendar.step_check_appointment_has_attachment(page=logged_in_page, title=title)