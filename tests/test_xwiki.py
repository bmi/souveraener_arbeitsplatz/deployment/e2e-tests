# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page
from tests.steps import steps_portal, steps_xwiki
import allure
import pytest
import random

@allure.epic("Smoke")
@allure.feature("XWiki")
@allure.story("User", "UI")
@allure.title("XWiki: General availability")
@pytest.mark.dependency(name="check_xwiki_page")
@pytest.mark.user("user")
def test_check_xwiki_site(logged_in_page: Page, url_portal: str):
    steps_xwiki.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_xwiki.step_check_navbar_logo(page=logged_in_page)
    steps_xwiki.step_check_navbar_services(page=logged_in_page)
    steps_xwiki.step_check_navbar_search(page=logged_in_page)
    steps_xwiki.step_check_navbar_right_buttons(page=logged_in_page)
    steps_xwiki.step_check_navbar_drawer(page=logged_in_page)
    steps_xwiki.step_check_left_panels(page=logged_in_page)
    steps_xwiki.step_check_right_panels(page=logged_in_page)
    
@allure.epic("Smoke", "Load")
@allure.feature("XWiki")
@allure.story("User", "Page", "Create page", "Edit page")
@allure.title("XWiki: New page")
@pytest.mark.dependency(name="create_xwiki_page")
@pytest.mark.user("user")
def test_create_wiki_page(logged_in_page: Page, url_portal: str, run_id: str):
    steps_xwiki.step_open_page(page=logged_in_page, url_portal=url_portal)
    title: str = f"page-{run_id}"
    content: str = "".join(random.choices(population="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 .,", k=random.randint(40, 60)))
    
    steps_xwiki.step_click_create_button(page=logged_in_page)
    steps_xwiki.step_new_page_fill_title(page=logged_in_page, title=title)
    steps_xwiki.step_new_page_click_create(page=logged_in_page)
    steps_xwiki.step_edit_page_fill_content(page=logged_in_page, content=content)
    steps_xwiki.step_edit_page_click_save_and_view(page=logged_in_page)
    steps_xwiki.step_check_new_page_in_navigation_and_selected(page=logged_in_page, title=title)

@allure.epic("Smoke")
@allure.feature("XWiki")
@allure.story("User", "Page", "Create page", "Edit page")
@allure.title("XWiki: Create newsfeed blog post")
@pytest.mark.dependency(name="create_newsfeed_blog_post")
@pytest.mark.user("component_admin")
def test_create_newsfeed_blog_post(logged_in_page: Page, url_portal: str, run_id: str):
    title: str = f"newsfeed-{run_id}"
    content: str = "".join(random.choices(population="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 .,", k=random.randint(40, 60)))
    
    steps_xwiki.step_open_newsfeed_page(page=logged_in_page, url_portal=url_portal)
    steps_xwiki.step_fill_newsfeed_blog_post_title(page=logged_in_page, title=title)
    steps_xwiki.step_click_create_newsfeed_blog_post(page=logged_in_page)
    steps_xwiki.step_edit_blog_fill_content(page=logged_in_page, content=content)
    steps_xwiki.step_edit_blog_click_save_and_view(page=logged_in_page)
    
    steps_portal.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_portal.step_newsfeed_blog_visible(page=logged_in_page, title=title, content=content)
    