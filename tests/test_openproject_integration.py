# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page
from tests.config.config import Config
from tests.steps import steps_openproject
import allure
import pytest

@allure.epic("Smoke")
@allure.feature("OpenProject")
@allure.story("User", "File Storage")
@allure.title("OpenProject: Create integration project with filestore")
@pytest.mark.dependency(name="create_integration_project", scope="session")
@pytest.mark.user("component_admin")
@pytest.mark.xdist_group("nc_op_integration")
def test_openproject_create_integration_project(logged_in_page: Page, url_portal: str, run_id: str, config: Config):
    project_name: str = f"integration-project-{run_id}" 
    
    steps_openproject.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_openproject.step_click_through_welcome_modals(page=logged_in_page)
    steps_openproject.step_open_project_select(page=logged_in_page)
    steps_openproject.step_click_new_project(page=logged_in_page)
    
    steps_openproject.step_new_project_fill_name(page=logged_in_page, name=project_name)
    steps_openproject.step_new_project_unselect_parent_project(page=logged_in_page)
    steps_openproject.step_new_project_check_no_parent_selected(page=logged_in_page)
    steps_openproject.step_new_project_click_create(page=logged_in_page)
    steps_openproject.step_check_project_open(page=logged_in_page, project_name=project_name)
    
    steps_openproject.step_open_files_menu(page=logged_in_page)
    steps_openproject.step_click_add_storage(page=logged_in_page)
    steps_openproject.step_click_submit_button(page=logged_in_page)
    steps_openproject.step_check_automatic_checked(page=logged_in_page)
    steps_openproject.step_click_submit_button(page=logged_in_page)
    steps_openproject.step_grant_access_to_nextcloud(page=logged_in_page)
    steps_openproject.step_check_file_storage_exists(page=logged_in_page)
    
    config.save("IntegrationProjectName", project_name)

@allure.epic("Smoke")
@allure.feature("OpenProject")
@allure.story("User")
@allure.title("OpenProject: Check Nextcloud integration work package exists")
@pytest.mark.dependency(depends=["create_nextcloud_work_package"], scope="session")
@pytest.mark.user("component_admin")
@pytest.mark.xdist_group("nc_op_integration")
def test_openproject_nextcloud_integration_work_package_exists(logged_in_page: Page, url_portal: str, config: Config):
    project_name: str | None = config.load("IntegrationProjectName")
    assert project_name is not None, "Integration project was not created"
    work_package_subject: str | None = config.load("NCWorkPackageSubject")
    assert work_package_subject is not None, "Work package was not created"
    
    steps_openproject.step_open_page(page=logged_in_page, url_portal=url_portal)
    steps_openproject.step_open_project_select(page=logged_in_page)
    steps_openproject.step_open_project(page=logged_in_page, project_name=project_name)
    steps_openproject.step_open_work_packages(page=logged_in_page)
    steps_openproject.step_check_work_package_exists(page=logged_in_page, subject=work_package_subject)