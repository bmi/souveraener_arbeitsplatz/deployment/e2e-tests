# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

import datetime
from imap_tools import MailBox
from playwright.sync_api import Page
from tests.steps import steps_mail, steps_opentalk
import allure
import pytest

@allure.epic("OpenTalk")
@allure.feature("OpenTalk")
@allure.title("OpenTalk: Login user")
def test_login(page: Page, opentalk_url: str, opentalk_username: str, opentalk_password: str):
    steps_opentalk.step_open_page(page=page, opentalk_url=opentalk_url)
    steps_opentalk.step_check_login_page(page=page)
    steps_opentalk.step_check_login_form(page=page)
    steps_opentalk.step_fill_login_form(page=page, username=opentalk_username, password=opentalk_password)
    steps_opentalk.step_click_login(page=page)
    steps_opentalk.step_check_opentalk_page(page=page)

@allure.epic("OpenTalk")
@allure.feature("OpenTalk")
@allure.title("OpenTalk: Start new meeting")
def test_start_meeting(opentalk_page: Page, opentalk_url: str):
    steps_opentalk.step_open_page(page=opentalk_page, opentalk_url=opentalk_url)
    steps_opentalk.step_check_opentalk_page(page=opentalk_page)
    steps_opentalk.step_click_start_new(page=opentalk_page)
    room_page: Page = steps_opentalk.step_click_open_video_room(page=opentalk_page)
    steps_opentalk.step_click_join_room(page=room_page)
    steps_opentalk.step_check_room_joined(page=room_page)
    room_page.close()

@allure.epic("OpenTalk")
@allure.feature("OpenTalk")
@allure.title("OpenTalk: Plan new meeting")
@pytest.mark.xdist_group("meeting1")
def test_plan_new_meeting(opentalk_page: Page, opentalk_url: str, run_id: str, autotest_mail: str, mailbox: MailBox):
    meeting_title: str = f"Meeting {run_id}"
    
    steps_opentalk.step_open_page(page=opentalk_page, opentalk_url=opentalk_url)
    steps_opentalk.step_click_plan_new(page=opentalk_page)
    steps_opentalk.step_fill_title(page=opentalk_page, title=meeting_title)
    steps_opentalk.step_click_save(page=opentalk_page)
    steps_opentalk.step_click_create_anyway(page=opentalk_page)
    steps_opentalk.step_fill_participant(page=opentalk_page, participant_mail=autotest_mail)
    steps_opentalk.step_select_participant(page=opentalk_page, participant_mail=autotest_mail)
    steps_opentalk.step_click_send_invitations(page=opentalk_page)
    steps_mail.step_receive_email(mailbox=mailbox, subject=meeting_title)

@allure.epic("OpenTalk")
@allure.feature("OpenTalk")
@allure.title("OpenTalk: Edit meeting")
@pytest.mark.xdist_group("meeting1")
def test_edit_meeting(opentalk_page: Page, run_id: str, opentalk_url: str):
    meeting_title: str = f"Meeting {run_id}"
    new_meeting_title = f"Edited Meeting {run_id}"
    
    steps_opentalk.step_open_page(page=opentalk_page, opentalk_url=opentalk_url)
    steps_opentalk.step_open_meetings_tab(page=opentalk_page)
    steps_opentalk.step_click_on_period_combobox(page=opentalk_page)
    steps_opentalk.step_select_period_in_the_future(page=opentalk_page)
    steps_opentalk.step_check_meeting_tile_visible(page=opentalk_page, meeting_title=meeting_title)
    steps_opentalk.step_click_on_more_settings_button(page=opentalk_page, meeting_title=meeting_title)
    steps_opentalk.step_click_on_edit(page=opentalk_page)
    steps_opentalk.step_fill_title(page=opentalk_page, title=new_meeting_title)
    steps_opentalk.step_click_save(page=opentalk_page)
    steps_opentalk.step_click_create_anyway(page=opentalk_page)

@allure.epic("OpenTalk")
@allure.feature("OpenTalk")
@allure.title("OpenTalk: Delete meeting")
@pytest.mark.xdist_group("meeting1")
def test_delete_meeting(opentalk_page: Page, run_id: str, opentalk_url: str):
    meeting_title = f"Edited Meeting {run_id}"
    
    steps_opentalk.step_open_page(page=opentalk_page, opentalk_url=opentalk_url)
    steps_opentalk.step_open_meetings_tab(page=opentalk_page)
    steps_opentalk.step_click_on_period_combobox(page=opentalk_page)
    steps_opentalk.step_select_period_in_the_future(page=opentalk_page)
    steps_opentalk.step_check_meeting_tile_visible(page=opentalk_page, meeting_title=meeting_title)
    steps_opentalk.step_click_on_more_settings_button(page=opentalk_page, meeting_title=meeting_title)
    steps_opentalk.step_click_delete(page=opentalk_page)
    steps_opentalk.step_check_meeting_delete_window(page=opentalk_page)
    steps_opentalk.step_check_meeting_tile_not_visible(page=opentalk_page, meeting_title=meeting_title)

@allure.epic("OpenTalk")
@allure.feature("OpenTalk")
@allure.title("OpenTalk: Edit profile name")
def test_edit_profile_name(opentalk_page: Page, opentalk_url: str, opentalk_firstname: str, opentalk_lastname: str):
    original_name: str = f"{opentalk_firstname} {opentalk_lastname}"
    changed_name: str = f"New {opentalk_firstname} {opentalk_lastname}"
    steps_opentalk.step_open_page(page=opentalk_page, opentalk_url=opentalk_url)
    steps_opentalk.step_click_profile_picture(page=opentalk_page)
    steps_opentalk.step_change_profile_name(page=opentalk_page, profile_name=changed_name)
    steps_opentalk.step_click_save_changes(page=opentalk_page)
    steps_opentalk.step_change_profile_name(page=opentalk_page, profile_name=original_name)
    steps_opentalk.step_click_save_changes(page=opentalk_page)
    
    
    
@allure.epic("OpenTalk")
@allure.feature("OpenTalk")
@allure.title("OpenTalk: Look at Meeting with filter")
def test_look_at_meeting_with_filter(opentalk_page: Page, opentalk_url: str):
    steps_opentalk.step_open_page(page=opentalk_page, opentalk_url=opentalk_url)
    steps_opentalk.step_open_meetings_tab(page=opentalk_page)
    steps_opentalk.step_click_on_period_combobox(page=opentalk_page)
    steps_opentalk.step_select_period_in_the_future(page=opentalk_page)
    steps_opentalk.step_open_time_period_select(page=opentalk_page)
    steps_opentalk.step_check_time_period_select_expands(page=opentalk_page)
    steps_opentalk.step_select_time_period_month(page=opentalk_page)

@allure.epic("OpenTalk")
@allure.feature("OpenTalk")
@allure.title("OpenTalk: Link to the help function works")
def test_link_to_the_help_function_works(opentalk_page: Page, opentalk_url: str):
    steps_opentalk.step_open_page(page=opentalk_page, opentalk_url=opentalk_url)
    steps_opentalk.step_click_help_sidebar(page=opentalk_page)
    steps_opentalk.step_check_documentation_page(page=opentalk_page)
    steps_opentalk.step_click_open_button(page=opentalk_page)

@allure.epic("OpenTalk")
@allure.feature("OpenTalk")
@allure.title("OpenTalk: Join Meeting with Url")
def test_join_meeting_with_url(opentalk_page: Page, opentalk_url: str):
    steps_opentalk.step_open_page(page=opentalk_page, opentalk_url=opentalk_url)
    steps_opentalk.step_click_join_meeting_button(page=opentalk_page)
    steps_opentalk.step_check_dialogbox(page=opentalk_page)
    steps_opentalk.step_enter_conference_id(page=opentalk_page, conference_id="5f26a072-2d2e-47ff-93c6-cac0c59485b2") # Woher kommt die ID? Bitte vom vorher erstellten Meeting speichern und verwenden
    steps_opentalk.step_click_join_button(page=opentalk_page)
    steps_opentalk.step_check_successful_join(page=opentalk_page)

@allure.epic("OpenTalk")
@allure.feature("OpenTalk")
@allure.title("OpenTalk: Add meetings to favorites")
def test_add_meetings_to_favorites(opentalk_page: Page, run_id: str, opentalk_url: str):
    meeting_title = f"Add meetings to favorites {run_id}"
    
    steps_opentalk.step_open_page(page=opentalk_page, opentalk_url=opentalk_url)
    steps_opentalk.step_click_plan_new(page=opentalk_page)
    steps_opentalk.step_fill_title(page=opentalk_page, title=meeting_title)
    steps_opentalk.step_click_save(page=opentalk_page)
    steps_opentalk.step_click_create_anyway(page=opentalk_page)
    steps_opentalk.step_open_meetings_tab(page=opentalk_page)
    steps_opentalk.step_click_on_period_combobox(page=opentalk_page)
    steps_opentalk.step_select_period_in_the_future(page=opentalk_page)
    steps_opentalk.step_check_meeting_tile_visible(page=opentalk_page, meeting_title=meeting_title)
    steps_opentalk.step_click_on_more_settings_button(page=opentalk_page, meeting_title=meeting_title)
    steps_opentalk.step_add_to_favorites(page=opentalk_page)

@allure.epic("OpenTalk")
@allure.feature("OpenTalk")
@allure.title("OpenTalk: Join meeting with link")
def test_join_meeting_with_link(opentalk_page: Page, opentalk_url: str):
    steps_opentalk.step_open_page(page=opentalk_page, opentalk_url=opentalk_url)
    steps_opentalk.step_click_start_new(page=opentalk_page)
    room_page: Page = steps_opentalk.step_click_open_video_room(page=opentalk_page)
    steps_opentalk.step_click_join_room(page=room_page)
    steps_opentalk.step_check_room_joined(page=room_page)
    room_page.close()

@allure.epic("OpenTalk")
@allure.feature("OpenTalk")
@allure.title("OpenTalk: Change language")
def test_change_language(opentalk_page: Page, opentalk_url: str):
    steps_opentalk.step_open_page(page=opentalk_page, opentalk_url=opentalk_url)
    steps_opentalk.step_click_settings(page=opentalk_page)
    steps_opentalk.step_click_language_box(page=opentalk_page)
    steps_opentalk.step_check_language_selection_box(page=opentalk_page)
    steps_opentalk.step_select_english(page=opentalk_page)
    steps_opentalk.step_save_language_change(page=opentalk_page)

@allure.epic("OpenTalk")
@allure.feature("OpenTalk")
@allure.title("OpenTalk: Change password")
def test_change_password(opentalk_page: Page, opentalk_url: str, opentalk_password: str, opentalk_email: str):
    steps_opentalk.step_open_page(page=opentalk_page, opentalk_url=opentalk_url)
    steps_opentalk.step_click_account_management(page=opentalk_page)
    steps_opentalk.step_check_reset_password_page(page=opentalk_page, email=opentalk_email)
    steps_opentalk.step_fill_new_password(page=opentalk_page, new_password=opentalk_password)
    steps_opentalk.step_save_new_password(page=opentalk_page)

@allure.epic("OpenTalk")
@allure.feature("OpenTalk")
@allure.title("OpenTalk: Check Impressum link")
def test_check_impressum_link(opentalk_page: Page, opentalk_url: str):
    steps_opentalk.step_open_page(page=opentalk_page, opentalk_url=opentalk_url)
    steps_opentalk.step_click_legal(page=opentalk_page)
    steps_opentalk.step_click_impressum(page=opentalk_page)

@allure.epic("OpenTalk")
@allure.feature("OpenTalk")
@allure.title("OpenTalk: Link to data protection notice works")
def test_link_to_data_protection_notice_works(opentalk_page: Page, opentalk_url: str):
    steps_opentalk.step_open_page(page=opentalk_page, opentalk_url=opentalk_url)
    steps_opentalk.step_click_legal(page=opentalk_page)
    steps_opentalk.step_click_privacy_notice(page=opentalk_page)

@allure.epic("OpenTalk")
@allure.feature("OpenTalk")
@allure.title("OpenTalk: Logout works")
def test_logout_works(opentalk_page: Page, opentalk_url: str):
    steps_opentalk.step_open_page(page=opentalk_page, opentalk_url=opentalk_url)
    steps_opentalk.step_click_logout(page=opentalk_page)

@allure.epic("OpenTalk")
@allure.feature("OpenTalk")
@allure.title("OpenTalk: New user with registration")
def test_new_user_with_registration(opentalk_page: Page, opentalk_url: str):
    steps_opentalk.step_open_page(page=opentalk_page, opentalk_url=opentalk_url)
    steps_opentalk.step_click_logout(page=opentalk_page)
    steps_opentalk.step_click_register(page=opentalk_page)

@allure.epic("OpenTalk")
@allure.feature("OpenTalk")
@allure.title("OpenTalk: Invite person with link to a meeting while in one")
def test_invite_person_with_link_to_a_meeting_while_in_one(opentalk_page: Page, opentalk_url: str):
    steps_opentalk.step_open_page(page=opentalk_page, opentalk_url=opentalk_url)
    steps_opentalk.step_check_opentalk_page(page=opentalk_page)
    steps_opentalk.step_click_start_new(page=opentalk_page)
    room_page: Page = steps_opentalk.step_click_open_video_room(page=opentalk_page)
    steps_opentalk.step_click_join_room(page=room_page)
    steps_opentalk.step_check_room_joined(page=room_page)
    steps_opentalk.step_click_three_dots(page=room_page)
    steps_opentalk.step_click_invite_guest(page=room_page)
    steps_opentalk.step_click_create_button(page=room_page)
    steps_opentalk.step_copy_link(page=room_page)
    room_page.close()

@allure.epic("OpenTalk")
@allure.feature("OpenTalk")
@allure.title("OpenTalk: Go back to the homepage")
def test_go_back_to_the_homepage(opentalk_page: Page, opentalk_url: str):
    steps_opentalk.step_open_page(page=opentalk_page, opentalk_url=opentalk_url)
    steps_opentalk.step_check_opentalk_page(page=opentalk_page)
    steps_opentalk.step_click_start_new(page=opentalk_page)
    room_page: Page = steps_opentalk.step_click_open_video_room(page=opentalk_page)
    steps_opentalk.step_click_join_room(page=room_page)
    steps_opentalk.step_check_room_joined(page=room_page)
    steps_opentalk.step_leave_meeting(page=room_page)
    steps_opentalk.step_click_arrow_to_home(page=room_page)

@allure.epic("OpenTalk")
@allure.feature("OpenTalk")
@allure.title("OpenTalk: Change view")
def test_change_view(opentalk_page: Page, opentalk_url: str):
    steps_opentalk.step_open_page(page=opentalk_page, opentalk_url=opentalk_url)
    steps_opentalk.step_check_opentalk_page(page=opentalk_page)
    steps_opentalk.step_click_start_new(page=opentalk_page)
    room_page: Page = steps_opentalk.step_click_open_video_room(page=opentalk_page)
    steps_opentalk.step_click_join_room(page=room_page)
    steps_opentalk.step_check_room_joined(page=room_page)
    steps_opentalk.step_click_view_options(page=room_page)
    steps_opentalk.step_switch_to_gallery_view(page=room_page)
    steps_opentalk.step_click_view_options(page=room_page)
    steps_opentalk.step_switch_to_speaker_view(page=room_page)
    steps_opentalk.step_click_view_options(page=room_page)
    steps_opentalk.step_switch_to_grid_view(page=room_page)

@allure.epic("OpenTalk")
@allure.feature("OpenTalk")
@allure.title("OpenTalk: Create repeating meetings showing in meetings")
def test_create_repeating_meetings_showing_in_meetings(opentalk_page: Page, opentalk_url: str, run_id: str, autotest_mail: str, mailbox: MailBox):
    meeting_title: str = f"Meeting {run_id}"
    
    steps_opentalk.step_open_page(page=opentalk_page, opentalk_url=opentalk_url)
    steps_opentalk.step_click_plan_new(page=opentalk_page)
    steps_opentalk.step_fill_title(page=opentalk_page, title=meeting_title)
    steps_opentalk.step_select_meeting_frequency(page=opentalk_page)
    steps_opentalk.step_select_daily_option(page=opentalk_page)
    steps_opentalk.step_click_save(page=opentalk_page)
    steps_opentalk.step_click_create_anyway(page=opentalk_page)
    steps_opentalk.step_fill_participant(page=opentalk_page, participant_mail=autotest_mail)
    steps_opentalk.step_select_participant(page=opentalk_page, participant_mail=autotest_mail)
    steps_opentalk.step_click_send_invitations(page=opentalk_page)
    steps_mail.step_receive_email(mailbox=mailbox, subject=meeting_title)

@allure.epic("OpenTalk")
@allure.feature("OpenTalk")
@allure.title("OpenTalk: Forgot password")
def test_forgot_password(page: Page, opentalk_url: str, mailbox: MailBox, opentalk_email: str):
    steps_opentalk.step_open_page(page=page, opentalk_url=opentalk_url)
    steps_opentalk.step_check_login_page(page=page)
    steps_opentalk.step_check_login_form(page=page)
    steps_opentalk.step_click_forgot_password(page=page)
    steps_opentalk.step_enter_email(page=page, email=opentalk_email)
    steps_opentalk.step_click_submit(page=page)
    mail = steps_mail.step_receive_email(mailbox=mailbox, body="https://accounts.meet.opentalk.eu/auth/realms/opentalk/login-actions/action-token?key=")
    assert mail, "No mail received"
    
    
@allure.epic("OpenTalk")
@allure.feature("OpenTalk")
@allure.title("OpenTalk: Function bar under your camera feed hand symbol")
def test_function_bar_under_your_camera_feed_hand_symbol(opentalk_page: Page, opentalk_url: str):
    steps_opentalk.step_open_page(page=opentalk_page, opentalk_url=opentalk_url)
    steps_opentalk.step_check_opentalk_page(page=opentalk_page)
    steps_opentalk.step_click_start_new(page=opentalk_page)
    room_page: Page = steps_opentalk.step_click_open_video_room(page=opentalk_page)
    steps_opentalk.step_click_join_room(page=room_page)
    steps_opentalk.step_check_room_joined(page=room_page)
    steps_opentalk.step_raise_hand(page=room_page)
    steps_opentalk.step_lower_hand(page=room_page)
    room_page.close()
    
    
    
@allure.epic("OpenTalk")
@allure.feature("OpenTalk")
@allure.title("OpenTalk: Setting Recurring Appointments")
def test_setting_recurring_appointments(opentalk_page: Page, opentalk_url: str, run_id: str, autotest_mail: str, mailbox: MailBox):
    meeting_title: str = f"Meeting {run_id}"
    
    steps_opentalk.step_open_page(page=opentalk_page, opentalk_url=opentalk_url)
    steps_opentalk.step_click_plan_new(page=opentalk_page)
    steps_opentalk.step_fill_title(page=opentalk_page, title=meeting_title)
    steps_opentalk.step_select_meeting_frequency(page=opentalk_page)
    steps_opentalk.step_select_daily_option(page=opentalk_page)
    steps_opentalk.step_select_meeting_frequency(page=opentalk_page)
    steps_opentalk.step_check_weekly_option(page=opentalk_page)
    steps_opentalk.step_select_meeting_frequency(page=opentalk_page)
    steps_opentalk.step_click_every_2_weeks_option(page=opentalk_page)
    steps_opentalk.step_select_meeting_frequency(page=opentalk_page)
    steps_opentalk.step_click_monthly_option(page=opentalk_page)
    steps_opentalk.step_select_meeting_frequency(page=opentalk_page)
    steps_opentalk.step_click_every_2_weeks_option(page=opentalk_page)
    steps_opentalk.step_click_save(page=opentalk_page)
    
    
@allure.epic("OpenTalk")
@allure.feature("OpenTalk")
@allure.title("OpenTalk: Microphone function works in the meeting room")
def test_microphone_function_works_in_the_meeting_room(opentalk_page: Page, opentalk_url: str):
    steps_opentalk.step_open_page(page=opentalk_page, opentalk_url=opentalk_url)
    steps_opentalk.step_check_opentalk_page(page=opentalk_page)
    steps_opentalk.step_click_start_new(page=opentalk_page)
    room_page: Page = steps_opentalk.step_click_open_video_room(page=opentalk_page)
    steps_opentalk.step_click_join_room(page=room_page)
    steps_opentalk.step_check_room_joined(page=room_page)
    steps_opentalk.step_click_microphone_button(page=room_page)
    steps_opentalk.step_click_microphone_button(page=room_page)
    room_page.close()
    
    
@allure.epic("OpenTalk")
@allure.feature("OpenTalk")
@allure.title("OpenTalk: Video function works in the meeting room")
def test_video_function_works_in_the_meeting_room(opentalk_page: Page, opentalk_url: str):
    steps_opentalk.step_open_page(page=opentalk_page, opentalk_url=opentalk_url)
    steps_opentalk.step_check_opentalk_page(page=opentalk_page)
    steps_opentalk.step_click_start_new(page=opentalk_page)
    room_page: Page = steps_opentalk.step_click_open_video_room(page=opentalk_page)
    steps_opentalk.step_click_join_room(page=room_page)
    steps_opentalk.step_check_room_joined(page=room_page)
    steps_opentalk.step_click_video_source_dropdown(page=room_page)
    steps_opentalk.step_click_video_source_dropdown(page=room_page)
    room_page.close()
    

@allure.epic("OpenTalk")
@allure.feature("OpenTalk")
@allure.title("OpenTalk: 3-Point Menu function works in the meeting room")
def test_3_point_menu_function_works_in_the_meeting_room(opentalk_page: Page, opentalk_url: str):
    steps_opentalk.step_open_page(page=opentalk_page, opentalk_url=opentalk_url)
    steps_opentalk.step_check_opentalk_page(page=opentalk_page)
    steps_opentalk.step_click_start_new(page=opentalk_page)
    room_page: Page = steps_opentalk.step_click_open_video_room(page=opentalk_page)
    steps_opentalk.step_click_join_room(page=room_page)
    steps_opentalk.step_check_room_joined(page=room_page)
    steps_opentalk.step_click_3_point_menu(page=room_page)
    room_page.close()
    
    
@allure.epic("OpenTalk")
@allure.feature("OpenTalk")
@allure.title("OpenTalk: Leave the Room function works in the meeting room")
def test_leave_the_room_function_works_in_the_meeting_room(opentalk_page: Page, opentalk_url: str):
    steps_opentalk.step_open_page(page=opentalk_page, opentalk_url=opentalk_url)
    steps_opentalk.step_check_opentalk_page(page=opentalk_page)
    steps_opentalk.step_click_start_new(page=opentalk_page)
    room_page: Page = steps_opentalk.step_click_open_video_room(page=opentalk_page)
    steps_opentalk.step_click_join_room(page=room_page)
    steps_opentalk.step_check_room_joined(page=room_page)
    steps_opentalk.step_click_leave_room(page=room_page)
    room_page.close()
    
@allure.epic("OpenTalk")
@allure.feature("OpenTalk")
@allure.title("OpenTalk: Share Screen functions works in the meeting room")
def test_share_screen_function_works_in_the_meeting_room(opentalk_page: Page, opentalk_url: str):
    steps_opentalk.step_open_page(page=opentalk_page, opentalk_url=opentalk_url)
    steps_opentalk.step_check_opentalk_page(page=opentalk_page)
    steps_opentalk.step_click_start_new(page=opentalk_page)
    room_page: Page = steps_opentalk.step_click_open_video_room(page=opentalk_page)
    steps_opentalk.step_click_join_room(page=room_page)
    steps_opentalk.step_check_room_joined(page=room_page)
    steps_opentalk.step_share_application_window(page=room_page)
    room_page.close()