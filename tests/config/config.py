# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from __future__ import annotations
import base64
import json
from pathlib import Path
import string
from time import sleep
from typing import Literal
from filelock import FileLock
import pytest
import re
import os
import random
import sys
import requests
from configparser import ConfigParser

import yaml
import yaml.parser

import config.settings as settings

def config_parser_init(parser: pytest.Parser):
    """
    This is called from pytest_addoption that again is just an argparser
    :param parser: pytest.Parser
    :return:
    """
    for setting in settings.settings:
        setting.set_config_option(parser=parser)
    for environment_setting in settings.environment_settings:
        environment_setting.set_config_option(parser=parser)


class Config:
    """
    Config collects all relevant config parameters. It is able to handle
    ini-file base config, or cli based. Also, a very basic initialization check
    is performed.
    """
    
    class User:
        """
        Inner class that is used by Config() and given to the test cases that will
        create a new user and perform other checks on the created user. 
        """
        
        def __init__(self, key: str, firstname: str | None = None, lastname: str | None = None, username: str | None = None, password: str | None = None, email: str | None = None, email_domain: str | None = None, disable_components: bool = False, component_admin: bool = False, create_admin: bool = False, enable_2fa: bool = False, skip_import: bool = False) -> None:
            self.key = key
            self.firstname = firstname
            self.lastname = lastname
            self.username = username
            self.password = password
            self.email_domain = email_domain
            self.email = email or f"{username}@{email_domain}"
            self.element_username = "@" + self.email.replace("@", ":")
            self.disable_components = disable_components
            self.component_admin = component_admin
            self.create_admin = create_admin
            self.enable_2fa = enable_2fa
            self.skip_import = skip_import
            self.is_admin = False
        
        def copy(self, key: str, username_suffix: str = ""):
            return Config.User(
                key=key,
                firstname=f"{self.firstname}",
                lastname=self.lastname,
                username=f"{self.username}{username_suffix}",
                password=self.password,
                email_domain=self.email_domain,
                disable_components=self.disable_components,
                component_admin=self.component_admin,
                create_admin=self.create_admin,
                enable_2fa=self.enable_2fa,
                skip_import=self.skip_import
            )
        
        def to_admin(self):
            return self.copy(f"{self.key}_admin", "-admin").set("create_admin", False).set("is_admin", True).set("skip_import", True)
        
        def set(self, key: str, value: bool):
            setattr(self, key, value)
            return self

    def __init__(self, session: pytest.Session):
        config_options = session.config.option
        items = session.items
        
        config_file: str = config_options.config_file
        if config_file:
            if not os.path.isfile(config_file):
                raise FileNotFoundError(f"Unable to find config file {config_file}")

            config_parser = ConfigParser()
            config_parser.read(config_file)
        else:
            config_parser = None
        
        for setting in settings.settings:
            value = None
            
            if config_file is not None and config_parser is not None:
                if setting.ini_section is not None and setting.ini_key is not None:
                    value = setting.get_ini_value(config_parser=config_parser)
                    
            if setting.cli_name is not None:
                value = setting.get_cli_value(config_options=config_options) or value
            
            default = setting.get_default(self)
            value = value or default
            if not value and setting.required:
                raise Exception(f"Setting {setting.name} required but not set.")
            
            if setting.sanitize is not None:
                value = setting.sanitize(value)
            setattr(self, setting.name, value)
        
        self.stored_values = {}
        self.namespace_profile = None
        
        settings.register_test_users(self)
        
        self.test_users = settings.test_users.copy()
        
        for environment_setting in settings.environment_settings:
            setattr(self, environment_setting.name, environment_setting.default_value)
        
        if self.disable_population:
            print("Disabled population. Skipping environment settings initialisation and user import.")
            return

        if self.profile == "Custom":
            for environment_setting in settings.environment_settings:
                value = None
                
                if config_options is not None and config_parser is not None:
                    if setting.ini_section is not None and setting.ini_key is not None:
                        value = environment_setting.get_ini_value(config_parser=config_parser)
                        
                if environment_setting.cli_name is not None:
                    value = environment_setting.get_cli_value(config_options=config_options) or value
                
                default = environment_setting.default_value
                value = value or default
                
                setattr(self, environment_setting.name, value)
        elif self.profile == "Namespace":
            self.namespace_profile = self.fetch_namespace_profile()
            
            for environment_setting in settings.environment_settings:
                value = environment_setting.get_namespace_value(namespace_profile=self.namespace_profile)
                setattr(self, environment_setting.name, value)
        
        if self.env_self_signed_ca:
            self.self_signed_certificate = self.fetch_self_signed_ca()
            self.setup_self_signed_certificate()
        
        used_users: set[str] = set()
        
        for item in items:
            if user := item.get_closest_marker("user"):
                used_users.add(user.args[0])
            if users := item.get_closest_marker("users"):
                for user in users.args[0]:
                    used_users.add(user)
            if users_dependency := item.get_closest_marker("users_dependency"):
                for user in users_dependency.args[0]:
                    used_users.add(user)
            
        session.config.hook.pytest_deselected(items=items)
        
        self.user_importer: settings.UserImporter = settings.UserImporter.from_config(self)
        self.udm_api: settings.UdmApi = settings.UdmApi.from_config(self)
        
        if not self.env_udm_api_available:
            self.user_importer.use_localhost_port(port=random.randint(9900, 9950))
            self.udm_api.use_localhost_port(port=random.randint(9850, 9899))
        
        if not used_users:
            print("No users to import. Skipping user import.")
            return
        
        for test_user in settings.test_users:
            if not test_user.to_admin().key in used_users:
                if test_user.key not in used_users:
                    print(f"Skipping user {test_user.key}. Not used.")
                    continue
                if test_user.skip_import:
                    print(f"Skipping user {test_user.key}. Skip import flag set.")
                    continue
            
            user = self.user_importer.import_user(test_user)
            if user:
                self.test_users.append(user)
            else:
                print(f"User import failed for {test_user.username}.")
        
        user_import_grace_period: int = int(self.user_import_grace_period)
        print(f"Grace period for user import: {user_import_grace_period}s")
        sleep(user_import_grace_period)
    
    def register_env_args(self):
        os.environ["DO_SCREENSHOT_BEFORE_STEP"] = self.screenshot_before_step
        os.environ["DO_SCREENSHOT_AFTER_STEP"] = self.screenshot_after_step
        os.environ["DO_SCREENSHOT_REDIRECT_STEP"] = self.screenshot_redirect_step
        os.environ["LANGUAGE"] = self.language
        if self.env_self_signed_ca and (sys.platform == "linux" or sys.platform == "linux2"):
            os.environ["REQUESTS_CA_BUNDLE"] = "/etc/ssl/certs/ca-certificates.crt"
    
    def get_test_user(self, key: Literal["user", "user_admin", "user_2fa", "new_user", "guest", "component_admin"]) -> Config.User | None:
        for user in self.test_users:
            if user.key == key:
                return user
        return None
    
    def cleanup(self):
        for setting in settings.settings:
            setting.do_cleanup(self)
    
    def setup_self_signed_certificate(self):
        if sys.platform == "linux" or sys.platform == "linux2":
            print("Copying certificate file...")
            os.system(f"cp {self.temp_folder}{os.sep}{self.self_signed_certificate} /usr/local/share/ca-certificates/")
            print("Updating certificates...")
            os.system("update-ca-certificates")
            print("Setting certificate for requests...")
            os.environ["REQUESTS_CA_BUNDLE"] = "/etc/ssl/certs/ca-certificates.crt"
            print("Setting certificate for playwright...")
            os.system("apt-get install libnss3-tools")
            os.system("mkdir -p $HOME/.pki/nssdb")
            os.system(f"certutil -d sql:$HOME/.pki/nssdb -A -t \"C,,\" -n {self.self_signed_certificate} -i /usr/local/share/ca-certificates/{self.self_signed_certificate}")
        else:
            print(f"Cannot setup self signed certificate. Platform not implemented: {sys.platform}")
    
    def fetch_self_signed_ca(self) -> str:
        gitlab_namespace_template: str = self.gitlab_env_namespace_template.format_map({
            "file": self.gitlab_env_certificate_file
        })
        gitlab_certificate_url = gitlab_namespace_template.format_map({
            "operator": self.operator,
            "cluster": self.cluster,
            "namespace": self.namespace
        })
        certificate = requests.get(gitlab_certificate_url, headers={
            "PRIVATE-TOKEN": self.gitlab_token
        })
        
        if certificate.status_code != 200:
            raise Exception(f"Self signed certificate could not be found. Url: {gitlab_certificate_url}", certificate.text)
        
        certificate_json: dict = certificate.json()
        
        file_name: str = certificate_json.get("file_name")
        certificate_content_b64: str = certificate_json["content"]
        certificate_content = base64.b64decode(certificate_content_b64)
        
        with open(f"{self.temp_folder}{os.sep}{file_name}", "wb") as cert_file:
            cert_file.write(certificate_content)
            cert_file.close()
        
        return file_name
    
    def fetch_namespace_profile(self):
        gitlab_namespace_template: str = self.gitlab_env_namespace_template.format_map({
            "file": self.gitlab_env_namespace_file
        })
        gitlab_namespace_url = gitlab_namespace_template.format_map({
            "operator": self.operator,
            "cluster": self.cluster,
            "namespace": self.namespace
        })
        gitlab_namespace_default_url = gitlab_namespace_template.format_map({
            "operator": self.operator,
            "cluster": self.cluster,
            "namespace": self.gitlab_default_env_namespace
        })
        default_config = requests.get(self.gitlab_functional_yaml, headers={
            "PRIVATE-TOKEN": self.gitlab_token
        })
        if default_config.status_code != 200:
            raise Exception(f"Default config could not be found. Url: {self.gitlab_functional_yaml}", default_config.text)
        
        namespace_default_config = requests.get(gitlab_namespace_default_url, headers={
            "PRIVATE-TOKEN": self.gitlab_token
        })
        namespace_config = requests.get(gitlab_namespace_url, headers={
            "PRIVATE-TOKEN": self.gitlab_token
        })
        if namespace_default_config.status_code != 200:
            raise Exception(f"Namespace default config could not be found. Url: {gitlab_namespace_default_url}", namespace_default_config.text)
        
        config_content_b64: str = default_config.json()["content"]
        config_content = base64.b64decode(config_content_b64).decode("utf-8")
        config = self.parse_go_temp(config_content)
        namespace_default_content_b64: str = namespace_default_config.json()["content"]
        namespace_default_content = base64.b64decode(namespace_default_content_b64).decode("utf-8")
        namespace_default = self.parse_go_temp(namespace_default_content)
        
        if namespace_config.status_code == 200:
            namespace_content_b64: str = namespace_config.json()["content"]
            namespace_content = base64.b64decode(namespace_content_b64).decode("utf-8")
            namespace = self.parse_go_temp(namespace_content)
        else:
            namespace = {}
        
        merged_config = self.merge_dicts([config, namespace_default, namespace])

        return merged_config
    
    def dict_to_mapping(self, d, path=None):
        mapping = {}
        for key, value in d.items():
            curr_path = key if not path else f"{path}.{key}"
            if isinstance(value, dict):
                mapping |= self.dict_to_mapping(value, curr_path)
            else:
                mapping[curr_path] = value
        return mapping
    
    def merge_dicts(self, dicts):
        mapping = {}
        for d in dicts:
            mapping |= self.dict_to_mapping(d)
        
        new_d = {}
        for key, value in mapping.items():
            key_path = key.split(".")
            self.set_dict_value(new_d, key_path, value)
        
        return new_d
    
    def set_dict_value(self, d: dict, path: list[str], value):
        if len(path) == 0:
            return
        cur = path[0]
        if len(path) > 1:
            rest = path[1:]
            d[cur] = d.get(cur, {})
            self.set_dict_value(d[cur], rest, value)
            return
        d[cur] = value
    
    def parse_go_temp(self, string):
        parsed = {}
        
        yaml_blocks_starts = re.finditer("\n[^\\s]", string)
        start_block_indecies = [entry.span()[0] for entry in yaml_blocks_starts]
        yaml_blocks = self.split_indecies(string, start_block_indecies)
        
        for block in yaml_blocks:
            try:
                block = re.sub("\\{\\{(.*)\\}\\}", "var", block)
                block_parsed = yaml.safe_load(block)
                if block_parsed:
                    parsed |= block_parsed
            except Exception as e:
                pass
        
        return parsed
    
    def split_indecies(self, string, indecies):
        split_list = []
        curr = ""
        for i, s in enumerate(string):
            curr += s
            if i in indecies:
                split_list.append(curr)
                curr = ""
        split_list.append(curr)
        return split_list
    
    def load_stored_values(self):
        fn = Path("tmp/stored_values.json")
        with FileLock(str(fn) + ".lock"):
            if fn.is_file():
                with open(fn, "rb") as file:
                    data = json.loads(file.read())
            else:
                data = {}
                with open(fn, "wb") as file:
                    file.write(json.dumps(data).encode("utf-8"))
        self.stored_values = data
    
    def updated_stored_values(self, name, value):
        fn = Path("tmp/stored_values.json")
        with FileLock(str(fn) + ".lock"):
            data = {}
            if fn.is_file():
                with open(fn, "rb") as file:
                    data = json.loads(file.read())
            data[name] = value
            with open(fn, "wb") as file:
                file.write(json.dumps(data).encode("utf-8"))
    
    def save(self, name, value):
        self.updated_stored_values(name, value)
    
    def load(self, name):
        self.load_stored_values()
        return self.stored_values.get(name)
    
    def generate_number_sequence(self, length: int = 4):
        return "".join([str(random.choice(range(10))) for _ in range(length)])
    
    def generate_password(self, length: int = 20):
        return "".join([random.choice(string.ascii_letters + string.digits + string.punctuation) for _ in range(length)])