# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page
from pages.ox.page import *
from tests.utils import screenshot_step
import allure

from .steps_ox import *

@allure.step("Open Tasks page")
@screenshot_step
def step_open_page(page: Page, url_portal: str):
    TasksPage(page).open_page(url_portal)
    TasksPage(page).validate()

@allure.step("Click 'new task' button")
@screenshot_step
def step_click_new_task_button(page: Page):
    tasks_page: TasksPage = TasksPage(page=page)

    expect(tasks_page.new_tasks_button).to_be_visible()
    tasks_page.new_tasks_button.click()

@allure.step("Click 'expand form' button")
@screenshot_step
def step_click_expand_form_button(page: Page):
    tasks_page: TasksPage = TasksPage(page=page)

    expect(tasks_page.create_task_dialog.extend_form_button).to_be_visible()
    tasks_page.create_task_dialog.extend_form_button.click()

@allure.step("Check if contact row is visible")
@screenshot_step
def step_check_if_contact_row_is_visible(page: Page):
    tasks_page: TasksPage = TasksPage(page=page)

    expect(tasks_page.create_task_dialog.contact_row).to_be_visible()

@allure.step("Click 'pick contact' button")
@screenshot_step
def step_click_pick_contact_button(page: Page):
    tasks_page: TasksPage = TasksPage(page=page)
    pick_contact_button: Locator = tasks_page.create_task_dialog.pick_contact_button

    expect(pick_contact_button).to_be_visible()
    pick_contact_button.click()

@allure.step("Check if contact is in participant list")
@screenshot_step
def step_check_if_contact_is_in_participant_list(page: Page, firstname: str, lastname: str):
    tasks_page: TasksPage = TasksPage(page)

    expect(tasks_page.create_task_dialog.participants_row.get_participant_by_name(lastname=lastname, firstname=firstname)).to_be_visible()

