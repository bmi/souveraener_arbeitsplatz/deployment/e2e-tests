# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

import random
import shutil

from playwright.sync_api import Page, Locator
from pages.base.base import allow_fail, expect
from pages.nextcloud.page import *
from tests.utils import retry, screenshot_step
import allure
import os
import datetime

@allure.step("Open Nextcloud page")
@screenshot_step
def step_open_page(page: Page, url_portal: str):
    NextcloudPage(page).open_page(url_portal)
    NextcloudPage(page).validate()

@allure.step("Reload page")
@screenshot_step
def step_reload_page(page: Page, *_, **__):
    page.reload()
    NextcloudPage(page).validate()

@allure.step("Open admin Groupfolders page")
@screenshot_step
def step_open_admin_groupfolders_page(page: Page, url_portal: str):
    GroupfoldersSettingsPage(page).open_page(url_portal)
    GroupfoldersSettingsPage(page).validate()

@allure.step("Open security settings page")
@screenshot_step
def step_open_security_settings_page(page: Page, url_portal: str):
    SecuritySettingsPage(page).open_page(url_portal)
    SecuritySettingsPage(page).validate()

@allure.step("Open OpenProject Integration page")
@screenshot_step
def step_open_openproject_integration_page(page: Page, url_portal: str):
    OpenProjectIntegrationPage(page).open_page(url_portal)
    OpenProjectIntegrationPage(page).validate()

@allure.step("Open rich documents settings page")
@screenshot_step
def step_open_rich_documents_settings_page(page: Page, url_portal: str):
    RichDocumentsSettingsPage(page).open_page(url_portal)
    RichDocumentsSettingsPage(page).validate()

@allure.step("Upload file")
@screenshot_step
def step_upload_file(page: Page, folder: str, filename: str):
    abs_file_path: str = os.path.abspath(f"{folder}{os.sep}{filename}")
    files_page: FilesPage = FilesPage(page)
    
    files_page.get_upload_file_input().set_input_files(abs_file_path)
        
@allure.step("Check if file exists")
@screenshot_step
def step_check_file_exists(page: Page, filename: str):
    files_page: FilesPage = FilesPage(page)
    
    file_entry: FileEntry = files_page.file_list.get_file_entry(filename)
    
    expect(file_entry).to_be_attached(timeout=20000)
    file_entry.page_part_locator.scroll_into_view_if_needed()
    expect(file_entry).to_be_visible()
    
    step_upload_grace_period(page=page, timeout=2000)
    
    expect(files_page.file_conflict_modal).not_to_be_visible()
        
@allure.step("Check if zip file created")
@retry(trys=10, retry_timeout=0, on_fail=step_reload_page)
@screenshot_step
def step_check_zip_file_created(page: Page, filename: str):
    files_page: FilesPage = FilesPage(page)
    
    file_entry: FileEntry = files_page.file_list.get_file_entry(filename)
    
    expect(file_entry).to_be_attached(timeout=30*1000)
    file_entry.page_part_locator.scroll_into_view_if_needed()
    expect(file_entry).to_be_visible()

@allure.step("Open file")
@screenshot_step
def step_open_file(page: Page, filename: str):
    files_page: FilesPage = FilesPage(page)
    
    file_entry: FileEntry = files_page.file_list.get_file_entry(filename)
    
    expect(file_entry).to_be_attached()
    file_entry.page_part_locator.scroll_into_view_if_needed()
    expect(file_entry).to_be_visible()
    file_entry.page_part_locator.click()
        
@allure.step("Check if folder exists")
@screenshot_step
def step_check_folder_exists(page: Page, foldername: str):
    files_page: FilesPage = FilesPage(page)
    
    list_entry: FileEntry = files_page.file_list.get_file_entry(foldername)
    
    expect(list_entry).to_be_attached(timeout=30*1000)
    list_entry.page_part_locator.scroll_into_view_if_needed()
    expect(list_entry).to_be_visible()
    
@allure.step("Open folder")
@screenshot_step
def step_open_folder(page: Page, foldername: str):
    files_page: FilesPage = FilesPage(page)
    
    list_entry: FileEntry = files_page.file_list.get_file_entry(foldername)
    
    expect(list_entry).to_be_attached()
    list_entry.page_part_locator.scroll_into_view_if_needed()
    expect(list_entry).to_be_visible()
    list_entry.page_part_locator.click()
        
@allure.step("Check that file does not exist")
@screenshot_step
def step_check_file_not_exists(page: Page, filename: str):
    files_page: FilesPage = FilesPage(page)
    
    file_entry: FileEntry = files_page.file_list.get_file_entry(filename)
    
    expect(file_entry).not_to_be_visible()

@allure.step("Check file changed seconds ago")
@screenshot_step
def step_check_file_edit_seconds_ago(page: Page, filename: str):
    files_page: FilesPage = FilesPage(page)
    
    last_modified: Locator = files_page.file_list.get_file_entry(filename).last_modified
    
    expect(last_modified).to_be_visible()
    expect(last_modified).to_have_text(files_page.file_list.get_modified_seconds_ago())
    
@allure.step("Grace period for file upload")
@screenshot_step
def step_upload_grace_period(page: Page, timeout: float):
    page.wait_for_timeout(timeout)

@allure.step("Open file dropdown menu")
@screenshot_step
def step_open_file_dropdown_menu(page: Page, filename: str):
    files_page: FilesPage = FilesPage(page)
    
    file_entry: FileEntry = files_page.file_list.get_file_entry(filename)
    
    expect(file_entry).to_be_attached()
    file_entry.page_part_locator.scroll_into_view_if_needed()
    expect(file_entry).to_be_visible()
    
    expect(file_entry.drop_down_button).to_be_visible()
    file_entry.drop_down_button.click()

@allure.step("Click open details")
@screenshot_step
def step_click_open_details(page: Page):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.file_dropdown_popper.details_entry).to_be_visible()
    files_page.file_dropdown_popper.details_entry.click()

@allure.step("Open activity tab")
@screenshot_step
def step_open_activity_tab(page: Page):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.sidebar_right.activity_tab_button).to_be_visible()
    files_page.sidebar_right.activity_tab_button.click()

@allure.step("Open OpenProject tab")
@screenshot_step
def step_open_openproject_tab(page: Page):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.sidebar_right.open_project_tab_button).to_be_visible()
    files_page.sidebar_right.open_project_tab_button.click()

@allure.step("Check OpenProject integration not established")
@screenshot_step
def step_check_connect_to_openproject_visible(page: Page):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.sidebar_right.open_project_tab.connect_to_openproject_button).to_be_visible()

@allure.step("Check create link work package visible")
@screenshot_step
def step_check_create_link_work_package_visible(page: Page):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.sidebar_right.open_project_tab.create_link_to_work_package_button).to_be_visible()

@allure.step("Click create and link a new work package")
@screenshot_step
def step_click_create_link_work_package(page: Page):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.sidebar_right.open_project_tab.create_link_to_work_package_button).to_be_visible()
    files_page.sidebar_right.open_project_tab.create_link_to_work_package_button.click()
    expect(files_page.create_work_package_modal).to_be_visible()

@allure.step("Fill work package project")
@screenshot_step
def step_fill_work_package_project(page: Page, project_name: str):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.create_work_package_modal.project_input).to_be_visible()
    files_page.create_work_package_modal.project_input.clear()
    files_page.create_work_package_modal.project_input.type(project_name)
    expect(files_page.create_work_package_modal.project_input).to_have_value(project_name)

@allure.step("Select work package project")
@screenshot_step
def step_select_work_package_project(page: Page, project_name: str):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.create_work_package_modal.project_input).to_be_visible()
    expect(files_page.create_work_package_modal.get_project_entry(project_name)).to_be_visible()
    files_page.create_work_package_modal.get_project_entry(project_name).click()

@allure.step("Fill work package subject")
@screenshot_step
def step_fill_work_package_subject(page: Page, subject: str):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.create_work_package_modal.subject_input).to_be_visible()
    files_page.create_work_package_modal.subject_input.clear()
    files_page.create_work_package_modal.subject_input.type(subject)
    expect(files_page.create_work_package_modal.subject_input).to_have_value(subject)

@allure.step("Click create work package")
@screenshot_step
def step_click_create_work_package(page: Page):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.create_work_package_modal.create_button).to_be_visible()
    files_page.create_work_package_modal.create_button.click()
    expect(files_page.create_work_package_modal).not_to_be_visible()

@allure.step("Check work package created")
@screenshot_step
def step_check_work_package_created(page: Page, subject: str):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.sidebar_right.open_project_tab.get_work_package_by_subject(subject)).to_be_visible()

@allure.step("Select emoji")
@screenshot_step
def step_select_emoji(page: Page, emoji: str):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.sidebar_right.activity_tab.emoji_list).to_be_visible()
    expect(files_page.sidebar_right.activity_tab.get_emoji_entry(emoji)).to_be_visible()
    files_page.sidebar_right.activity_tab.get_emoji_entry(emoji).click()

@allure.step("Select user")
@screenshot_step
def step_select_user(page: Page, firstname: str, lastname: str):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.sidebar_right.activity_tab.autocomplete_list).to_be_visible()
    expect(files_page.sidebar_right.activity_tab.get_autocomplete_entry(firstname=firstname, lastname=lastname)).to_be_visible()
    files_page.sidebar_right.activity_tab.get_autocomplete_entry(firstname=firstname, lastname=lastname).click()

@allure.step("Type comment")
@screenshot_step
def step_type_comment(page: Page, comment: str):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.sidebar_right.activity_tab.comment_input).to_be_visible()
    files_page.sidebar_right.activity_tab.comment_input.type(comment)

@allure.step("Send comment with enter")
@screenshot_step
def step_send_comment(page: Page):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.sidebar_right.activity_tab.comment_input).to_be_visible()
    files_page.sidebar_right.activity_tab.comment_input.press("Enter")

@allure.step("Check comment exists")
@screenshot_step
def step_check_comment_exists(page: Page, firstname: str, lastname: str, message: str):
    files_page: FilesPage = FilesPage(page)
    
    comment_entry = files_page.sidebar_right.activity_tab.get_comment(firstname=firstname, lastname=lastname, message=message)
    expect(comment_entry).to_be_visible()

@allure.step("Check comment with mentions exists")
@screenshot_step
def step_check_comment_with_mentions_exists(page: Page, firstname: str, lastname: str, mentions: list[str], message: str | None = None):
    files_page: FilesPage = FilesPage(page)
    
    comment_entry = files_page.sidebar_right.activity_tab.get_comment(firstname=firstname, lastname=lastname, message=message, mentions=mentions)
    expect(comment_entry).to_be_visible()

@allure.step("Click move or copy")
@screenshot_step
def step_click_move_or_copy(page: Page):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.file_dropdown_popper.move_copy_entry).to_be_visible()
    files_page.file_dropdown_popper.move_copy_entry.click()
    
    expect(files_page.move_copy_modal).to_be_visible()
    expect(files_page.move_copy_modal.home_button).to_be_visible()

@allure.step("Select folder")
@screenshot_step
def step_open_folder_for_move_copy(page: Page, foldername: str):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.move_copy_modal).to_be_visible()
    expect(files_page.move_copy_modal.get_folder(foldername)).to_be_visible()
    files_page.move_copy_modal.get_folder(foldername).click()

@allure.step("Click move button")
@screenshot_step
def step_click_move_to_folder(page: Page):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.move_copy_modal.move_button).to_be_visible()
    files_page.move_copy_modal.move_button.click()

@allure.step("Goto home folder")
@screenshot_step
def step_goto_home_folder(page: Page):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.file_list.breadcrumb_home).to_be_visible()
    files_page.file_list.breadcrumb_home.click()

@allure.step("Click open with cryptpad")
@screenshot_step
def step_click_open_cryptpad(page: Page):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.file_dropdown_popper.open_cryptpad_entry).to_be_visible()
    files_page.file_dropdown_popper.open_cryptpad_entry.click()

@allure.step("Click open with office")
@screenshot_step
def step_click_open_office(page: Page):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.file_dropdown_popper.open_office_entry).to_be_visible()
    files_page.file_dropdown_popper.open_office_entry.click()

@allure.step("Check share not available")
@screenshot_step
def step_check_share_not_available(page: Page, filename: str):
    files_page: FilesPage = FilesPage(page)
    
    file_entry: FileEntry = files_page.file_list.get_file_entry(filename)
    
    expect(file_entry).to_be_visible()
    expect(file_entry.share_button).not_to_be_visible()

@allure.step("Open file share menu")
@screenshot_step
def step_open_share_menu(page: Page, filename: str):
    files_page: FilesPage = FilesPage(page)
    
    file_entry: FileEntry = files_page.file_list.get_file_entry(filename)
    
    expect(file_entry).to_be_attached()
    file_entry.page_part_locator.scroll_into_view_if_needed()
    expect(file_entry).to_be_visible()
    
    expect(file_entry.share_button).to_be_visible()
    file_entry.share_button.click()
    
    expect(files_page.sidebar_right.sharing_tab).to_be_visible()

@allure.step("Fill share mail")
@screenshot_step
def step_select_share_mail(page: Page, share_input: str, mail: str):
    files_page: FilesPage = FilesPage(page)
    
    files_page.sidebar_right.sharing_tab.share_input_field.click()
    expect(files_page.sidebar_right.sharing_tab.share_spinner).not_to_be_visible(timeout=20000)
    files_page.sidebar_right.sharing_tab.share_input_field.type(share_input)
    expect(files_page.sidebar_right.sharing_tab.share_spinner).not_to_be_visible(timeout=20000)
    expect(files_page.sidebar_right.sharing_tab.get_sharing_search_list_entry(mail)).to_be_visible(timeout=20000)
    files_page.sidebar_right.sharing_tab.share_input_field.press("Enter")

@allure.step("Enable password")
@screenshot_step
def step_sharing_enable_password(page: Page):
    files_page: FilesPage = FilesPage(page) 
    expect(files_page.sidebar_right.sharing_tab.set_password_checkbox).to_be_visible()
    if files_page.sidebar_right.sharing_tab.set_password_checkbox_checked.is_visible():
        return
    files_page.sidebar_right.sharing_tab.set_password_checkbox.click()
    expect(files_page.sidebar_right.sharing_tab.set_password_checkbox_checked).to_be_visible()

@allure.step("Check password enforced")
@screenshot_step
def step_sharing_password_enforced(page: Page):
    files_page: FilesPage = FilesPage(page) 
    expect(files_page.sidebar_right.sharing_tab.set_password_checkbox_checked).to_be_visible()
    expect(files_page.sidebar_right.sharing_tab.set_password_checkbox_disabled).to_be_visible()

@allure.step("Check password not enforced")
@screenshot_step
def step_sharing_password_not_enforced(page: Page):
    files_page: FilesPage = FilesPage(page) 
    expect(files_page.sidebar_right.sharing_tab.set_password_checkbox).to_be_visible()
    expect(files_page.sidebar_right.sharing_tab.set_password_checkbox_disabled).not_to_be_visible()

@allure.step("Check expiry")
@screenshot_step
def step_sharing_check_expiry(page: Page):
    files_page: FilesPage = FilesPage(page) 
    expect(files_page.sidebar_right.sharing_tab.expiry_checkbox).to_be_visible()
    if files_page.sidebar_right.sharing_tab.expiry_checkbox_checked.is_visible():
        return
    files_page.sidebar_right.sharing_tab.expiry_checkbox.click()
    expect(files_page.sidebar_right.sharing_tab.expiry_checkbox_checked).to_be_visible()

@allure.step("Check expiry days")
@screenshot_step
def step_sharing_check_expiry_days(page: Page, days: int):
    files_page: FilesPage = FilesPage(page) 
    
    expect(files_page.sidebar_right.sharing_tab.expiry_input).to_be_visible()
    min: str = files_page.sidebar_right.sharing_tab.expiry_input.get_attribute("min")
    value: str = files_page.sidebar_right.sharing_tab.expiry_input.input_value()
    
    min_time: datetime.datetime = datetime.datetime.strptime(min, "%Y-%m-%d")
    value_time: datetime.datetime = datetime.datetime.strptime(value, "%Y-%m-%d")
    
    delta_days = (value_time - min_time).days + 1
    
    assert delta_days == days

@allure.step("Check expiry checked")
@screenshot_step
def step_sharing_expiry_checked(page: Page):
    files_page: FilesPage = FilesPage(page) 
    expect(files_page.sidebar_right.sharing_tab.expiry_checkbox_checked).to_be_visible()

@allure.step("Check expiry not checked")
@screenshot_step
def step_sharing_expiry_not_checked(page: Page):
    files_page: FilesPage = FilesPage(page) 
    expect(files_page.sidebar_right.sharing_tab.expiry_checkbox).to_be_visible()
    expect(files_page.sidebar_right.sharing_tab.expiry_checkbox_checked).not_to_be_visible()

@allure.step("Check expiry enforced")
@screenshot_step
def step_sharing_expiry_enforced(page: Page):
    files_page: FilesPage = FilesPage(page) 
    expect(files_page.sidebar_right.sharing_tab.expiry_checkbox_checked).to_be_visible()
    expect(files_page.sidebar_right.sharing_tab.expiry_checkbox_disabled).to_be_visible()

@allure.step("Check expiry not enforced")
@screenshot_step
def step_sharing_expiry_not_enforced(page: Page):
    files_page: FilesPage = FilesPage(page) 
    expect(files_page.sidebar_right.sharing_tab.expiry_checkbox).to_be_visible()
    expect(files_page.sidebar_right.sharing_tab.expiry_checkbox_disabled).not_to_be_visible()

@allure.step("Fill password")
@screenshot_step
def step_sharing_fill_password(page: Page, password: str):
    files_page: FilesPage = FilesPage(page) 
    
    expect(files_page.sidebar_right.sharing_tab.set_password_input).to_be_visible()
    files_page.sidebar_right.sharing_tab.set_password_input.clear()
    files_page.sidebar_right.sharing_tab.set_password_input.type(password)
    
   
@allure.step("Click save share button")
@screenshot_step
def step_click_save_share_button(page: Page):
    files_page: FilesPage = FilesPage(page) 
    expect(files_page.sidebar_right.sharing_tab.share_save).to_be_visible()
    files_page.sidebar_right.sharing_tab.share_save.click()
    
@allure.step("Check extern shared entry visible")
@screenshot_step
def step_extern_shared_entry_visible(page: Page, mail: str):
    files_page: FilesPage = FilesPage(page)
    expect(files_page.sidebar_right.sharing_tab.get_sharing_shared_extern_entry(mail)).to_be_visible()

@allure.step("Check intern shared entry visible")
@screenshot_step
def step_intern_shared_entry_visible(page: Page, mail: str):
    files_page: FilesPage = FilesPage(page)
    expect(files_page.sidebar_right.sharing_tab.get_sharing_shared_entry(mail)).to_be_visible()

@allure.step("Open shares tab")
@screenshot_step
def step_open_shares_tab(page: Page):
    files_page: FilesPage = FilesPage(page)

    expect(files_page.sidebar_left.shares_tab_button).to_be_visible()
    files_page.sidebar_left.shares_tab_button.click()
    
@allure.step("Open file chat menu")
@screenshot_step
def step_open_chat_menu(page: Page):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.sidebar_right.chat_tab_button).to_be_visible(timeout=20000)
    files_page.sidebar_right.chat_tab_button.click()

@allure.step("Join conversation")
@screenshot_step
def step_join_conversation(page: Page):
    files_page: FilesPage = FilesPage(page)

    expect(files_page.sidebar_right.chat_tab.join_conversation_button).to_be_visible()
    files_page.sidebar_right.chat_tab.join_conversation_button.click()

@allure.step("Enter chat message")
@screenshot_step
def step_enter_chat_message(page: Page, chat_message: str):
    files_page: FilesPage = FilesPage(page)

    expect(files_page.sidebar_right.chat_tab.chat_message_input).to_be_visible(timeout=20000)
    files_page.sidebar_right.chat_tab.chat_message_input.clear()
    files_page.sidebar_right.chat_tab.chat_message_input.type(chat_message)

@allure.step("Send chat message")
@screenshot_step
def step_send_chat_message(page: Page):
    files_page: FilesPage = FilesPage(page)

    expect(files_page.sidebar_right.chat_tab.chat_message_send_button).to_be_visible()
    files_page.sidebar_right.chat_tab.chat_message_send_button.click()

@allure.step("Check chat message exists")
@screenshot_step
def step_check_chat_message_exists(page: Page, chat_message: str):
    files_page: FilesPage = FilesPage(page)

    expect(files_page.sidebar_right.chat_tab.get_chat_message(message=chat_message)).to_be_visible()

@allure.step("Close right sidebar")
@screenshot_step
def step_close_sidebar_right(page: Page):
    files_page: FilesPage = FilesPage(page)
    files_page.sidebar_right.close_button.click()

@allure.step("Check file cannot be shared")
@screenshot_step
def step_no_share_file(page: Page, mail: str):
    files_page: FilesPage = FilesPage(page)
    
    files_page.sidebar_right.sharing_tab.share_input_field.click()
    expect(files_page.sidebar_right.sharing_tab.share_spinner).not_to_be_visible(timeout=15000)
    files_page.sidebar_right.sharing_tab.share_input_field.type(mail)
    expect(files_page.sidebar_right.sharing_tab.share_spinner).not_to_be_visible(timeout=15000)
    
    expect(files_page.sidebar_right.sharing_tab.get_sharing_search_list_entry(mail)).not_to_be_visible()

@allure.step("Check share recommendation")
@screenshot_step
def step_check_share_file_recommend(page: Page, mail: str):
    files_page: FilesPage = FilesPage(page)
    
    files_page.sidebar_right.sharing_tab.share_input_field.click()
    files_page.sidebar_right.sharing_tab.share_input_field.type(mail.split("@")[0])
    
    expect(files_page.sidebar_right.sharing_tab.get_sharing_search_list_entry(mail)).to_be_attached()
    files_page.sidebar_right.sharing_tab.get_sharing_search_list_entry(mail).scroll_into_view_if_needed()
    expect(files_page.sidebar_right.sharing_tab.get_sharing_search_list_entry(mail)).to_be_visible()

@allure.step("Check cryptpad editor opened")
@screenshot_step
def step_check_cryptpad_editor_opened(page: Page):
    cryptpad_page: CryptpadPage = CryptpadPage(page)
    
    expect(cryptpad_page.frame.owner).to_be_visible(timeout=1000*60)
    expect(cryptpad_page.sbox_frame.owner).to_be_visible(timeout=1000*60)
    expect(cryptpad_page.app_frame.owner).to_be_visible(timeout=1000*60)
    expect(cryptpad_page.app_container).to_be_visible(timeout=1000*60)
    
    return page.title().split(" - ")[0]

@allure.step("Close cryptpad editor")
@screenshot_step
def step_close_cryptpad(page: Page) -> str:
    cryptpad_page: CryptpadPage = CryptpadPage(page)
    
    expect(cryptpad_page.close_button).to_be_visible()
    cryptpad_page.close_button.click()
    
    FilesPage(page).validate()

@allow_fail
@allure.step("Close welcome modal")
@screenshot_step
def step_close_welcome_modal(page: Page):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.office_viewer.user_welcome_modal.last_slide_indicator).to_be_visible(timeout=5000)
    files_page.office_viewer.user_welcome_modal.last_slide_indicator.click()
    
    expect(files_page.office_viewer.user_welcome_modal.last_slide_close_button).to_be_visible()
    files_page.office_viewer.user_welcome_modal.last_slide_close_button.click()

@allure.step("Edit office text file")
@screenshot_step
def step_edit_office_text_file(page: Page):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.office_viewer.content_block).to_be_visible()
    files_page.office_viewer.content_block.click()
    
    page.keyboard.press("Enter")
    page.keyboard.type("Neue Zeile")

@allure.step("Check office viewer opened")
@screenshot_step
def step_check_office_viewer_opened(page: Page):
    files_page: FilesPage = FilesPage(page)
    
    allow_fail(expect(files_page.office_viewer.loading_overlay).to_be_visible)(timeout=5000)
    expect(files_page.office_viewer.loading_overlay).not_to_be_visible(timeout=40000)
    
    step_close_welcome_modal(page=page)
    
    expect(files_page.office_viewer.frame).to_be_visible()

def step_check_office_font_exists(page: Page, font_name: str):
    step_open_font_dropdown(page=page)
    step_check_font_exists(page=page, font_name=font_name)
    step_close_font_dropdown(page=page)

@allure.step("Open font dropdown")
@screenshot_step
def step_open_font_dropdown(page: Page):
    files_page: FilesPage = FilesPage(page)
    expect(files_page.office_viewer.font_combobox_dropdown_button).to_be_visible(timeout=30000)
    files_page.office_viewer.font_combobox_dropdown_button.click()
    expect(files_page.office_viewer.font_combobox_dropdown).to_be_visible()
    
@allure.step("Check font exists")
@screenshot_step
def step_check_font_exists(page: Page, font_name: str):
    files_page: FilesPage = FilesPage(page)
    font = files_page.office_viewer.find_font(font_name=font_name)
    expect(font).to_be_attached()
    font.scroll_into_view_if_needed()
    expect(font).to_be_visible()
    
@allure.step("Close font dropdown")
@screenshot_step
def step_close_font_dropdown(page: Page):
    files_page: FilesPage = FilesPage(page)
    expect(files_page.office_viewer.font_combobox_dropdown).to_be_visible()
    page.keyboard.press("Escape")

def reopen_file_wrapper(url_portal: str, filename: str):
    
    def wrapper(page, *_, **__):
        step_open_page(page=page, url_portal=url_portal)
        step_open_file(page=page, filename=filename)
        step_check_office_viewer_opened(page=page)
        
    return wrapper

@allure.step("Close office viewer")
@screenshot_step
def step_close_document(page: Page) -> str:
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.office_viewer.close_button).to_be_visible()
    files_page.office_viewer.close_button.click()
    
    expect(files_page.office_viewer).not_to_be_visible()

@allure.step("Delete file")
@screenshot_step
def step_click_delete(page: Page):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.file_dropdown_popper.delete_file_entry).to_be_visible()
    files_page.file_dropdown_popper.delete_file_entry.click()
    
@allure.step("Rename file")
@screenshot_step
def step_click_rename_file(page: Page):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.file_dropdown_popper.rename_file_entry).to_be_visible()
    files_page.file_dropdown_popper.rename_file_entry.click()

@allure.step("Fill rename input")
@screenshot_step
def step_rename_file(page: Page, old_filename: str, filename: str):
    files_page: FilesPage = FilesPage(page)
    
    file_entry: FileEntry = files_page.file_list.get_file_entry(old_filename)
    expect(file_entry).to_be_visible()
    expect(file_entry.rename_file_input).to_be_visible()
    file_entry.rename_file_input.clear()
    file_entry.rename_file_input.type(filename)
    file_entry.rename_file_input.press("Enter")

@allure.step("Check delete not available")
@screenshot_step
def step_check_delete_not_available(page: Page):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.file_dropdown_popper).to_be_visible()
    expect(files_page.file_dropdown_popper.delete_file_entry).not_to_be_visible()

@allure.step("Create folder")
@screenshot_step
def step_create_folder(page: Page, foldername: str):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.file_list.new_file_button).to_be_visible()
    files_page.file_list.new_file_button.click()
    expect(files_page.new_file_popper.create_folder).to_be_visible()
    files_page.new_file_popper.create_folder.click()
    
    expect(files_page.new_entry_modal).to_be_visible()
    page.wait_for_timeout(1000*3)
    
    if foldername:
        expect(files_page.new_entry_modal.name_input).to_be_visible()
        files_page.new_entry_modal.name_input.clear()
        files_page.new_entry_modal.name_input.type(foldername)
        
        expect(files_page.new_entry_modal.name_input).to_have_value(foldername)
        page.wait_for_timeout(1000*1)
    
    files_page.new_entry_modal.create_button.click()

@allure.step("Create new Cryptpad file")
@screenshot_step
def step_create_new_cryptpad_file(page: Page) -> str:
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.file_list.new_file_button).to_be_visible()
    files_page.file_list.new_file_button.click()
    expect(files_page.new_file_popper.create_cryptpad_file).to_be_visible()
    files_page.new_file_popper.create_cryptpad_file.click()

@allure.step("Create document")
def step_create_document(page: Page, filename: str | None = None):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.file_list.new_file_button).to_be_visible()
    files_page.file_list.new_file_button.click()
    expect(files_page.new_file_popper.create_document).to_be_visible()
    files_page.new_file_popper.create_document.click()
    
    expect(files_page.new_entry_modal).to_be_visible()
    page.wait_for_timeout(1000*3)
    
    if filename:
        expect(files_page.new_entry_modal.name_input).to_be_visible()
        files_page.new_entry_modal.name_input.clear()
        files_page.new_entry_modal.name_input.type(filename)
        
        expect(files_page.new_entry_modal.name_input).to_have_value(filename)
        page.wait_for_timeout(1000*1)
    
    files_page.new_entry_modal.create_button.click()

@allure.step("Check general availability")
@screenshot_step
def step_general_availability(page: Page):
    files_page: FilesPage = FilesPage(page)
    expect(files_page.app_body).to_be_visible()
    expect(files_page.sidebar_left).to_be_visible()
    expect(files_page.file_list).to_be_visible()

@allure.step("Check error after file upload")
@screenshot_step
def step_check_file_upload_error(page: Page):
    files_page: FilesPage = FilesPage(page)
    expect(files_page.upload_error_toast).to_be_visible()

@allure.step("Upload font")
@screenshot_step
def step_upload_font(page: Page, folder: str, font_file: str):
    settings_page = RichDocumentsSettingsPage(page)
    
    expect(settings_page.upload_font_button).to_be_visible()
    settings_page.upload_font_button.scroll_into_view_if_needed()
    
    expect(settings_page.upload_font_input).to_be_attached()
    settings_page.upload_font_input.set_input_files(f"{folder}{os.sep}{font_file}")

@allure.step("Open user menu")
@screenshot_step
def step_open_user_menu(page: Page):
    files_page: FilesPage = FilesPage(page)
    
    files_page.header.user_menu_button.click()
    
@allure.step("Click on personal settings")
@screenshot_step
def step_click_personal_settings(page: Page):
    files_page: FilesPage = FilesPage(page)
    files_page.user_menu.settings_button.click()
    
@allure.step("Click on admin settings")
@screenshot_step
def step_click_admin_settings(page: Page):
    files_page: FilesPage = FilesPage(page)
    files_page.user_menu.admin_settings_button.click()
    
@allure.step("Open security settings")
@screenshot_step
def step_open_security_settings(page: Page):
    settings_page: SettingsPage = SettingsPage(page)
    
    expect(settings_page.security_tile).to_be_visible()
    settings_page.security_tile.click()
    
@allure.step("Open admin office settings")
@screenshot_step
def step_open_office_settings(page: Page):
    settings_page: SettingsPage = SettingsPage(page)
    settings_page.admin_rich_documents_tile.click()
    
@allure.step("Open admin groupfolders settings")
@screenshot_step
def step_open_groupfolders_settings(page: Page):
    settings_page: SettingsPage = SettingsPage(page)
    settings_page.admin_groupfolders_tile.click()
    
@allure.step("Check that font is visible")
@screenshot_step
def step_check_font_visible(page: Page, font_file: str):
    settings_page = RichDocumentsSettingsPage(page)
    
    font = settings_page.find_font(font_file=font_file)
    expect(font).to_be_visible()
    
@allure.step("Check that font is not visible")
@screenshot_step
def step_check_font_not_visible(page: Page, font_file: str):
    settings_page = RichDocumentsSettingsPage(page)
    
    font = settings_page.find_font(font_file=font_file)
    expect(font).not_to_be_visible()
    
@allure.step("Click delete font")
@screenshot_step
def step_click_delete_font(page: Page, font_file: str):
    settings_page = RichDocumentsSettingsPage(page)
    delete_font = settings_page.font_delete_button(font_file=font_file)
    delete_font.click()

@allure.step("Fill groupfolder name")
@screenshot_step
def step_fill_groupfolder_name(page: Page, groupfolder_name: str):
    settings_page = GroupfoldersSettingsPage(page)
    
    expect(settings_page.new_group_name_input).to_be_visible()
    settings_page.new_group_name_input.clear()
    settings_page.new_group_name_input.type(groupfolder_name)

@allure.step("Click create groupfolder")
@screenshot_step
def step_click_create_groupfolder(page: Page):
    settings_page = GroupfoldersSettingsPage(page)
    
    expect(settings_page.new_group_submit).to_be_visible()
    settings_page.new_group_submit.click()

@allure.step("Check groupfolder exists")
@screenshot_step
def step_check_groupfolder_exists(page: Page, groupfolder_name: str):
    settings_page = GroupfoldersSettingsPage(page)
    
    groupfolder = settings_page.get_groupfolder_entry(groupfolder_name=groupfolder_name)
    expect(groupfolder).to_be_visible()

@allure.step("Click on groupfolder groups to edit groups")
@screenshot_step
def step_click_open_groupfolder_groups(page: Page, groupfolder_name: str):
    settings_page = GroupfoldersSettingsPage(page)
    
    groupfolder = settings_page.get_groupfolder_entry(groupfolder_name=groupfolder_name)
    expect(groupfolder).to_be_visible()
    expect(groupfolder.group_edit_button).to_be_visible()
    groupfolder.group_edit_button.click()
    
    expect(groupfolder.group_edit).to_be_visible()

@allure.step("Add group to groupfolder")
@screenshot_step
def step_add_group_to_groupfolder(page: Page, groupfolder_name: str, group_name: str):
    settings_page = GroupfoldersSettingsPage(page)
    groupfolder_entry: AdminGroupfolderEntry = settings_page.get_groupfolder_entry(groupfolder_name=groupfolder_name)
    expect(groupfolder_entry).to_be_visible()
    groupfolder_entry.group_input.clear()
    groupfolder_entry.group_input.type(group_name)
    groupfolder_entry.group_input.press("Enter")
    
    group_entry: AdminGroupfolderGroupEntry = groupfolder_entry.get_group_entry(group_name=group_name)
    expect(group_entry).to_be_visible()

@allure.step("Remove permission write from group")
@screenshot_step
def step_remove_permission_write_from_group(page: Page, groupfolder_name: str, group_name: str):
    settings_page = GroupfoldersSettingsPage(page)
    group_entry: AdminGroupfolderGroupEntry = settings_page.get_groupfolder_entry(groupfolder_name=groupfolder_name).get_group_entry(group_name=group_name)
    expect(group_entry).to_be_visible()
    expect(group_entry.write_permission).to_be_visible()
    group_entry.write_permission.uncheck()

@allure.step("Remove permission share from group")
@screenshot_step
def step_remove_permission_share_from_group(page: Page, groupfolder_name: str, group_name: str):
    settings_page = GroupfoldersSettingsPage(page)
    group_entry: AdminGroupfolderGroupEntry = settings_page.get_groupfolder_entry(groupfolder_name=groupfolder_name).get_group_entry(group_name=group_name)
    expect(group_entry).to_be_visible()
    expect(group_entry.share_permission).to_be_visible()
    group_entry.share_permission.uncheck()

@allure.step("Remove permission delete from group")
@screenshot_step
def step_remove_permission_delete_from_group(page: Page, groupfolder_name: str, group_name: str):
    settings_page = GroupfoldersSettingsPage(page)
    group_entry: AdminGroupfolderGroupEntry = settings_page.get_groupfolder_entry(groupfolder_name=groupfolder_name).get_group_entry(group_name=group_name)
    expect(group_entry).to_be_visible()
    expect(group_entry.delete_permission).to_be_visible()
    group_entry.delete_permission.uncheck()

@allure.step("Close groups")
@screenshot_step
def step_close_groups(page: Page):
    settings_page = GroupfoldersSettingsPage(page)
    expect(settings_page.new_group_name_input).to_be_attached()
    settings_page.new_group_name_input.click(force=True)

@allure.step("Toggle file select")
@screenshot_step
def step_toggle_select_file(page: Page, filename: str):
    files_page: FilesPage = FilesPage(page)
    
    file_entry: FileEntry = files_page.file_list.get_file_entry(filename)
    expect(file_entry).to_be_visible()
    expect(file_entry.checkbox).to_be_visible()
    file_entry.checkbox.click()

@allure.step("Check file selected")
@screenshot_step
def step_check_file_selected(page: Page, filename: str):
    files_page: FilesPage = FilesPage(page)
    
    file_entry: FileEntry = files_page.file_list.get_file_entry(filename)
    expect(file_entry).to_be_visible()
    expect(file_entry.checkbox_checked).to_be_visible()

@allure.step("Click batch actions button")
@screenshot_step
def step_click_batch_actions(page: Page):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.file_list.batch_actions_dropdown_button).to_be_visible()
    files_page.file_list.batch_actions_dropdown_button.click()

@allure.step("Click zip files")
@screenshot_step
def step_click_zip_files(page: Page):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.batch_actions_popper.zip_files).to_be_visible()
    files_page.batch_actions_popper.zip_files.click()
    
    expect(files_page.compress_files_modal).to_be_attached()
    files_page.compress_files_modal.filename_input.click(force=True)
    expect(files_page.compress_files_modal).to_be_visible()
    expect(files_page.compress_files_modal.dialog).to_be_visible()

@allure.step("Fill zip file name")
@screenshot_step
def step_fill_zip_file_name(page: Page, filename: str):
    files_page: FilesPage = FilesPage(page)
    expect(files_page.compress_files_modal).to_be_visible()
    expect(files_page.compress_files_modal.filename_input).to_be_visible()
    files_page.compress_files_modal.filename_input.fill("")
    files_page.compress_files_modal.filename_input.type(filename)

@allure.step("Click compress files button")
@screenshot_step
def step_click_compress_files_button(page: Page):
    files_page: FilesPage = FilesPage(page)
    expect(files_page.compress_files_modal).to_be_visible()
    expect(files_page.compress_files_modal.compress_button).to_be_visible()
    files_page.compress_files_modal.compress_button.click()
    expect(files_page.compress_files_modal).not_to_be_visible()

@allure.step("Fill new app password name")
@screenshot_step
def step_fill_app_password_name(page: Page, name: str):
    security_settings_page: SecuritySettingsPage = SecuritySettingsPage(page)
    
    expect(security_settings_page.app_password_name_input).to_be_visible()
    security_settings_page.app_password_name_input.clear()
    security_settings_page.app_password_name_input.type(name)
    expect(security_settings_page.app_password_name_input).to_have_value(name)

@allure.step("Click create app password")
@screenshot_step
def step_click_create_app_password(page: Page):
    security_settings_page: SecuritySettingsPage = SecuritySettingsPage(page)
    
    expect(security_settings_page.app_password_create_button).to_be_visible()
    security_settings_page.app_password_create_button.click()
    
    expect(security_settings_page.app_password_modal).to_be_visible()

@allure.step("Get app name")
@screenshot_step
def step_get_app_name(page: Page) -> str:
    security_settings_page: SecuritySettingsPage = SecuritySettingsPage(page)
    
    expect(security_settings_page.app_password_modal.name_input).to_be_visible()
    return security_settings_page.app_password_modal.name_input.input_value()

@allure.step("Get app password")
@screenshot_step
def step_get_app_password(page: Page) -> str:
    security_settings_page: SecuritySettingsPage = SecuritySettingsPage(page)
    
    expect(security_settings_page.app_password_modal.password_input).to_be_visible()
    return security_settings_page.app_password_modal.password_input.input_value()

@allure.step("Open file settings")
@screenshot_step
def step_open_file_settings(page: Page):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.sidebar_left.file_settings_entry).to_be_visible()
    files_page.sidebar_left.file_settings_entry.click()
    
@allure.step("Close file settings")
@screenshot_step
def step_close_file_settings(page: Page):
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.file_settings_modal.close_button).to_be_visible()
    files_page.file_settings_modal.close_button.click()

@allure.step("Get WebDAV URL")
@screenshot_step
def step_get_webdav_url(page: Page) -> str:
    files_page: FilesPage = FilesPage(page)
    
    expect(files_page.file_settings_modal.webdav_url_input).to_be_visible()
    return files_page.file_settings_modal.webdav_url_input.input_value()

@allure.step("Check OpenProject integration not established")
@screenshot_step
def step_check_openproject_integration_not_established(page: Page):
    open_project_integration_page: OpenProjectIntegrationPage = OpenProjectIntegrationPage(page)
    
    expect(open_project_integration_page.connect_to_openproject_button).to_be_visible()
    expect(open_project_integration_page.disconnect_openproject_button).not_to_be_visible()

@allure.step("Click Connect to OpenProject button")
@screenshot_step
def step_click_connect_to_openproject(page: Page):
    open_project_integration_page: OpenProjectIntegrationPage = OpenProjectIntegrationPage(page)
    
    expect(open_project_integration_page.connect_to_openproject_button).to_be_visible()
    open_project_integration_page.connect_to_openproject_button.click()

@allure.step("Click authorize")
@screenshot_step
def step_click_authorize(page: Page):
    authorization_page: OpenProjectIntegrationAuthorizationPage = OpenProjectIntegrationAuthorizationPage(page)
    
    expect(authorization_page.authorize_button).to_be_visible()
    authorization_page.authorize_button.click()

@allure.step("Check authorization success")
@screenshot_step
def step_check_authorization_success(page: Page):
    open_project_integration_page: OpenProjectIntegrationPage = OpenProjectIntegrationPage(page)
    
    expect(open_project_integration_page.connect_to_openproject_button).not_to_be_visible()
    expect(open_project_integration_page.disconnect_openproject_button).to_be_visible()