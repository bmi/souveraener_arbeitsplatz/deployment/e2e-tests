# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page, Response
import pytest
import requests
from tests.config.config import Config
from tests.utils import screenshot_step
from pages.base.base import expect
import allure
import re
import subprocess

@allure.step("Open base domain")
@screenshot_step
def step_open_base_domain(page: Page, base_domain: str):
    page.goto(f"https://{base_domain}/")

@allure.step("Check portal url")
@screenshot_step
def step_check_portal_url(page: Page, url_portal: str):
    expect(page).to_have_url(re.compile("^" + re.escape(url_portal)))

@allure.step("Set kubectl context")
def step_set_kubectl_context(cluster: str):
    subprocess.run(
        f"kubectl config use-context {cluster}",
        shell=True,
        capture_output=True,
        text=True,
        check=True
    )

@allure.step("Get pod images")
def step_get_pod_images(namespace: str) -> list:
    result = subprocess.run(
        f"kubectl get pods -n {namespace} -o jsonpath=\"{{.items[*].spec['initContainers', 'containers'][*].image}}\"",
        shell=True,
        capture_output=True,
        text=True,
        check=True
    )
    images = result.stdout.split()
    images = list(set(images))
    images.sort()
    
    allure.attach("\n".join(images), "image list", allure.attachment_type.TEXT)
    
    return images

@allure.step("Check image registry")
def step_check_image_registry(images: list[str], is_enterprise: bool):
    image_registries: list[str] = [
        "registry.open-de.sk/docker.io",
        "registry.open-de.sk/registry.opencode.de"
    ]
    ee_registries: list[str] = [
        "registry.opencode.de/zendis"
    ]
    if is_enterprise:
        image_registries.extend(ee_registries)
        
    for image in images:
        registry_found: bool = False
        for registry in image_registries:
            if image.startswith(registry):
                registry_found = True
                break
        if registry_found: continue
        
        image_registries_str: str = "\n".join([f"- {registry}" for registry in image_registries])
        raise AssertionError(f"Image registry of '{image}' not in: \n{image_registries_str}")
    
    with allure.step(f"Checked {len(images)} unique images."):
        pass

@allure.step("Check pod restart thresholds")
def step_check_pods(config: Config, pod_restart_thresholds: dict[str, int]):
    namespace: str = config.namespace
    
    result = subprocess.run(
        f"kubectl get pods -n {namespace} -o jsonpath=\"{{.items[*].metadata.name}}\"",
        shell=True,
        capture_output=True,
        text=True,
        check=True
    )
    pod_names = result.stdout.split()
    
    failed: dict[str, tuple[int, int]] = {}
    
    for pod in pod_names:
        threshold: int = 0
        for pod_regex, pod_threshold in pod_restart_thresholds.items():
            if re.match(pod_regex, pod):
                threshold = pod_threshold
                break
        
        count: int = step_check_pod(namespace, pod, threshold)
        
        if count > threshold:
            failed[pod] = (count, threshold)
    
    if failed:
        error_message: str = "Pods with restart count above threshold:\n" + "\n".join([f"{pod}: {count} > {threshold}" for pod, (count, threshold) in failed.items()])
        pytest.fail(error_message, pytrace=False)

def step_check_pod(namespace: str, pod: str, threshold: int) -> int:
    count: int = 0
    try:
        with allure.step(f"Checking restart count of pod {pod}."):
            result = subprocess.run(
                f"kubectl get pod -n {namespace} {pod} -o jsonpath=\"{{.status.containerStatuses[*].restartCount}}\"",
                shell=True,
                capture_output=True,
                text=True,
                check=True
            )
            counts = result.stdout.split()
            for index, count_str in enumerate(counts):
                count = int(count_str)
                allure.attach(f"Restart count: {count}, Threshold: {threshold}", f"restart count {index}", allure.attachment_type.TEXT)
                assert count <= threshold
    except:
        pass
    return count

@allure.step("Open Keycloak admin")
@screenshot_step
def step_open_keycloak_admin(page: Page, url_portal: str) -> Response:
    keycloak_admin_url = url_portal.replace("portal.", "id.")
    if not keycloak_admin_url.endswith("/"):
        keycloak_admin_url += "/"
    keycloak_admin_url += "admin"
    return page.goto(keycloak_admin_url)

@allure.step("Open Keycloak admin/")
@screenshot_step
def step_open_keycloak_admin_with_slash(page: Page, url_portal: str) -> Response:
    keycloak_admin_url = url_portal.replace("portal.", "id.")
    if not keycloak_admin_url.endswith("/"):
        keycloak_admin_url += "/"
    keycloak_admin_url += "admin/"
    return page.goto(keycloak_admin_url)

@allure.step("Check http status code is 404")
def step_check_page_http_404(response: Response):
    assert response.status == 404, f"Expected 404 status code, but got {response.status}"

@allure.step("Check UDM API not available")
def step_check_udm_api_not_available(url_portal: str):
    udm_api_url = url_portal
    if not udm_api_url.endswith("/"): udm_api_url += "/"
    udm_api_url += "univention/udm/users/"
    
    request: requests.Response = requests.get(udm_api_url)
    
    assert request.status_code == 404, f"Expected 404 status code, but got {request.status_code}"