# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page
from pages.base.base import expect

from pages.jitsi.page import *
from tests.config.config import Config
from tests.utils import screenshot_step
import allure

@allure.step("Open Jitsi page")
@screenshot_step
def step_open_page(page: Page, url_portal: str):
    JitsiPage(page).open_page(url_portal)
    JitsiPage(page).validate()

@allure.step("Check general availability")
@screenshot_step
def step_general_availability(page: Page):
    jitsi_home_page: JitsiHomePage = JitsiHomePage(page=page)
    jitsi_home_page.validate()
    expect(jitsi_home_page.settings_button).to_be_visible()
    expect(jitsi_home_page.enter_room_input).to_be_visible()
    expect(jitsi_home_page.topbar_logo).to_be_visible()
    expect(jitsi_home_page.topbar_navigation).to_be_visible()
    expect(jitsi_home_page.topbar_navigation_menu).not_to_be_visible()
    jitsi_home_page.topbar_navigation.click()
    expect(jitsi_home_page.topbar_navigation_menu).to_be_visible()
    jitsi_home_page.topbar_navigation.click(force=True)
    expect(jitsi_home_page.topbar_navigation_menu).not_to_be_visible()

@allure.step("Fill conference name")
@screenshot_step
def step_fill_conference_name(page: Page, conference_name: str):
    jitsi_home_page: JitsiHomePage = JitsiHomePage(page=page)

    expect(jitsi_home_page.enter_room_input).to_be_visible()

    jitsi_home_page.enter_room_input.type(conference_name)
    expect(jitsi_home_page.enter_room_input).to_have_value(conference_name)

@allure.step("Click start conference button")
@screenshot_step
def step_click_start_conference_button(page: Page):
    jitsi_home_page: JitsiHomePage = JitsiHomePage(page=page)

    expect(jitsi_home_page.enter_room_button).to_be_visible()
    
    jitsi_home_page.enter_room_button.click()
    
    JitsiJoinPage(page=page).validate()

@allure.step("Fill display name")
@screenshot_step
def step_fill_display_name(page: Page, display_name: str):
    jitsi_join_page: JitsiJoinPage = JitsiJoinPage(page=page)
    jitsi_join_page.display_name_field.type(display_name)
    expect(jitsi_join_page.display_name_field).to_have_value(display_name)

@allure.step("Check display name")
@screenshot_step
def step_check_display_name(page: Page, display_name: str):
    jitsi_join_page: JitsiJoinPage = JitsiJoinPage(page=page)
    expect(jitsi_join_page.display_name_field).to_have_value(display_name, timeout=1000*10)

@allure.step("Disable audio")
@screenshot_step
def step_disable_audio(page: Page):
    jitsi_join_page: JitsiJoinPage = JitsiJoinPage(page=page)
    expect(jitsi_join_page.audio_preview_button).to_be_visible()
    try:
        expect(jitsi_join_page.audio_preview_button_enabled).to_be_visible()
        jitsi_join_page.audio_preview_button.click()
        expect(jitsi_join_page.audio_preview_button_disabled).to_be_visible()
    except:
        pass

@allure.step("Disable video")
@screenshot_step
def step_disable_video(page: Page):
    jitsi_join_page: JitsiJoinPage = JitsiJoinPage(page=page)
    expect(jitsi_join_page.video_preview_button).to_be_visible()
    try:
        expect(jitsi_join_page.video_preview_button_enabled).to_be_visible()
        jitsi_join_page.video_preview_button.click()
        expect(jitsi_join_page.video_preview_button_disabled).to_be_visible()
    except:
        pass

@allure.step("Enable audio")
@screenshot_step
def step_enable_audio(page: Page):
    jitsi_join_page: JitsiJoinPage = JitsiJoinPage(page=page)
    expect(jitsi_join_page.audio_preview_button).to_be_visible()
    try:
        expect(jitsi_join_page.audio_preview_button_disabled).to_be_visible()
        jitsi_join_page.audio_preview_button.click()
        expect(jitsi_join_page.audio_preview_button_enabled).to_be_visible()
    except:
        pass
    
@allure.step("Enable video")
@screenshot_step
def step_enable_video(page: Page):
    jitsi_join_page: JitsiJoinPage = JitsiJoinPage(page=page)
    expect(jitsi_join_page.video_preview_button).to_be_visible()
    try:
        expect(jitsi_join_page.video_preview_button_disabled).to_be_visible()
        jitsi_join_page.video_preview_button.click()
        expect(jitsi_join_page.video_preview_button_enabled).to_be_visible()
    except:
        pass

@allure.step("Click join conference")
@screenshot_step
def step_click_join_conference(page: Page) -> str:
    jitsi_join_page: JitsiJoinPage = JitsiJoinPage(page=page)
    expect(jitsi_join_page.join_button).to_be_visible()
    jitsi_join_page.join_button.click()

@allure.step("Check if wait for moderator or login dialog appears.")
@screenshot_step
def step_check_wait_for_moderator_dialog(page: Page):
    jitsi_join_page: JitsiJoinPage = JitsiJoinPage(page=page)
    expect(jitsi_join_page.join_button).to_be_visible()

@allure.step("Check joined conference room")
@screenshot_step
def step_check_joined_conference_room(page: Page):
    jitsi_room_page: JitsiRoomPage = JitsiRoomPage(page=page)
    jitsi_room_page.validate()
    
    return jitsi_room_page.get_videoconference_url()

def step_check_participants(page: Page, participants: list[str]):
    jitsi_room_page: JitsiRoomPage = JitsiRoomPage(page=page)
    @allure.step("Open participants pane")
    @screenshot_step
    def open_participants_pane():
        jitsi_room_page.move_mouse()
        expect(jitsi_room_page.participants_button).to_be_visible()
        expect(jitsi_room_page.participants_pane).not_to_be_visible()
        jitsi_room_page.participants_button.click()
        expect(jitsi_room_page.participants_pane).to_be_visible()
    open_participants_pane()
    
    for participant in participants:
        @allure.step(f"Check '{participant}' is participating")
        @screenshot_step
        def check_participant():
            expect(jitsi_room_page.get_participant(name=participant)).to_be_visible()
        check_participant()
    
    
    @allure.step("Close participants pane")
    @screenshot_step
    def close_participans_pane():
        expect(jitsi_room_page.participants_pane).to_be_visible()
        jitsi_room_page.participants_button.click()
        expect(jitsi_room_page.participants_pane).not_to_be_visible()
    close_participans_pane()

@allure.step("Check dominant speaker is being displayed")
@screenshot_step
def step_check_dominant_speaker_displayed(page: Page):
    jitsi_room_page: JitsiRoomPage = JitsiRoomPage(page=page)
    
    expect(jitsi_room_page.dominant_speaker).to_be_visible(timeout=10000)

@allure.step("Check large video is being displayed")
@screenshot_step
def step_check_large_video_displayed(page: Page):
    jitsi_room_page: JitsiRoomPage = JitsiRoomPage(page=page)
    
    expect(jitsi_room_page.large_video_wrapper).to_be_visible(timeout=10000)

@allure.step("Check filmstrip container is displaying avatar")
@screenshot_step
def step_check_filmstrip_avatar(page: Page, participant_name: str):
    jitsi_room_page: JitsiRoomPage = JitsiRoomPage(page=page)
    
    user_filmstrip: Locator = jitsi_room_page.get_filmstrip_by_user(displayname=participant_name)
    
    expect(user_filmstrip).to_be_visible()
    expect(user_filmstrip).to_have_class(re.compile("display-avatar-only"))

@allure.step("Check filmstrip container is displaying video")
@screenshot_step
def step_check_filmstrip_video(page: Page, participant_name: str):
    jitsi_room_page: JitsiRoomPage = JitsiRoomPage(page=page)
    
    user_filmstrip: Locator = jitsi_room_page.get_filmstrip_by_user(displayname=participant_name)
    
    expect(user_filmstrip).to_be_visible()
    expect(user_filmstrip).to_have_class(re.compile("display-video"))
    