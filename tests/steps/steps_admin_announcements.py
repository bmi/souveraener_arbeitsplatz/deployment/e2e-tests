# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page, Locator
from pages.base.base import expect
from pages.management.page import *
from tests.utils import screenshot_step
import allure

@allure.step("Open Announcements page")
@screenshot_step
def step_open_page(page: Page, url_portal: str):
    AnnouncementsPage(page).open_page(url_portal)
    AnnouncementsPage(page).validate()

@allure.step("Click add button")
@screenshot_step
def step_click_add_object(page: Page):
    ox_resources_page: AnnouncementsPage = AnnouncementsPage(page)
    
    expect(ox_resources_page.add_button).to_be_visible()
    ox_resources_page.add_button.click()
    
    edit_announcement: EditAnnouncementPage = EditAnnouncementPage(page)
    
    expect(edit_announcement.internal_name_input).to_be_visible(timeout=10000)
    expect(edit_announcement.first_title_language_input).to_be_visible()
    expect(edit_announcement.first_title_display_name_input).to_be_visible()

@allure.step("Fill internal name")
@screenshot_step
def step_fill_internal_name(page: Page, name: str):
    edit_announcement: EditAnnouncementPage = EditAnnouncementPage(page)
    expect(edit_announcement.internal_name_input).to_be_visible()
    edit_announcement.internal_name_input.type(name)

@allure.step("Fill title language")
@screenshot_step
def step_fill_title_language(page: Page, language_code: str):
    edit_announcement: EditAnnouncementPage = EditAnnouncementPage(page)
    expect(edit_announcement.first_title_language_input).to_be_visible()
    edit_announcement.first_title_language_input.type(language_code)
    
@allure.step("Fill title")
@screenshot_step
def step_fill_title(page: Page, title: str):
    edit_announcement: EditAnnouncementPage = EditAnnouncementPage(page)
    expect(edit_announcement.first_title_display_name_input).to_be_visible()
    edit_announcement.first_title_display_name_input.type(title)
    
@allure.step("Click add groups button")
@screenshot_step
def step_click_add_groups(page: Page):
    edit_announcement: EditAnnouncementPage = EditAnnouncementPage(page)
    expect(edit_announcement.groups_add_button).to_be_visible()
    edit_announcement.groups_add_button.click()
    
    edit_announcement.add_group_modal.get_down_arrow().click()
    edit_announcement.add_group_modal.property_select.wait_for()
    edit_announcement.add_group_modal.get_down_arrow().click()
    
    expect(edit_announcement.add_group_modal.property_select_hidden).to_have_value("None")

@allure.step("Fill group name")
@screenshot_step
def step_fill_group_name(page: Page, group: str):
    edit_announcement: EditAnnouncementPage = EditAnnouncementPage(page)
    
    edit_announcement.add_group_modal.search_field.type(group)
    edit_announcement.add_group_modal.search_field.press("Enter")

@allure.step("Select group from list")
@screenshot_step
def step_select_group(page: Page, group: str):
    edit_announcement: EditAnnouncementPage = EditAnnouncementPage(page)
    
    username_row: Locator = edit_announcement.add_group_modal.get_add_object_table_entry(group)
    expect(username_row).to_be_visible()
    
    username_row.locator("> td > div").click()

@allure.step("Click add group")
@screenshot_step
def step_click_add_group(page: Page):
    edit_announcement: EditAnnouncementPage = EditAnnouncementPage(page)
    
    expect(edit_announcement.add_group_modal.add_button).to_be_visible()
    edit_announcement.add_group_modal.add_button.click()

@allure.step("Click save")
@screenshot_step
def step_click_save(page: Page):
    edit_announcement: EditAnnouncementPage = EditAnnouncementPage(page)
    
    expect(edit_announcement.save_button).to_be_visible()
    edit_announcement.save_button.click()

@allure.step("Check object appears in list")
@screenshot_step
def step_check_object_created(page: Page, name: str):
    announcements_page: AnnouncementsPage = AnnouncementsPage(page)
    
    announcements_page.add_button.wait_for(state="visible")
    
    page.wait_for_timeout(500)
    
    announcements_page.search_field.fill(name)
    announcements_page.search_field.press("Enter")
    
    expect(announcements_page.get_object_table_entry(name)).to_be_visible()
    
@allure.step("Create announcement")
@screenshot_step
def step_create_object(page: Page, name: str, language_code: str, title: str, group: str | None = None):
    step_fill_internal_name(page=page, name=name)
    step_fill_title_language(page=page, language_code=language_code)
    step_fill_title(page=page, title=title)
    if group:
        step_click_add_groups(page=page)
        step_fill_group_name(page=page, group=group)
        step_select_group(page=page, group=group)
        step_click_add_group(page=page)
    step_click_save(page=page)
    step_check_object_created(page=page, name=name)