# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

import allure
from webdav3.client import Client, ResponseErrorCode

from tests.utils import retry

@allure.step("Open WebDAV client")
def step_open_client(url: str, username: str, password: str):
    options = {
        'webdav_hostname': url,
        'webdav_login': username,
        'webdav_password': password
    }
    client: Client = Client(options=options)
    client.verify = False
    
    return client

@allure.step("Check if file exists")
@retry(trys=3, retry_timeout=5, exception_types=[ResponseErrorCode])
def step_check_file_exists(client: Client, path: str):
    assert client.check(path), f"File {path} does not exist"

@allure.step("Check if file does not exist")
@retry(trys=3, retry_timeout=5, exception_types=[ResponseErrorCode])
def step_check_file_not_exists(client: Client, path: str):
    assert not client.check(path), f"File {path} exists"

@allure.step("Create directory")
@retry(trys=3, retry_timeout=5, exception_types=[ResponseErrorCode])
def step_create_directory(client: Client, path: str):
    return client.mkdir(path)

@allure.step("Upload file")
@retry(trys=3, retry_timeout=5, exception_types=[ResponseErrorCode])
def step_upload_file(client: Client, path: str, input_file: str):
    client.upload_file(remote_path=path, local_path=input_file)

@allure.step("Download file")
@retry(trys=3, retry_timeout=5, exception_types=[ResponseErrorCode])
def step_download_file(client: Client, path: str, output_file: str):
    client.download_file(remote_path=path, local_path=output_file)

@allure.step("Delete file")
@retry(trys=3, retry_timeout=5, exception_types=[ResponseErrorCode])
@retry(trys=3, retry_timeout=5)
def step_delete_file(client: Client, path: str):
    client.clean(remote_path=path)

@allure.step("Move file")
@retry(trys=3, retry_timeout=5, exception_types=[ResponseErrorCode])
def step_move_file(client: Client, from_path: str, to_path: str):
    client.move(remote_path_from=from_path, remote_path_to=to_path)

@allure.step("Rename file")
@retry(trys=3, retry_timeout=5, exception_types=[ResponseErrorCode])
def step_rename_file(client: Client, old_name: str, new_name: str):
    client.move(remote_path_from=old_name, remote_path_to=new_name)

@allure.step("Copy file")
@retry(trys=3, retry_timeout=5, exception_types=[ResponseErrorCode])
def step_copy_file(client: Client, from_path: str, to_path: str):
    client.copy(remote_path_from=from_path, remote_path_to=to_path)

@allure.step("List files")
@retry(trys=3, retry_timeout=5, exception_types=[ResponseErrorCode])
def step_list_files(client: Client, path: str):
    return client.list(remote_path=path)