# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page, Locator
from pages.base.base import expect
from pages.ox.page import *
from tests.utils import screenshot_step, retry
import allure

def step_wait(page: Page, timeout: float):
    with allure.step(f"Waiting {timeout/1000:.2f} seconds..."):
        page.wait_for_timeout(timeout=timeout)

@allure.step("Open OX page")
@screenshot_step
def step_open_page(page: Page, url_portal: str):
    OXPage(page).open_page(url_portal)
    OXPage(page).validate()

@allure.step("Checking OX top-left bar")
@screenshot_step
def step_check_ox_top_left_bar(page: Page):
    ox_page: OXPage = OXPage(page)
    
    top_left_bar: TopLeftBar = ox_page.top_left_bar
    
    expect(top_left_bar.logo).to_be_visible()
    expect(top_left_bar.navigation_button).to_be_visible()
    expect(top_left_bar.navigation_dropdown).not_to_be_visible()
    
    top_left_bar.navigation_button.click()
    expect(top_left_bar.navigation_dropdown).to_be_visible()
    
    top_left_bar.navigation_button.click(force=True)
    expect(top_left_bar.navigation_dropdown).not_to_be_visible()
    
@allure.step("Checking OX search bar")
@screenshot_step
def step_check_ox_search_bar(page: Page):
    ox_page: OXPage = OXPage(page)
    
    search_bar: SearchBar = ox_page.search_bar
    
    expect(search_bar.container).to_be_visible()
    expect(search_bar.search_button).to_be_visible()
    expect(search_bar.search_input).to_be_visible()
    expect(search_bar.cancel_search_button).not_to_be_visible()
    expect(search_bar.search_drop_down_button).to_be_visible()
    expect(search_bar.search_drop_down_form).not_to_be_visible()
    
    search_bar.search_input.click()
    expect(search_bar.cancel_search_button).to_be_visible()
    
    search_bar.search_drop_down_button.click()
    expect(search_bar.search_drop_down_form).to_be_visible()
    
    search_bar.search_drop_down_button.click()
    expect(search_bar.search_drop_down_form).not_to_be_visible()
    
@allure.step("Checking OX top-right bar")
@screenshot_step
def step_check_ox_top_right_bar(page: Page):
    ox_page: OXPage = OXPage(page)
    
    top_right_bar: TopRightBar = ox_page.top_right_bar
    
    expect(top_right_bar.address_list_button).to_be_visible()
    expect(top_right_bar.account_button).to_be_visible()
    expect(top_right_bar.help_button).to_be_visible()
    expect(top_right_bar.refresh_button).to_be_visible()
    expect(top_right_bar.settings_button).to_be_visible()
    expect(top_right_bar.toggle_notifications_button).to_be_visible()

@allure.step("Open navigation")
@screenshot_step
def step_open_navigation(page: Page):
    ox_page: OXPage = OXPage(page)
    
    top_left_bar: TopLeftBar = ox_page.top_left_bar
    
    expect(top_left_bar.navigation_button).to_be_visible()
    top_left_bar.navigation_button.click()
    
    expect(top_left_bar.navigation_dropdown).to_be_visible()
    
@allure.step("Click calendar tile on navigation")
@screenshot_step
def step_navigation_calendar_tile_redirect(page: Page):
    ox_page: OXPage = OXPage(page)
    
    expect(ox_page.top_left_bar.calendar_navigation_tile).to_be_visible()
    ox_page.top_left_bar.calendar_navigation_tile.click()
    
    CalendarPage(page).validate()

@allure.step("Click mail tile on navigation")
@screenshot_step
def step_navigation_mail_tile_redirect(page: Page):
    ox_page: OXPage = OXPage(page)
    
    expect(ox_page.top_left_bar.mail_navigation_tile).to_be_visible()
    ox_page.top_left_bar.mail_navigation_tile.click()
    
    MailPage(page).validate()
    
@allure.step("Select contact")
@screenshot_step
def step_select_contact(page: Page, entry: AddressBookEntry):
    expect(entry.checkmark).to_be_visible()
    if entry.is_selected():
        return
    entry.checkmark.click()
    assert entry.is_selected(), "Entry should be selected but is not."

@allure.step("Click select contacts button")
@screenshot_step
def step_click_select_contacts_button(page: Page):
    ox_page: OXPage = OXPage(page)
    expect(ox_page.address_book_modal.select_button).to_be_visible()
    ox_page.address_book_modal.select_button.click()

@allure.step("Check if selected contacts appear in recipient list")
@screenshot_step
def step_check_if_contact_is_in_recipient_list(page: Page, firstname: str, lastname: str):
    ox_page: OXPage = OXPage(page)

    expect(ox_page.write_mail_floating_window.get_recipient_by_name(firstname, lastname)).to_be_visible()

@allure.step("Select functional mailboxes")
@screenshot_step
def step_select_functional_mailboxes(page: Page):
    ox_page: OXPage = OXPage(page)

    expect(ox_page.address_book_modal.address_list_select).to_be_visible()
    functional_mailbox_name = ox_page.address_book_modal.get_functional_mailbox_name()
    ox_page.address_book_modal.address_list_select.select_option(label=functional_mailbox_name)
    
@allure.step("Select file")
@screenshot_step
def step_select_file(page: Page, filename: str):
    ox_page: OXPage = OXPage(page)
    
    file_entry: Locator = ox_page.nextcloud_file_picker_modal.get_file(filename=filename)
    expect(file_entry).to_be_visible()
    file_entry.click()
    expect(file_entry).to_have_class(re.compile("selected"))

@allure.step("Open link settings")
@screenshot_step
def step_open_link_settings(page: Page):
    ox_page: OXPage = OXPage(page)
    
    expect(ox_page.nextcloud_file_picker_modal.link_settings_checkbox).to_be_visible()
    expect(ox_page.nextcloud_file_picker_modal.link_settings_checkbox).not_to_be_checked()
    ox_page.nextcloud_file_picker_modal.link_settings_checkbox.check()
    expect(ox_page.nextcloud_file_picker_modal.link_settings_checkbox).to_be_checked()

@allure.step("Check allow edit checkbox")
@screenshot_step
def step_link_allow_edit(page: Page):
    ox_page: OXPage = OXPage(page)
    
    expect(ox_page.nextcloud_file_picker_modal.allow_edit_input).to_be_visible()
    expect(ox_page.nextcloud_file_picker_modal.allow_edit_input).not_to_be_checked()
    ox_page.nextcloud_file_picker_modal.allow_edit_input.check()
    expect(ox_page.nextcloud_file_picker_modal.allow_edit_input).to_be_checked()

@allure.step("Type link password")
@screenshot_step
def step_link_type_password(page: Page, password: str):
    ox_page: OXPage = OXPage(page)
    
    expect(ox_page.nextcloud_file_picker_modal.password_protect_input).to_be_visible()
    ox_page.nextcloud_file_picker_modal.password_protect_input.type(password)
    expect(ox_page.nextcloud_file_picker_modal.password_protect_input).to_have_value(password)

@allure.step("Set expiration date to first valid day")
@screenshot_step
def step_link_set_expiration(page: Page):
    ox_page: OXPage = OXPage(page)
    
    expect(ox_page.nextcloud_file_picker_modal.expiration_datepicker_input).to_be_visible()
    ox_page.nextcloud_file_picker_modal.expiration_datepicker_input.click()
    expect(ox_page.nextcloud_file_picker_modal.expiration_datepicker_list).to_be_visible()
    expect(ox_page.nextcloud_file_picker_modal.expiration_datepicker_first_valid_date).to_be_visible()
    ox_page.nextcloud_file_picker_modal.expiration_datepicker_first_valid_date.click()

@allure.step("Save file links")
@screenshot_step
def step_save_file_links(page: Page):
    ox_page: OXPage = OXPage(page)
    
    expect(ox_page.nextcloud_file_picker_modal.save_button).to_be_visible()
    ox_page.nextcloud_file_picker_modal.save_button.click()

@allure.step("Check save file links success")
@screenshot_step
def step_check_save_success(page: Page):
    ox_page: OXPage = OXPage(page)
    
    expect(ox_page.nextcloud_file_picker_modal).not_to_be_visible()
    
@allure.step("Check save file links failed")
@screenshot_step
def step_check_save_failed(page: Page):
    ox_page: OXPage = OXPage(page)
    
    expect(ox_page.error_alert).to_be_visible()
    
@allure.step("Check if address book is visible")
@screenshot_step
def step_check_if_address_book_visible(page: Page):
    ox_page: OXPage = OXPage(page)

    expect(ox_page.address_book_modal).to_be_visible(timeout=10000)
    expect(ox_page.address_book_modal.search_query_input).to_be_visible(timeout=20000)

@allure.step("Input user in global address list window")
@screenshot_step
def step_input_user_in_global_address_list(page: Page, username: str):
    ox_page: OXPage = OXPage(page)

    expect(ox_page.address_book_modal.search_query_input).to_be_visible()
    ox_page.address_book_modal.search_query_input.clear()
    ox_page.address_book_modal.search_query_input.type(username)

@allure.step("Check if searched user appears in contact list")
@screenshot_step
def step_check_contact_appears_in_contact_list(page: Page, email: str) -> AddressBookEntry:
    ox_page: OXPage = OXPage(page)

    expect(ox_page.address_book_modal.mailbox_list).to_be_visible(timeout=30000)
    entry: AddressBookEntry = ox_page.address_book_modal.get_entry_by_email(email)
    expect(entry).to_be_visible()
    return entry

@allure.step("Select address book select option")
@screenshot_step
def step_select_address_book_select_option(page: Page, value: str):
    ox_page: OXPage = OXPage(page)
    address_book_modal = ox_page.address_book_modal
    expect(address_book_modal.address_list_select).to_be_visible()
    address_book_modal.address_list_select.select_option(value)