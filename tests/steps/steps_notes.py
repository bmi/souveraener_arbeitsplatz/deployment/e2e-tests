# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page, Locator
from pages.base.base import *
from pages.login.page import LoginPage
from pages.notes.page import *
from tests.utils import retry, screenshot_step
import allure

@allure.step("Open Notes page")
@screenshot_step
def step_open_page(page: Page, url_portal: str):
    notes_page: NotesPage = NotesPage(page)
    notes_page.open_page(url_portal=url_portal)
    notes_page.validate()

@allure.step("Check general availablity")
@screenshot_step
def step_general_availability(page: Page):
    notes_page: NotesListPage = NotesListPage(page)
    expect(notes_page.header).to_be_visible()
    expect(notes_page.header.logo).to_be_visible()
    expect(notes_page.header.navigation_button).to_be_visible()
    expect(notes_page.new_note_button).to_be_visible()
    expect(notes_page.note_list).to_be_visible()
    notes_page.header.navigation_button.click()
    expect(notes_page.header.navigation).to_be_visible()

@allure.step("Click new note button")
@screenshot_step
def step_click_new_note_button(page: Page):
    notes_page: NotesListPage = NotesListPage(page)
    notes_page.new_note_button.click()

@allure.step("Fill note name")
@screenshot_step
def step_fill_note_name(page: Page, name: str):
    edit_note_page: EditNotePage = EditNotePage(page)
    expect(edit_note_page.note_name).to_be_visible()
    edit_note_page.note_name.clear()
    edit_note_page.note_name.type(name)
    expect(edit_note_page.note_name).to_have_text(name)

@allure.step("Fill note content")
@screenshot_step
def step_fill_note_content(page: Page, content: str):
    edit_note_page: EditNotePage = EditNotePage(page)
    expect(edit_note_page.content_editor).to_be_visible()
    edit_note_page.content_editor.clear()
    edit_note_page.content_editor.type(content)
    expect(edit_note_page.content_editor).to_have_text(content)

@allure.step("Return to notes list")
@screenshot_step
def step_return_to_notes_list(page: Page):
    edit_note_page: EditNotePage = EditNotePage(page)
    
    expect(edit_note_page.home_button).to_be_visible()
    edit_note_page.home_button.click()

@allure.step("Check note exists")
@screenshot_step
def step_note_exists(page: Page, note_name: str):
    notes_page: NotesListPage = NotesListPage(page)
    
    expect(notes_page.note_list.get_note_by_name(note_name)).to_be_visible()

@allure.step("Check note not exists")
@screenshot_step
def step_note_not_exists(page: Page, note_name: str):
    notes_page: NotesListPage = NotesListPage(page)
    
    expect(notes_page.note_list.get_note_by_name(note_name)).not_to_be_visible()

@allure.step("Open note")
@screenshot_step
def step_open_note(page: Page, note_name: str):
    notes_page: NotesListPage = NotesListPage(page)
    
    expect(notes_page.note_list.get_note_by_name(note_name)).to_be_visible()
    notes_page.note_list.get_note_by_name(note_name).page_part_locator.click()
    expect(EditNotePage(page).note_name).to_have_text(note_name, timeout=15000)

@allure.step("Get note url")
@screenshot_step
def step_get_note_link(page: Page):
    edit_note_page: EditNotePage = EditNotePage(page)
    
    expect(edit_note_page.note_content).to_be_visible()
    
    return page.url
    
@allure.step("Click note share button")
@screenshot_step
def step_click_note_share_button(page: Page):
    edit_note_page: EditNotePage = EditNotePage(page)
    
    expect(edit_note_page.share_button).to_be_visible()
    edit_note_page.share_button.click()
    
    expect(edit_note_page.share_modal).to_be_visible()

@allure.step("Check share modal opened")
@screenshot_step
def step_check_share_modal_opened(page: Page):
    edit_note_page: EditNotePage = EditNotePage(page)
    
    expect(edit_note_page.share_modal).to_be_visible()
    expect(edit_note_page.share_modal.share_link_section).to_be_visible()
    expect(edit_note_page.share_modal.share_visibility_section).to_be_visible()
    expect(edit_note_page.share_modal.share_mail_section).to_be_visible()
    expect(edit_note_page.share_modal.share_groups_section).to_be_visible()

@allure.step("Open visibility select")
@screenshot_step
def step_open_visibility_select(page: Page):
    edit_note_page: EditNotePage = EditNotePage(page)
    
    expect(edit_note_page.share_modal.visibility_select).to_be_visible()
    edit_note_page.share_modal.visibility_select.click()
    expect(edit_note_page.share_modal.visibility_select_menu).to_be_visible()

@allure.step("Select visibility private")
@screenshot_step
def step_select_visibility_private(page: Page):
    edit_note_page: EditNotePage = EditNotePage(page)
    
    expect(edit_note_page.share_modal.visibility_select_item_private).to_be_visible()
    edit_note_page.share_modal.visibility_select_item_private.click()

@allure.step("Select visibility public")
@screenshot_step
def step_select_visibility_public(page: Page):
    edit_note_page: EditNotePage = EditNotePage(page)
    
    expect(edit_note_page.share_modal.visibility_select_item_public).to_be_visible()
    edit_note_page.share_modal.visibility_select_item_public.click()
    
@allure.step("Select visibility intern")
@screenshot_step
def step_select_visibility_intern(page: Page):
    edit_note_page: EditNotePage = EditNotePage(page)
    
    expect(edit_note_page.share_modal.visibility_select_item_intern).to_be_visible()
    edit_note_page.share_modal.visibility_select_item_intern.click()

@allure.step("Set visibility type read only")
@screenshot_step
def step_set_visibility_type_read_only(page: Page):
    edit_note_page: EditNotePage = EditNotePage(page)
    
    expect(edit_note_page.share_modal.visibility_type_read_only).to_be_visible()
    edit_note_page.share_modal.visibility_type_read_only.click()

@allure.step("Set visibility type read write")
@screenshot_step
def step_set_visibility_type_read_write(page: Page):
    edit_note_page: EditNotePage = EditNotePage(page)
    
    expect(edit_note_page.share_modal.visibility_type_read_write).to_be_visible()
    edit_note_page.share_modal.visibility_type_read_write.click()

@allure.step("Open note with link")
@screenshot_step
def step_open_note_with_link(page: Page, note_link: str):
    page.goto(note_link)

@allure.step("Check note no permission (Logged in)")
@screenshot_step
def step_check_note_no_permission_logged_in(page: Page):
    edit_note_page: EditNotePage = EditNotePage(page)
    
    expect(edit_note_page.error_alert).to_be_visible(timeout=10000)
    expect(edit_note_page.note_content).not_to_be_visible()

@allure.step("Check note no permission (Not logged in)")
@screenshot_step
def step_check_note_no_permission_not_logged_in(page: Page):
    LoginPage(page).validate()

@allure.step("Check note read only")
@screenshot_step
def step_check_note_read_only(page: Page):
    edit_note_page: EditNotePage = EditNotePage(page)
    
    expect(edit_note_page.warning_alert).to_be_visible(timeout=10000)
    expect(edit_note_page.note_content).to_be_visible()
    expect(edit_note_page.content_editor).not_to_be_visible()

@allure.step("Check note read write")
@screenshot_step
def step_check_note_read_write(page: Page):
    edit_note_page: EditNotePage = EditNotePage(page)
    
    expect(edit_note_page.warning_alert).not_to_be_visible(timeout=10000)
    expect(edit_note_page.note_content).to_be_visible()
    expect(edit_note_page.content_editor).to_be_visible()

@allure.step("Click delete note button")
@screenshot_step
def step_click_delete_note_button(page: Page, note_name: str):
    note_list_page: NotesListPage = NotesListPage(page)
    
    note = note_list_page.note_list.get_note_by_name(note_name)
    expect(note).to_be_visible()
    note.delete_button.click()
    
    expect(note_list_page.delete_note_modal).to_be_visible()

@allure.step("Confirm delete note")
@screenshot_step
def step_click_confirm_delete_button(page: Page):
    note_list_page: NotesListPage = NotesListPage(page)
    
    expect(note_list_page.delete_note_modal.delete_button).to_be_visible()
    note_list_page.delete_note_modal.delete_button.click()
    expect(note_list_page.delete_note_modal).not_to_be_visible()