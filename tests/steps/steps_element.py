# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

import json
import re
import uuid
from playwright.sync_api import Page, Locator
import requests
from pages.base.base import expect

import pytest
from pages.element.page import *
from pages.jitsi.page import JitsiJoinPage, JitsiRoomPage
from tests.utils import screenshot_step
import allure

@allure.step("Open Element page")
@screenshot_step
def step_open_page(page: Page, url_portal: str):
    ElementPage(page).open_page(url_portal)
    ElementPage(page).validate()

@allure.step("Open Element home page")
@screenshot_step
def step_open_home_page(page: Page, url_portal: str):
    ElementPage(page).open_home_page(url_portal)

@allure.step("Join room via url")
@screenshot_step
def step_open_room_url(page: Page, url_portal: str | None = None, room_url: str | None = None, room_address: str | None = None):
    if room_address:
        element_url: str = url_portal.replace("portal.", "chat.")
        if not element_url.endswith("/"): element_url += "/"
        url: str = f"{element_url}#/room/{room_address}"
        page.goto(url=url)
    else:
        page.goto(url=room_url)
    ElementPage(page).validate()

@allure.step("Check element site render")
@screenshot_step
def step_check_site_render(page: Page):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.navbar).to_be_visible()
    expect(element_page.navbar.navbar_logo).to_be_visible()
    expect(element_page.navbar.menu_button).to_be_visible()
    
    expect(element_page.left_panel).to_be_visible()
    expect(element_page.space_panel).to_be_visible()
    expect(element_page.room_list).to_be_visible()
    
@allure.step("Check element menu functionality")
@screenshot_step
def step_check_menu_functionality(page: Page):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.navbar.menu_list).not_to_be_visible()
    expect(element_page.navbar.menu_button).to_be_visible()
    element_page.navbar.menu_button.click()
    expect(element_page.navbar.menu_list).to_be_visible()
    expect(element_page.navbar.menu_list_items_header).to_be_visible()
    page.wait_for_timeout(500)
    element_page.navbar.menu_button.click(force=True)
    expect(element_page.navbar.menu_list).not_to_be_visible()

@allure.step("Click user menu")
@screenshot_step
def step_click_user_menu(page: Page):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.space_panel.user_menu_button).to_be_visible(timeout=60000)
    element_page.space_panel.user_menu_button.click()

@allure.step("Click user menu security button")
@screenshot_step
def step_click_user_menu_security_button(page: Page):
    element_page: ElementPage = ElementPage(page)
    element_page.user_menu.security_button.click()
    expect(element_page.user_settings_dialog).to_be_visible()

@allure.step("Click setup button")
@screenshot_step
def step_click_setup_security_key_button(page: Page):
    element_page: ElementPage = ElementPage(page)
    element_page.user_settings_dialog.secure_backup_setup_button.click()

@allure.step("Click delete backup button")
@screenshot_step
def step_click_delete_backup_button(page: Page):
    element_page: ElementPage = ElementPage(page)

    expect(element_page.user_settings_dialog.secure_backup_delete_button).to_be_visible()
    element_page.user_settings_dialog.secure_backup_delete_button.click()

@allure.step("Confirm backup deletion")
@screenshot_step
def step_click_confirm_delete_backup(page: Page):
    element_page: ElementPage = ElementPage(page)

    expect(element_page.confirm_backup_deletion_dialog.confirm_deletion_button).to_be_visible()
    element_page.confirm_backup_deletion_dialog.confirm_deletion_button.click()

@allure.step("Check generate key option selected")
@screenshot_step
def step_check_security_key_selected(page: Page):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.create_secret_dialog.option_key).to_have_class(re.compile("mx_StyledRadioButton_checked"))

@allure.step("Click all settings button")
@screenshot_step
def step_click_all_settings_button(page: Page):
    element_page: ElementPage = ElementPage(page)

    expect(element_page.user_menu.all_settings_button).to_be_visible()
    element_page.user_menu.all_settings_button.click()

@allure.step("Click close dialog button")
@screenshot_step
def step_click_close_dialog_button(page: Page):
    element_page: ElementPage = ElementPage(page)

    expect(element_page.user_settings_dialog.close_button).to_be_visible()
    element_page.user_settings_dialog.close_button.click()

@allure.step("Open new room menu")
@screenshot_step
def step_open_new_room_menu(page: Page):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.room_list.new_room_button).to_be_visible()
    element_page.room_list.new_room_button.click()
    
@allure.step("Click new room button")
@screenshot_step
def step_click_new_room(page: Page):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.room_list.new_room_menu.new_room_button).to_be_visible()
    element_page.room_list.new_room_menu.new_room_button.click()
    
@allure.step("Click explore public rooms button")
@screenshot_step
def step_click_explore_public_rooms(page: Page):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.room_list.new_room_menu.explore_public_rooms_button).to_be_visible()
    element_page.room_list.new_room_menu.explore_public_rooms_button.click()

@allure.step("Check spotlight search dialog opened")
@screenshot_step
def step_check_spotlight_dialog_open(page: Page):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.spotlight_dialog).to_be_visible()

@allure.step("Fill search input")
@screenshot_step
def step_fill_spotlight_search(page: Page, search_query: str):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.spotlight_dialog.search_input).to_be_visible()
    element_page.spotlight_dialog.search_input.clear()
    element_page.spotlight_dialog.search_input.type(search_query)

@allure.step("Click join public room")
@screenshot_step
def step_click_join_public_room(page: Page, room_address: str):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.spotlight_dialog).to_be_visible()
    room = element_page.spotlight_dialog.get_public_room_result_by_id(room_id=room_address)
    expect(room).to_be_visible()
    expect(room.join_button).to_be_visible()
    room.join_button.click()
    
@allure.step("Fill room name")
@screenshot_step
def step_fill_room_name(page: Page, room_name: str):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.new_room_dialog.name_input).to_be_visible()
    element_page.new_room_dialog.name_input.type(room_name)
    
@allure.step("Select knock join rule")
@screenshot_step
def step_select_join_rule_knock(page: Page):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.new_room_dialog.join_rule_dropdown).to_be_visible()
    element_page.new_room_dialog.join_rule_dropdown.click()
    expect(element_page.new_room_dialog.join_rule_knock).to_be_visible()
    element_page.new_room_dialog.join_rule_knock.click()
    
@allure.step("Select invite join rule")
@screenshot_step
def step_select_join_rule_invite(page: Page):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.new_room_dialog.join_rule_dropdown).to_be_visible()
    element_page.new_room_dialog.join_rule_dropdown.click()
    expect(element_page.new_room_dialog.join_rule_invite).to_be_visible()
    element_page.new_room_dialog.join_rule_invite.click()
    
@allure.step("Select public join rule")
@screenshot_step
def step_select_join_rule_public(page: Page):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.new_room_dialog.join_rule_dropdown).to_be_visible()
    element_page.new_room_dialog.join_rule_dropdown.click()
    expect(element_page.new_room_dialog.join_rule_public).to_be_visible()
    element_page.new_room_dialog.join_rule_public.click()
    
@allure.step("Fill public room address")
@screenshot_step
def step_fill_public_room_address(page: Page, room_id: str):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.new_room_dialog.room_address_input).to_be_visible()
    element_page.new_room_dialog.room_address_input.type(room_id)
    
@allure.step("Click create room button")
@screenshot_step
def step_click_create_room(page: Page):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.new_room_dialog.create_room_button).to_be_visible()
    element_page.new_room_dialog.create_room_button.click()

@allure.step("Click Account button in settings dialog")
@screenshot_step
def step_click_account_button_in_settings_dialog(page: Page):
    element_page: ElementPage = ElementPage(page)

    expect(element_page.user_settings_dialog.account_settings_button).to_be_visible()
    element_page.user_settings_dialog.account_settings_button.click()

@allure.step("Enter new displayname")
@screenshot_step
def step_enter_new_displayname(page: Page, name: str):
    element_page: ElementPage = ElementPage(page)

    expect(element_page.user_settings_dialog.account_settings_page.diplayname_settings.displayname_input).to_be_visible()
    old_name: str = element_page.user_settings_dialog.account_settings_page.diplayname_settings.displayname_input.input_value()
    element_page.user_settings_dialog.account_settings_page.diplayname_settings.displayname_input.clear()
    element_page.user_settings_dialog.account_settings_page.diplayname_settings.displayname_input.type(name)

    expect(element_page.user_settings_dialog.account_settings_page.diplayname_settings.accept_button).to_be_visible()
    element_page.user_settings_dialog.account_settings_page.diplayname_settings.accept_button.click()
    
    return old_name

@allure.step("Check displayname")
@screenshot_step
def step_check_displayname(page: Page, name: str):
    element_page: ElementPage = ElementPage(page)

    expect(element_page.user_menu.page_part_locator.get_by_text(name)).to_be_visible()

@allure.step("Check that displayname is immutable")
@screenshot_step
def step_check_displayname_is_immutable(page: Page):
    element_page: ElementPage = ElementPage(page)

    expect(element_page.user_settings_dialog.account_settings_page.diplayname_settings.displayname_input).to_be_disabled()


@allure.step("Click next")
@screenshot_step
def step_click_next(page: Page):
    element_page: ElementPage = ElementPage(page)
    element_page.create_secret_dialog.primary_button.click()

@allure.step("Check if localpart of matrixID is equal to username")
@screenshot_step
def step_check_localpart_is_username(page: Page, username: str):
    element_page: ElementPage = ElementPage(page)

    matrixid_label: Locator = element_page.user_settings_dialog.account_settings_page.matrix_id_label
    expect(matrixid_label.get_by_text(username, exact=True)).to_be_visible()

@allure.step("Check if localpart of username is a UUID")
@screenshot_step
def step_check_localpart_is_uuid(page: Page):
    element_page: ElementPage = ElementPage(page)

    matrixid_label: Locator = element_page.user_settings_dialog.account_settings_page.matrix_id_label
    localpart = matrixid_label.text_content().split(':')[0][1:]

    try:
        uuid.UUID(localpart)
    except:
        pytest.fail("Username is not a UUID")

@allure.step("Download security key")
@screenshot_step
def step_click_download_key(page: Page):
    element_page: ElementPage = ElementPage(page)
    with page.expect_download() as download_info:
        element_page.create_secret_dialog.recovery_key_dialog.download_key_button.click()
    
    download = download_info.value
    key = None
    with open(download.path()) as file:
        key = file.read()
        allure.attach(body=key, name=download.suggested_filename)
        file.close()
    return key

@allure.step("Check security setup successful")
@screenshot_step
def step_check_setup_successful(page: Page):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.secret_created_success_dialog).to_be_visible(timeout=30000)
    
    element_page.secret_created_success_dialog.primary_button.click()

@allure.step("Click new conversation button")
@screenshot_step
def step_click_new_conversation_button(page: Page):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.room_list.new_conversation_button).to_be_visible()
    element_page.room_list.new_conversation_button.click()

@allure.step("Fill invite username")
@screenshot_step
def step_fill_invite_username(page: Page, username: str):
    element_page: ElementPage = ElementPage(page)
    element_page.invite_user_dialog.text_input.type(username)

@allure.step("Fill invite firstname and lastname")
@screenshot_step
def step_fill_invite_fullname(page: Page, firstname: str, lastname: str):
    element_page: ElementPage = ElementPage(page)
    element_page.invite_user_dialog.text_input.type(f"{firstname} {lastname}")

@allure.step("Select user")
@screenshot_step
def step_select_user_by_name(page: Page, firstname: str, lastname: str):
    element_page: ElementPage = ElementPage(page)
    found_user: Locator = element_page.invite_user_dialog.find_user_by_name(firstname=firstname, lastname=lastname)
    expect(found_user).to_be_visible(timeout=10000)
    found_user.click()

@allure.step("Select user by username")
@screenshot_step
def step_select_user_by_username(page: Page, username: str):
    element_page: ElementPage = ElementPage(page)
    found_user: Locator = element_page.invite_user_dialog.find_user_by_username(username=username)
    expect(found_user).to_be_visible(timeout=10000)
    found_user.click()

@allure.step("Select user by fullname")
@screenshot_step
def step_select_user_by_fullname(page: Page, firstname: str, lastname: str):
    element_page: ElementPage = ElementPage(page)
    found_user: Locator = element_page.invite_user_dialog.find_user_by_name(firstname=firstname, lastname=lastname)
    expect(found_user).to_be_visible(timeout=10000)
    found_user.click()

@allure.step("Click invite button")
@screenshot_step
def step_click_invite_user(page: Page):
    element_page: ElementPage = ElementPage(page)
    element_page.invite_user_dialog.invite_button.click()
    expect(element_page.invite_user_dialog).not_to_be_visible()

@allure.step("Check room open by username")
@screenshot_step
def step_check_room_header_username(page: Page, username: str):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.current_room.get_room_header_with_name(username)).to_be_visible(timeout=30*1000)

@allure.step("Check room open by firstname and lastname")
@screenshot_step
def step_check_room_header_name(page: Page, firstname: str, lastname: str):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.current_room.get_room_header_with_internal_name(firstname=firstname, lastname=lastname)).to_be_visible(timeout=30*1000)

@allure.step("Check room open")
@screenshot_step
def step_check_room_header(page: Page, room_name: str):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.current_room.get_room_header(room_name)).to_be_visible(timeout=30*1000)
    
@allure.step("Open chat room invitation")
@screenshot_step
def step_open_chat_invitation(page: Page, firstname: str, lastname: str):
    element_page: ElementPage = ElementPage(page)
    invitation: Locator = element_page.room_list.find_room_chat_invitation(firstname=firstname, lastname=lastname)
    expect(invitation).to_be_visible()
    invitation.click()
    
@allure.step("Open room invitation")
@screenshot_step
def step_open_room_invitation(page: Page, room_name: str):
    element_page: ElementPage = ElementPage(page)
    invitation: Locator = element_page.room_list.find_room_invitation(room_name=room_name)
    expect(invitation).to_be_visible()
    invitation.click()
    
@allure.step("Open chat room")
@screenshot_step
def step_open_chat_room(page: Page, room_name: str):
    element_page: ElementPage = ElementPage(page)
    chat_room: Locator = element_page.room_list.find_room(room_name=room_name)
    expect(chat_room).to_be_visible()
    chat_room.click()
    
@allure.step("Open person room")
@screenshot_step
def step_open_person_room(page: Page, person_name: str):
    element_page: ElementPage = ElementPage(page)
    chat_room: Locator = element_page.room_list.find_person(person_name=person_name)
    expect(chat_room).to_be_visible()
    chat_room.click()

@allure.step("Open appointment scheduler")
@screenshot_step
def step_open_appointment_scheduler(page: Page):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.current_room.home_page).to_be_visible()
    expect(element_page.current_room.home_page.open_meeting_scheduler_button).to_be_visible()    
    element_page.current_room.home_page.open_meeting_scheduler_button.click()

@allure.step("Click schedule new meeting")
@screenshot_step
def step_click_schedule_new_meeting(page: Page):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.meeting_scheduler).to_be_visible(timeout=15000)
    element_page.meeting_scheduler.schedule_button.click()
    
    expect(element_page.schedule_meeting_dialog).to_be_visible(timeout=15000)

@allure.step("Fill meeting title")
@screenshot_step
def step_fill_meeting_title(page: Page, title: str):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.schedule_meeting_dialog.title_input).to_be_visible()
    element_page.schedule_meeting_dialog.title_input.type(title)
    expect(element_page.schedule_meeting_dialog.title_input).to_have_value(title)

@allure.step("Fill meeting description")
@screenshot_step
def step_fill_meeting_description(page: Page, description: str):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.schedule_meeting_dialog.description_input).to_be_visible()
    element_page.schedule_meeting_dialog.description_input.type(description)
    expect(element_page.schedule_meeting_dialog.description_input).to_have_value(description)

@allure.step("Click schedule meeting")
@screenshot_step
def step_click_create_meeting(page: Page):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.schedule_meeting_dialog.create_meeting_button).to_be_visible()
    element_page.schedule_meeting_dialog.create_meeting_button.click()
    expect(element_page.schedule_meeting_dialog).not_to_be_visible()

@allure.step("Check meeting created")
@screenshot_step
def step_check_meeting_exists(page: Page, title: str):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.meeting_scheduler.get_meeting_entry(title)).to_be_visible()

@allure.step("Join meeting room")
@screenshot_step
def step_join_meeting(page: Page, title: str):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.meeting_scheduler.get_meeting_entry(title)).to_be_visible()
    element_page.meeting_scheduler.get_meeting_entry(title).open_meeting_room_button.click()

@allure.step("Check meeting join page opened")
@screenshot_step
def step_check_meeting_join_page_opened(page: Page):
    jitsi_page: JitsiJoinPage = JitsiJoinPage(ElementPage(page).conference_room.jitsi_frame)
    jitsi_page.validate()

@allure.step("Fill display name")
@screenshot_step
def step_fill_meeting_display_name(page: Page, name: str):
    jitsi_page: JitsiJoinPage = JitsiJoinPage(ElementPage(page).conference_room.jitsi_frame)
    expect(jitsi_page.display_name_field).to_be_visible()
    jitsi_page.display_name_field.type(name)
    expect(jitsi_page.display_name_field).to_have_value(name)

@allure.step("Click join meeting")
@screenshot_step
def step_click_join_meeting(page: Page):
    jitsi_page: JitsiJoinPage = JitsiJoinPage(ElementPage(page).conference_room.jitsi_frame)
    expect(jitsi_page.join_button).to_be_visible()
    jitsi_page.join_button.click()

@allure.step("Check meeting room opened")
@screenshot_step
def step_check_meeting_room_opened(page: Page):
    jitsi_page: JitsiRoomPage = JitsiRoomPage(ElementPage(page).conference_room.jitsi_frame)
    jitsi_page.validate()

def step_check_meeting_participants(page: Page, participants: list[str]):
    jitsi_room_page: JitsiRoomPage = JitsiRoomPage(ElementPage(page).conference_room.jitsi_frame)
    @allure.step("Open participants pane")
    @screenshot_step
    def open_participants_pane():
        jitsi_room_page.move_mouse()
        expect(jitsi_room_page.participants_button).to_be_visible()
        expect(jitsi_room_page.participants_pane).not_to_be_visible()
        jitsi_room_page.participants_button.click()
        expect(jitsi_room_page.participants_pane).to_be_visible()
    open_participants_pane()
    
    for participant in participants:
        @allure.step(f"Check '{participant}' is participating")
        @screenshot_step
        def check_participant():
            expect(jitsi_room_page.get_participant(name=participant)).to_be_visible()
        check_participant()

@allure.step("Get meeting room url")
@screenshot_step
def step_get_meeting_room_url(page: Page):
    return page.url

@allure.step("Join meeting with url")
@screenshot_step
def step_join_meeting_with_url(page: Page, url: str):
    page.goto(url)
    ElementPage(page).validate()

@allure.step("Click start voice call")
@screenshot_step
def step_click_start_voice_call(page: Page):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.current_room.voice_call_button).to_be_visible()
    element_page.current_room.voice_call_button.click()

@allure.step("Check voice call started")
@screenshot_step
def step_check_voice_call_started(page: Page):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.current_room.room_call_view).to_be_visible()
    expect(element_page.current_room.room_call_view.hangup_button).to_be_visible()

@allure.step("Move mouse to open call menu")
def step_move_mouse_to_open_call_menu(page: Page):
    element_page: ElementPage = ElementPage(page)
    element_page.current_room.room_call_view.move_mouse()
    expect(element_page.current_room.room_call_view.hangup_button).to_be_visible()

@allure.step("Click hangup call")
@screenshot_step
def step_click_hangup_call(page: Page):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.current_room.room_call_view.hangup_button).to_be_visible()
    element_page.current_room.room_call_view.hangup_button.click()

@allure.step("Check voice call ended")
@screenshot_step
def step_check_voice_call_ended(page: Page):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.current_room.room_call_view).not_to_be_visible()
    expect(element_page.current_room.get_latest_call_message().primary_button).not_to_be_visible()

@allure.step("Click join call")
@screenshot_step
def step_click_join_call(page: Page):
    element_page: ElementPage = ElementPage(page)
    
    room_call_message: RoomCallMessage = element_page.current_room.get_latest_call_message()
    expect(room_call_message).to_be_visible()
    expect(room_call_message.danger_button).to_be_visible()
    expect(room_call_message.primary_button).to_be_visible()
    room_call_message.primary_button.click()

@allure.step("Accept user knock")
@screenshot_step
def step_accept_knock(page: Page, username: str):
    element_page: ElementPage = ElementPage(page)
    user_knock: RoomKnock = element_page.current_room.get_knock(username=username)
    expect(user_knock).to_be_visible()
    expect(user_knock.accept_button).to_be_visible()
    user_knock.accept_button.click()

@allure.step("Accept user knock")
@screenshot_step
def step_accept_knock_fullname(page: Page, firstname: str, lastname: str):
    element_page: ElementPage = ElementPage(page)
    user_knock: RoomKnock = element_page.current_room.get_knock_fullname(firstname=firstname, lastname=lastname)
    expect(user_knock).to_be_visible()
    expect(user_knock.accept_button).to_be_visible()
    user_knock.accept_button.click()

@allure.step("Accept guest knock")
@screenshot_step
def step_accept_guest_knock(page: Page, guest_name: str):
    element_page: ElementPage = ElementPage(page)
    guest_knock: RoomKnock = element_page.current_room.get_knock_guest(guest_name=guest_name)
    expect(guest_knock).to_be_visible()
    expect(guest_knock.accept_button).to_be_visible()
    guest_knock.accept_button.click()

@allure.step("Click invite into room")
@screenshot_step
def step_click_invite(page: Page):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.current_room.invite_button).to_be_visible()
    element_page.current_room.invite_button.click()

@allure.step("Click join button")
@screenshot_step
def step_click_join_room(page: Page):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.current_room.room_preview.join_room_button).to_be_visible(timeout=20000)
    element_page.current_room.room_preview.join_room_button.click()

@allure.step("Fill guest join name")
@screenshot_step
def step_fill_guest_join_name(page: Page, name: str):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.request_room_access_dialog.name_input).to_be_visible()
    element_page.request_room_access_dialog.name_input.clear()
    element_page.request_room_access_dialog.name_input.type(name)

@allure.step("Click continue as guest")
@screenshot_step
def step_click_join_as_guest(page: Page):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.request_room_access_dialog.continue_as_guest_button).to_be_visible()
    element_page.request_room_access_dialog.continue_as_guest_button.click()
    expect(element_page.request_room_access_dialog).not_to_be_visible()
    ElementPage(page).validate()

@allure.step("Check join room failed")
@screenshot_step
def step_check_join_room_failed(page: Page):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.error_dialog).to_be_visible()

@allure.step("Send message in current room")
@screenshot_step
def step_send_message_in_room(page: Page, message: str):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.current_room.text_input).to_be_visible()
    element_page.current_room.text_input.type(message)
    expect(element_page.current_room.text_input).to_have_text(message)
    element_page.current_room.text_input.press("Enter")
    expect(element_page.current_room.text_input).to_have_text("", timeout=90000)

@allure.step("Check message is in current room")
@screenshot_step
def step_check_message_in_chat(page: Page, message: str):
    element_page: ElementPage = ElementPage(page)
    message_locator: Locator = element_page.current_room.get_message(message=message)
    expect(message_locator).to_be_visible()

@allure.step("Fill security key")
@screenshot_step
def step_fill_security_key(page: Page, security_key: str):
    element_page: ElementLoginPage = ElementLoginPage(page)
    element_page.security_key_input.clear()
    element_page.security_key_input.type(security_key)

@allure.step("Click continue to use security key")
@screenshot_step
def step_security_click_continue(page: Page):
    element_page: ElementLoginPage = ElementLoginPage(page)
    element_page.primary_button.click()
    
@allure.step("Check element page")
@screenshot_step
def step_check_element_page(page: Page):
    element_page: ElementPage = ElementPage(page)
    expect(element_page.wrapper).to_be_visible()

@allure.step("Click verify with security key")
@screenshot_step
def step_verify_with_security_key(page: Page):
    element_page: ElementLoginPage = ElementLoginPage(page)
    expect(element_page.verify_with_key_button).to_be_visible(timeout=60000)
    element_page.verify_with_key_button.click()

@allure.step("Click done")
@screenshot_step
def step_click_security_key_done(page: Page):
    element_page: ElementLoginPage = ElementLoginPage(page)
    expect(element_page.security_key_done_button).to_be_visible()
    element_page.security_key_done_button.click()

@allure.step("Check if a security key is expected")
@screenshot_step
def step_check_security_key(page: Page, security_key: str | None):
    try:
        expect(page).to_have_title("Element")
    except:
        pytest.skip("No request for security key.")
    
    step_verify_with_security_key(page=page)
    if not security_key:
        step_click_no_security_key(page=page)
        return
    
    step_use_security_key(page=page, security_key=security_key)

@allure.step("Click back to avoid using a security key")
@screenshot_step
def step_click_no_security_key(page: Page):
    element_page: ElementPage = ElementPage(page)
    element_page.dialog_warning_button.click()

@allure.step("Use security key")
@screenshot_step
def step_use_security_key(page: Page, security_key: str):
    step_fill_security_key(page=page, security_key=security_key)
    step_security_click_continue(page=page)
    step_click_security_key_done(page=page)
    step_check_element_page(page=page)

@allure.step("Get element server status")
def step_get_server_status(base_domain: str) -> dict:
    element_url: str = f"chat.{base_domain}"
    test_url: str = f"https://matrix.org/federationtester/api/report?server_name={element_url}"
    
    response: requests.Response = requests.get(test_url)
    assert response.ok
    json_content: dict = response.json()
    
    allure.attach(json.dumps(json_content, indent=3), name="Status", attachment_type=allure.attachment_type.JSON)
    
    return json_content

@allure.step("Check federation enabled")
def step_check_federation_enabled(response: dict):
    assert response["FederationOK"] == True

@allure.step("Check federation disabled")
def step_check_federation_disabled(response: dict):
    assert response["FederationOK"] == False
