# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page
from pages.base.base import expect
from pages.xwiki.page import *
from tests.utils import screenshot_step
import allure

@allure.step("Open XWiki page")
@screenshot_step
def step_open_page(page: Page, url_portal: str):
    XWikiPage(page).open_page(url_portal)
    XWikiPage(page).validate()

@allure.step("Open XWiki newsfeed page")
@screenshot_step
def step_open_newsfeed_page(page: Page, url_portal: str):
    XWikiNewsfeedPage(page).open_page(url_portal)
    XWikiNewsfeedPage(page).validate()

@allure.step("Check XWiki navbar company logo")
@screenshot_step
def step_check_navbar_logo(page: Page):
    xwiki_page: XWikiPage = XWikiPage(page=page)
    
    expect(xwiki_page.navbar.logo).to_be_visible()

@allure.step("Check XWiki navbar services navigation")
@screenshot_step
def step_check_navbar_services(page: Page):
    xwiki_page: XWikiPage = XWikiPage(page=page)
    expect(xwiki_page.navbar.services_button).to_be_visible()
    
    expect(xwiki_page.navbar.services_list).not_to_be_visible()
    xwiki_page.navbar.services_button.click()
    expect(xwiki_page.navbar.services_list).to_be_visible()
    xwiki_page.navbar.services_button.click()
    expect(xwiki_page.navbar.services_list).not_to_be_visible()

@allure.step("Check XWiki navbar search button")
@screenshot_step 
def step_check_navbar_search(page: Page):
    xwiki_page: XWikiPage = XWikiPage(page=page)
    expect(xwiki_page.navbar.search_button).to_be_visible()
    
    expect(xwiki_page.navbar.search_button_open).not_to_be_visible()
    xwiki_page.navbar.search_button.click()
    expect(xwiki_page.navbar.search_button_open).to_be_visible()
    xwiki_page.navbar.page_part_locator.click()
    expect(xwiki_page.navbar.search_button_open).not_to_be_visible()
    
@allure.step("Check XWiki navbar buttons right")
@screenshot_step
def step_check_navbar_right_buttons(page: Page):
    xwiki_page: XWikiPage = XWikiPage(page=page)
    expect(xwiki_page.navbar.notifications_button).to_be_visible()
    expect(xwiki_page.navbar.avatar_button).to_be_visible()

@allure.step("Check XWiki drawer")
@screenshot_step
def step_check_navbar_drawer(page: Page):
    xwiki_page: XWikiPage = XWikiPage(page=page)
    expect(xwiki_page.navbar.drawer_button).to_be_visible()
    
    expect(xwiki_page.navbar.drawer_nav).not_to_be_visible()
    xwiki_page.navbar.drawer_button.click()
    expect(xwiki_page.navbar.drawer_nav).to_be_visible()
    xwiki_page.navbar.drawer_nav_close_button.click()
    expect(xwiki_page.navbar.drawer_nav).not_to_be_visible()

@allure.step("Check XWiki left panels")
@screenshot_step
def step_check_left_panels(page: Page):
    xwiki_page: XWikiLeftPanelsPage = XWikiLeftPanelsPage(page=page)
    expect(xwiki_page.left_panels).to_be_visible()
    expect(xwiki_page.page_navigation).to_be_visible()
    
@allure.step("Check XWiki right panels")
@screenshot_step
def step_check_right_panels(page: Page):
    xwiki_page: XWikiPage = XWikiPage(page=page)
    expect(xwiki_page.right_panels).to_be_visible()    

@allure.step("Click create page button")
@screenshot_step
def step_click_create_button(page: Page):
    xwiki_page: XWikiViewPage = XWikiViewPage(page=page)
    expect(xwiki_page.create_page_button).to_be_visible()
    xwiki_page.create_page_button.click()
    
@allure.step("Fill new page title")
@screenshot_step
def step_new_page_fill_title(page: Page, title: str):
    xwiki_page: XWikiCreatePage = XWikiCreatePage(page=page)
    xwiki_page.title_field.type(title)
    expect(xwiki_page.title_field).to_have_value(title)

@allure.step("Create new page")
@screenshot_step
def step_new_page_click_create(page: Page):
    xwiki_page: XWikiCreatePage = XWikiCreatePage(page=page)
    expect(xwiki_page.create_button).to_be_visible()
    xwiki_page.create_button.click()

@allure.step("Fill page content")
@screenshot_step
def step_edit_page_fill_content(page: Page, content: str):
    xwiki_page: XWikiEditPage = XWikiEditPage(page=page)
    expect(xwiki_page.text_edit_container).to_be_visible()
    xwiki_page.text_edit_container.click()
    page.keyboard.type(content)

@allure.step("Click save and view button")
@screenshot_step
def step_edit_page_click_save_and_view(page: Page):
    xwiki_page: XWikiEditPage = XWikiEditPage(page=page)
    expect(xwiki_page.save_and_view_button).to_be_visible()
    xwiki_page.save_and_view_button.click()

@allure.step("Fill blog post content")
@screenshot_step
def step_edit_blog_fill_content(page: Page, content: str):
    xwiki_page: XWikiEditBlogPostPage = XWikiEditBlogPostPage(page=page)
    expect(xwiki_page.text_edit_container).to_be_visible()
    xwiki_page.text_edit_container.click()
    page.keyboard.type(content)

@allure.step("Click save and view blog post button")
@screenshot_step
def step_edit_blog_click_save_and_view(page: Page):
    xwiki_page: XWikiEditBlogPostPage = XWikiEditBlogPostPage(page=page)
    expect(xwiki_page.save_and_view_button).to_be_visible()
    xwiki_page.save_and_view_button.click()
    expect(xwiki_page.save_and_view_button).not_to_be_visible()

@allure.step("Check new page is in navigation and selected")
@screenshot_step
def step_check_new_page_in_navigation_and_selected(page: Page, title: str):
    xwiki_page: XWikiViewPage = XWikiViewPage(page=page)
    expect(xwiki_page.document_title).to_have_text(title, timeout=30000)
    expect(xwiki_page.page_navigation.get_page_in_navigation(title)).to_be_visible()
    expect(xwiki_page.page_navigation.current_page).to_have_text(title)

@allure.step("Fill new newsfeed blog post title")
@screenshot_step
def step_fill_newsfeed_blog_post_title(page: Page, title: str):
    xwiki_page: XWikiNewsfeedPage = XWikiNewsfeedPage(page=page)
    expect(xwiki_page.new_newsfeed_blog_input).to_be_visible()
    xwiki_page.new_newsfeed_blog_input.clear()
    xwiki_page.new_newsfeed_blog_input.type(title)
    expect(xwiki_page.new_newsfeed_blog_input).to_have_value(title)

@allure.step("Click create newsfeed blog post button")
@screenshot_step
def step_click_create_newsfeed_blog_post(page: Page):
    xwiki_page: XWikiNewsfeedPage = XWikiNewsfeedPage(page=page)
    expect(xwiki_page.new_newsfeed_blog_submit).to_be_visible()
    xwiki_page.new_newsfeed_blog_submit.click()