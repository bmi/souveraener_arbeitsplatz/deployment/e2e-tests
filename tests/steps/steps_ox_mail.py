# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

import re
from playwright.sync_api import Page, Locator
import pytest
from pages.base.base import expect
from pages.ox.page import *
from tests.utils import screenshot_step, retry
import allure
import os
import datetime

from .steps_ox import *

@allure.step("Open Mail page")
@screenshot_step
def step_open_mail_page(page: Page, url_portal: str):
    MailPage(page).open_page(url_portal)
    MailPage(page).validate()

@allure.step("Check new device notification was not received")
@screenshot_step
def step_check_new_device_notification_not_received(page: Page):
    mail_page: MailPage = MailPage(page)
    
    new_device_notification_mail: Locator = mail_page.mail_list.get_mail(subject="New device login on your openDesk account", status="unread")
    
    expect(new_device_notification_mail).not_to_be_visible()

@allure.step("Checking new device notification and opening notification mail")
@screenshot_step
def step_open_new_device_notification(page: Page):
    mail_page: MailPage = MailPage(page)
    
    new_device_notification_mail: Locator = mail_page.mail_list.get_mail(subject="New device login on your openDesk account", status="unread")
    
    expect(new_device_notification_mail).to_be_visible()
    new_device_notification_mail.click()

@allure.step("Checking new device notification timestamp after start")
@screenshot_step
def step_check_new_device_notification_timestamp(page: Page, test_start_timestamp: datetime.datetime):
    mail_page: MailPage = MailPage(page)
    
    expect(mail_page.mail_detail).to_be_visible()
    
    mail_content: Locator = mail_page.mail_detail.mail_content.body.content
    
    time_div = mail_content.locator("div", has_text=re.compile("Time: "))
    expect(time_div).to_be_visible()
    
    time_text = time_div.inner_text()
    time = time_text.split("Time: ")[1]
    
    time_parsed = datetime.datetime.strptime(time, "%Y-%m-%d %H:%M:%S.%f")
    
    time_parsed_utc = time_parsed.replace(tzinfo=datetime.timezone.utc)
    test_start_timestamp_utc = test_start_timestamp.astimezone(datetime.timezone.utc)
    
    assert time_parsed_utc > test_start_timestamp_utc

@allure.step("Click new mail")
@screenshot_step
def step_click_new_mail(page: Page):
    mail_page: MailPage = MailPage(page)
    
    new_button: Locator = mail_page.current_new_button()
    expect(new_button).to_be_visible()
    new_button.click()
    
    expect(mail_page.write_mail_floating_window).to_be_visible()

@allure.step("Select email sender")
@screenshot_step
def step_select_sender(page: Page, sender: str):
    mail_page: MailPage = MailPage(page)

    expect(mail_page.write_mail_floating_window.from_block.dropdown_link).to_be_visible()
    mail_page.write_mail_floating_window.from_block.dropdown_link.click()

    expect(mail_page.write_mail_floating_window.from_block.sender_entries.filter(has_text=sender)).to_be_visible()
    mail_page.write_mail_floating_window.from_block.sender_entries.filter(has_text=sender).click()

@allure.step("Fill internal recipient by username")
@screenshot_step
def step_fill_internal_recipient(page: Page, username: str):
    mail_page: MailPage = MailPage(page)
    
    mail_page.write_mail_floating_window.recepient_input.click()
    page.wait_for_timeout(1000)
    page.keyboard.type(username)

@allure.step("Click recipient")
@screenshot_step
def step_click_recipient(page: Page, username: str):
    mail_page: MailPage = MailPage(page)
    
    recipient = mail_page.write_mail_floating_window.get_recipient_recommend(username)
    expect(recipient).to_be_visible(timeout=30000)
    recipient.click()

@allure.step("Fill recipient")
@screenshot_step
def step_fill_recipient_email(page: Page, email: str):
    mail_page: MailPage = MailPage(page)
    
    mail_page.write_mail_floating_window.recepient_input.click()
    page.wait_for_timeout(1000)
    page.keyboard.type(email)
    page.wait_for_timeout(500)
    page.keyboard.press("Enter")

@allure.step("Fill subject")
@screenshot_step
def step_fill_subject(page: Page, subject: str):
    mail_page: MailPage = MailPage(page)
    
    expect(mail_page.write_mail_floating_window.subject_input).to_be_visible()
    mail_page.write_mail_floating_window.subject_input.clear()
    mail_page.write_mail_floating_window.subject_input.type(subject)
    expect(mail_page.write_mail_floating_window.subject_input).to_have_value(subject)
    
@allure.step("Upload attachments")
@screenshot_step
def step_mail_upload_attachments(page: Page, folder:str, files: list[str]):
    mail_page: MailPage = MailPage(page)

    expect(mail_page.write_mail_floating_window.attachment_input).to_be_attached()
    for file in files:
        step_mail_upload_attachment(page, folder, file)
    page.keyboard.press("Escape")

@allure.step("Upload attachment")
@screenshot_step
def step_mail_upload_attachment(page: Page, folder:str, file: str):
    mail_page: MailPage = MailPage(page)

    expect(mail_page.write_mail_floating_window.attachment_input).to_be_attached()
    mail_page.write_mail_floating_window.attachment_input.set_input_files(os.path.join(folder, file))

@allure.step("Click attachments button")
@screenshot_step
def step_click_attachments_button(page: Page):
    mail_page: MailPage = MailPage(page)
    
    expect(mail_page.write_mail_floating_window.attachment_button).to_be_visible()
    mail_page.write_mail_floating_window.attachment_button.click()

@allure.step("Close mail window")
@screenshot_step
def step_close_mail_window(page: Page):
    mail_page: MailPage = MailPage(page)
    
    expect(mail_page.write_mail_floating_window.close_button).to_be_visible()
    mail_page.write_mail_floating_window.close_button.click()

@allure.step("Click save draft button")
@screenshot_step
def step_click_save_draft_button(page: Page):
    mail_page: MailPage = MailPage(page)
    
    expect(mail_page.save_draft_modal).to_be_visible()
    mail_page.save_draft_modal.save_button.click()
    expect(mail_page.save_draft_modal).not_to_be_visible()

@allure.step("Open drafts folder")
@screenshot_step
def step_open_drafts_folder(page: Page):
    mail_page: MailPage = MailPage(page)
    
    mail_page.sidepanel.standard_folders.expand()
    drafts_folder: Folder = mail_page.sidepanel.standard_folders.drafts
    expect(drafts_folder).to_be_visible()
    drafts_folder.open()

@allure.step("Click contacts button")
@screenshot_step
def step_click_contacts_button(page: Page):
    mail_page: MailPage = MailPage(page)

    expect(mail_page.write_mail_floating_window.contacts_button).to_be_visible()
    mail_page.write_mail_floating_window.contacts_button.click()
    expect(mail_page.address_book_modal).to_be_visible()

@allure.step("Check if selected contacts appear in recipient list")
@screenshot_step
def step_check_if_contact_is_in_recipient_list(page: Page, firstname: str, lastname: str):
    mail_page: MailPage = MailPage(page)

    expect(mail_page.write_mail_floating_window.get_recipient_by_name(firstname, lastname)).to_be_visible()

@allure.step("Click add link to files button")
@screenshot_step
def step_click_link_from_files_button(page: Page):
    mail_page: MailPage = MailPage(page)
    
    expect(mail_page.write_mail_floating_window.link_to_files_button).to_be_visible()
    mail_page.write_mail_floating_window.link_to_files_button.click()

@allure.step("Click from files button")
@screenshot_step
def step_click_from_files_button(page: Page):
    mail_page: MailPage = MailPage(page)
    
    expect(mail_page.write_mail_floating_window.from_files_button).to_be_visible()
    mail_page.write_mail_floating_window.from_files_button.click()
    expect(mail_page.nextcloud_file_picker_modal).to_be_visible()

def step_check_attachment_preview(page: Page, filename: str):
    @allure.step(f"Check attachment preview '{filename}'")
    @screenshot_step
    def check_preview(page: Page, filename: str):
        mail_page: MailPage = MailPage(page)
        expect(mail_page.write_mail_floating_window.get_mail_attachment_preview(filename)).to_be_visible()
    
    check_preview(page=page, filename=filename)

@allure.step("Send mail")
@screenshot_step
def step_send_mail(page: Page):
    mail_page: MailPage = MailPage(page)
    mail_page.write_mail_floating_window.send_button.click()
    
    expect(mail_page.mail_progress_message).to_be_visible()
    expect(mail_page.mail_progress_message_state_send).to_be_visible(timeout=30000)

@allure.step("Get draft by subject")
@screenshot_step
def step_get_draft_by_subject(page: Page, subject: str) -> Locator:
    mail_page: MailPage = MailPage(page)
    
    draft: Locator = mail_page.mail_list.get_mail(subject=subject, status="all")
    
    expect(draft).to_be_visible(timeout=20000)
    
    return draft

@allure.step("Receive mail by subject")
@retry(trys=10, retry_timeout=0)
@screenshot_step
def step_receive_mail(page: Page, sender: str | None = None, subject: str | None = None, contains_subject: str | None = None, status: Literal["all", "read", "unread"] = "unread") -> Locator:
    mail_page: MailPage = MailPage(page)
    
    mail_by_subject: Locator = mail_page.mail_list.get_mail(sender=sender, subject=subject, contains_subject=contains_subject, status=status)
    
    expect(mail_by_subject).to_be_visible(timeout=20000)
    
    return mail_by_subject

@allure.step("Check mail by subject not found")
@screenshot_step
def step_mail_by_subject_not_found(page: Page, subject: str):
    mail_page: MailPage = MailPage(page)
    
    mail_by_subject: Locator = mail_page.mail_list.get_mail(subject=subject, status="unread")
    
    for _ in range(5):
        try:
            expect(mail_by_subject).to_be_visible(timeout=10000)
            break
        except:
            page.reload()
            mail_page.validate()
    else:
        return
    
    pytest.fail(f"Mail with subject '{subject}' should not be received but is in inbox.")

@allure.step("Click on mail to open")
@screenshot_step
def step_open_mail(page: Page, mail: Locator):
    mail_page: MailPage = MailPage(page)
    
    mail.click()
    expect(mail_page.mail_detail.mail_content).to_be_visible()

@allure.step("Check mail has attachment")
@screenshot_step
def step_check_mail_has_attachment(page: Page, filename: str):
    mail_page: MailPage = MailPage(page)
    
    expect(mail_page.mail_detail.mail_content.attachments.toggle_details_button).to_be_visible()
    mail_page.mail_detail.mail_content.attachments.expand_details()
    expect(mail_page.mail_detail.mail_content.attachments.get_attachment(filename)).to_be_visible()

@allure.step("Click attachment save to files")
@screenshot_step
def step_click_save_to_files(page: Page):
    mail_page: MailPage = MailPage(page)
    
    save_to_files_button = mail_page.mail_detail.mail_content.attachments.save_to_files_button
    expect(save_to_files_button).to_be_visible()
    save_to_files_button.click()
    
@allure.step("Save attachment to files")
@screenshot_step
def step_save_attachment_to_files(page: Page):
    mail_page: MailPage = MailPage(page)
    
    expect(mail_page.picker_modal).to_be_visible()
    expect(mail_page.picker_modal.save_button).to_be_visible()
    mail_page.picker_modal.save_button.click()
    
@allure.step("Click download attachments")
@screenshot_step
def step_download_attachments(page: Page):
    mail_page: MailPage = MailPage(page)
    
    download_button = mail_page.mail_detail.mail_content.attachments.download_button
    expect(download_button).to_be_visible()
    download_button.click()

@allure.step("Check malicious file modal shown")
@screenshot_step
def step_check_malicious_file_modal_shown(page: Page):
    mail_page: MailPage = MailPage(page)
    
    expect(mail_page.malicious_file_modal).to_be_visible()
    
@allure.step("Check functional account visible")
@screenshot_step
def step_check_functional_account_visible(page: Page, functional_account_name: str):
    mail_page: MailPage = MailPage(page)
    
    folder: MailFolder = mail_page.sidepanel.remote_folders.get_mail_subfolder(name=functional_account_name)
    expect(folder).to_be_visible(timeout=30000)

@allure.step("Open functional account inbox")
@screenshot_step
def step_open_functional_account_inbox(page: Page, functional_account_name: str):
    mail_page: MailPage = MailPage(page)
    
    folder: MailFolder = mail_page.sidepanel.remote_folders.get_mail_subfolder(name=functional_account_name)
    expect(folder).to_be_visible(timeout=30000)
    folder.expand()
    expect(folder.inbox).to_be_visible()
    folder.inbox.page_part_locator.click()

    expect(mail_page.mail_list).to_be_visible()


@allure.step("Expand functional account")
@screenshot_step
def step_expand_functional_account(page: Page, functional_account_name: str):
    mail_page: MailPage = MailPage(page)
    
    folder: MailFolder = mail_page.sidepanel.remote_folders.get_mail_subfolder(name=functional_account_name)
    expect(folder).to_be_visible()
    folder.expand()
    expect(folder.inbox).to_be_visible()
    
    
@allure.step("Click add folder")
@screenshot_step
def step_click_add_folder(page: Page):
    mail_page: MailPage = MailPage(page)
    
    expect(mail_page.sidepanel.add_folder_button).to_be_visible()
    mail_page.sidepanel.add_folder_button.click()
    
    expect(mail_page.sidepanel.add_folder_dropdown).to_be_visible()

@allure.step("Click add folder button")
@screenshot_step
def step_click_add_subfolder(page: Page):
    mail_page: MailPage = MailPage(page)
    
    expect(mail_page.sidepanel.add_folder_dropdown.add_subfolder_button).to_be_visible()
    mail_page.sidepanel.add_folder_dropdown.add_subfolder_button.click()

@allure.step("Fill folder name")
@screenshot_step
def step_fill_folder_name(page: Page, folder_name: str):
    mail_page: MailPage = MailPage(page)
    
    expect(mail_page.new_folder_modal).to_be_visible()
    mail_page.new_folder_modal.name_input.clear()
    mail_page.new_folder_modal.name_input.type(folder_name)
    expect(mail_page.new_folder_modal.name_input).to_have_value(folder_name)

@allure.step("Save folder")
@screenshot_step
def step_save_folder(page: Page):
    mail_page: MailPage = MailPage(page)
    
    expect(mail_page.new_folder_modal.add_button).to_be_visible()
    mail_page.new_folder_modal.add_button.click()
    expect(mail_page.new_folder_modal).not_to_be_visible()

@allure.step("Check folder exists")
@screenshot_step
def step_check_folder_exists(page: Page, folder_name: str):
    mail_page: MailPage = MailPage(page)
    
    mail_page.sidepanel.my_folders.expand()
    folder: Folder = mail_page.sidepanel.my_folders.get_subfolder(name=folder_name)
    expect(folder).to_be_visible()

@allure.step("Click move to folder")
@screenshot_step
def step_click_move_to_folder(page: Page):
    mail_page: MailPage = MailPage(page)
    
    expect(mail_page.mail_detail.toolbar.move_button).to_be_visible()
    mail_page.mail_detail.toolbar.move_button.click()
    
    expect(mail_page.mail_folder_select_modal).to_be_visible()

@allure.step("Expand my folders")
@screenshot_step
def step_move_mail_expand_my_folders(page: Page):
    mail_page: MailPage = MailPage(page)
    
    expect(mail_page.mail_folder_select_modal.my_folders).to_be_visible()
    mail_page.mail_folder_select_modal.my_folders.expand()

@allure.step("Select folder")
@screenshot_step
def step_move_mail_select_folder(page: Page, folder_name: str):
    mail_page: MailPage = MailPage(page)
    
    folder: Folder = mail_page.mail_folder_select_modal.my_folders.get_subfolder(name=folder_name)
    expect(folder).to_be_visible()
    folder.page_part_locator.click()

@allure.step("Move mail")
@screenshot_step
def step_move_mail_confirm(page: Page):
    mail_page: MailPage = MailPage(page)
    
    expect(mail_page.mail_folder_select_modal.confirm_button).to_be_visible()
    mail_page.mail_folder_select_modal.confirm_button.click()
    expect(mail_page.mail_folder_select_modal).not_to_be_visible()

@allure.step("Open folder")
@screenshot_step
def step_open_folder(page: Page, folder_name: str):
    mail_page: MailPage = MailPage(page)
    
    expect(mail_page.sidepanel.my_folders).to_be_visible()
    mail_page.sidepanel.my_folders.expand()
    folder: Folder = mail_page.sidepanel.my_folders.get_subfolder(name=folder_name)
    expect(folder).to_be_visible()
    folder.page_part_locator.click()
