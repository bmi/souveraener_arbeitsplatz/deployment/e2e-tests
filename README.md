<!---
SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->

# openDesk Tests

This is the system test suite for [openDesk](https://gitlab.souvap-univention.de/souvap/devops/sovereign-workplace).
The main purpose is to test if deployment and integration of all components was successful.
This README document contains information on how the tests are executed and how they are assembled.

## Test Reporting (Allure)

For test reporting we are using allure. Allure epics, features and more in: [Allure](documentation/allure.md)

How to use allure is described in [Allure Reporting](#allure-reporting) section.

## Playwright

The test suite runs on Python3 and
uses [playwright](https://playwright.dev/python/) which is well integrated
into [pytest](https://pytest.org).

## Execution

There are two ways how the tests can be executed local and in a triggered gitlab
pipeline.

### Local on Linux

The tests can be performed on a local computer if it runs a Linux (preferably
Debian) based operating system. For convenience three simple Bash-scripts are
available:

```
local-install
local-run
local-clean
```

#### Installation

The requirements are installed into a virtualenv with:

```sh
$ ./local-install.sh
```

#### Running the tests

Before the tests can get executed a configuration file needs to be created. For
more details on that see [Configuration](#configuration)
If the config is provided, the tests can get run with:

```sh
$ ./local-run.sh
```

Some examples

* In Firefox:
  ```sh
  $> ./local-run.sh --browser firefox
  ```

* Not headless and just some selected tests:
  ```sh
  $> ./local-run.sh --headed -m "login or portal or mav"
  ```

#### Cleanup

A simple cleanup that removes the virtualenv, log files and caches:

```sh
$ ./local-clean.sh
```

### Converting test-executions to k6-scripts
We can convert our test-execution via an intermediate layer into k6-scripts,
to support direct loadtests via k6.

To do so use this parameter:

```sh
$ ./local-run-load.sh
```

### Running OpenTalk tests
To run OpenTalk testcases we use a different config file. This config file must be based on config-opentalk-example.ini.
Tests will use config-opentalk.ini by default and only run tests within "tests/test_opentalk.py". 

To execute the OpenTalk testcases use:

```sh
$ ./local-run-opentalk.sh
```

### Gitlab triggered pipeline

Also a possibility to use this test suite is to trigger it by another pipeline
after that has deployed openDesk.

## Configuration

The test suite can be configured through command line parameters or via a
config.ini-file. The pipeline uses the command line arguments, the config-file
is for convenience on local runs.

### config.ini

An example [config-example.ini](config-example.ini) for ease of use is provided
and can be copied to `config.ini`. The `config.ini` is git-ignored and expected
by the `local-run` script.

```ini
[Admin]
username=default.admin
password=...

[Environment]
operator=zendis
namespace=develop
cluster=run
url=https://portal.<domain>
gitlab_token=<Gitlab API access token>
language=<de/en>
profile=<Default/Custom/Namespace>

[Autotest]
mail=<mail>@<domain>
server=<Mail Server>
imap_port=<IMAP Port: 993>
smtp_port=<SMTP Port: 465>
password=<Mail Password>

[Screenshot]
test=yes
before_step=yes
after_step=yes
redirect_step=yes

[Demo]
folder=demo_files
```

### Command line parameters

Most of the parameters that can be set in the `config.ini` can also be set via command line.

All options can be seen in the [Settings](tests/config/settings.py) file. A detailed description of how settings are created can be found in the [framework](documentation/framework/getting_started.md) documentation.

Additionally, there are some parameters that are just recognized by command
line:

* `-m "<mark1 or mark2 or mark3 ...>"`

  With _markers_ there exists a mechanism to run just a subset of tests. Every
  test has one or multiple marks that are defined by `@pytest.mark.markname`
  decorator in the test code. Depending on what tests ought to be run the
  regarding marks have to get chosen.

  To use multiple marks at one test-run they need to be separated by an `or` and
  get enclosed by quotes, like in this example:
  ```sh
  -m "mark1 or mark2 or mark3"
  ```

* `-k <test_identifier>` to execute a specific test only,
  example: `-k test_navigation_apps_mail`
* `--browser <browser>` the default webdriver used by playwright for the tests
  is Chromium. Valid drivers are `chromium`, or `firefox`.
* `--headed` usually the tests run headless without any windows popping up. But
  if it is desired to see what the tests are actually doing and how,
  use `--headed`.
* `--screenshot-before-step`, `--screenshot-after-step`, `--screenshot-redirect-step` change whether screenshots are taken before or after each step or when the step returns a page after being redirected ("yes" or "no").
* `--screenshot-test` change whether screenshots are taken for each test or just on fail/success ("yes", "no", "on_success", "on_fail").

## Developing tests

The test suite uses Python3 with [pytest](https://docs.pytest.org)
and [playwright](https://playwright.dev/).

### Pytest environment

This section explains briefly how to use pytest, it aims to provide basic
understanding for developers who never used pytest so far.

#### Fixtures

In pytest _fixtures_ are a mechanism to execute code that needs to be run before
or after tests or in custom situations. They evolved from the former setup- &
teardown functions and are way more versatile and powerful than these. In the
pytest documentation the part
explaining [how to use fixtures](https://docs.pytest.org/en/7.2.x/how-to/fixtures.html)
is the most important part of the pytest doc at all and may be a good start.

__TLDR__

To promote a function to a _fixture_, just this annotation is needed:

```python
@pytest.fixture
def my_simple_fixture():
    ...
```

_fixtures_ are executed implicitly if the code uses them as parameter, e.g.:

```python
# filename test_some.py
import pytest


@pytest.fixture
def my_simple_fixture():
    return "some text"


def test_some_text(my_simple_fixture):
    assert 'some text' == my_simple_fixture
```

This code snippet can be put in a file and get already run as a test:

```sh
$> pytest test_some.py
```

##### Scope, params, autouse

_Fixtures_ have some options that can be used to regulate by whom they may be
used, or how long they exist. Even parameters can be provided to them.
[Fixtures reference](https://docs.pytest.org/en/7.2.x/reference/fixtures.html#reference-fixtures).

#### conftest.py

Sharing fixtures across multiple test-files can be done with a file
named `conftest.py` that sits in the same directory as the test-files.

Due to more complex setups, the `conftest.py` does not contain any fixtures but imports them from other files located in the [fixtures](tests/fixtures/) folder.
The Config fixture is defined in [tests/fixtures/base.py](tests/fixtures/base.py).

```python
@pytest.fixture(scope="session")
def config(request) -> Generator[Config, None, None]:
    ...
```

A more detailed overview of all fixtures can be found in the [framework](documentation/framework/getting_started.md) documentation.

### Allure Reporting

Allure is a test-reporting tool that is used to visualize test results. You can define a tests epic, feature, and story to group tests and make them more readable. For more information on what epics, features and stories to use, see the [Allure](documentation/framework/allure.md) documentation.

Usage of the allure decorator:

```python
import allure

@allure.epic("<Epic>") # e.g. "Smoke" for smoke tests
@allure.feature("<Feature>") # e.g. "OX", "Calendar" for tests on the Open-Xchange Calendar app
@allure.story("<Story>") # e.g. "User" for tests with a normal user
@allure.title("<Feature>: <Short description>[(Additional information)]") # e.g. "OXcalendar: Create appointment (Add attachment)"
def test_create_appointment_with_attachment(...):
    ...
```

Tests ran locally will generate a `allure_result` folder in the root directory. This folder contains the test results in JSON format. To view the results, you can use the allure command line tool, which can be installed at [Allure Report Docs – Installation](https://allurereport.org/docs/install/).
To view the results, run the following command in the root directory:

```sh
allure serve allure_result
```

When running the tests in a pipeline the results will be uploaded to the [openDesk TestOps](https://testops.opendesk.run/project/1/) server.

## Pages

`src/pages/` contains the playwright representation of the Open Source apps that
are to be tested. Here is the app specific code that is used by the tests so
that they do not need to know much of the app specifics.
`src/pages/base/base.py` contains the two Parent classes `BasePage`
and `BasePagePart` that are widely used across all tests.

Code at: [src/pages/](src/pages/)

## Multi-language support

### Background
To enable testing in multiple languages - English and German so far - there is
the possibility to i18n the locator strings. In Python exists an i18n-toolset
named `gettext`. But since `gettext` was created primarily for web pages with
multiple languages and extensive textual content its process of providing and
integrating i18n-ized strings contains multiple tools and steps. Adding those to
a test suite would bloat the test process and dependencies unnecessarily.

Therefor a mechanism to keep the multi language-strings close to the code where
they are actually used, was established. So each Python module (file) has a
`.yml`-file with the same name beside it in the same folder. E.g.:

```bash
...
src/pages/nextcloud/home/common/base.py
src/pages/nextcloud/home/common/base.yml
...
```

These yaml files contain locator strings for both supported languages. In order
to use them, the class [Base()](src/pages/base/base.py) provides some methods
related to i18n. They are called in the `__init__(self)`-method of
`Base()` and read the yaml-files of all instances in the inheritance chain.
In order to trigger the reading of those files, each class that inherits
from `Base()` has to invoke its parents `__init__()`:
```python
  def __init__(self, page: Page):
      super().__init__(page)
```
Because `BasePage(Base)` and `BasePagePart(Base)` both inherit from `Base` this
has to be done in each class of the inheritance chain up to `Base()`. Given, of
course, i18n is to be used in the child class. If a component is not to be
tested in multiple languages, the super-call of the parent's `__init__(...)` is
not necessary.

But in order to reach `Base()`'s `__init__()`, each class that actually uses
i18n and _all_ of their ancestors need to call `super().__init__(page)`.

To use the content of the i18n-yaml files if once read, an object can just call
```python
self._("<locator id>")
```
to get the language dependent content of `<locator id>`.

`self._(key)` is the name of the method that is used by gettext to get the
translated content of a given key. In order to stay in this tradition the
i18n-ized-content-retrieving-method here is also called `_()`.

### Usage

In order to use i18n-ed strings the following steps should be done. Given the
fictitious class `I18nExample(BasePage)` in the module
```bash
src/pages/i18n/common/example.py
```

#### YAML file
First step is to create a yaml file named exactly as the module and placed in
the same directory:
```bash
src/pages/i18n/common/example.yml
```

Its content could look like this:

```yaml
---
I18nExample:
  locator-id:
    de: "dies ist ein deutscher Lokator"
    en: "this is an english locator"
  another-locator-id:
    de: "dies ist ein anderer deutscher Lokator"
    en: "this is another english locator"
```

In order to use an i18n-ized locator in an instance of `I18nExample(BasePage)`,
the yaml file needs to have an item named exactly as the class that wants to use
it. Below the class name items are the locator-ids and below them are the items
for each of the supported languages.

How to use an i18n-ed locator in the code:
```python
self.page.locator(self._("locator-id"))
```
`self._(...)` then returns the fitting string regarding the set language.

A "real code" example taken from Nextcloud's `HomeNavigation()` class:
```python
self.all_files = self.page_part_locator.get_by_role(
    "link", name=self._("all_files")
)
```
and the corresponding yaml:
```yaml
HomeNavigation:
  all_files:
    de: "Alle Dateien"
    en: "All files"
```

#### Setting the language

The language will be set before the test run. All contexts will use that language. 

The language will be set as an environment variable `LANGUAGE` with the value `de` or `en`.

__The format of the language has to be the same as in the yaml files.__
So if the entries in the yamls are `"de"` and `"en"`, the language string given
to the `__init__()` has to match those exactly.

#### Omitting multiple language support
If a component is deployed with only one language there is no need to use i18n
and therefor the extra effort to create yaml files and filling them with
locators can be omitted. In such a case the usage of plain locator strings
inside the code is completely legit. Also, a mix of plain locator strings and
`self._(...)`-substitutions is okay if some locators can be used for all
languages.

#### Guidelines

A detailed description of how to use i18n in the test suite can be found in the [POM guidelines](documentation/pom-guidelines.md).


## Tests

`tests` contains all test-classes and their tests. This is the `pytest`-part of
the system-test suite.

Code at: [tests/](tests/)

### Test structure

Test files are named after the component they are testing. E.g. [`test_nextcloud.py`](tests/test_nextcloud.py) for the Nextcloud tests. Some integration tests might be depended on tests from another component. In this case, those tests should be placed in the same file as the dependent test, to be able to ensure proper execution order when running in parallel.

Tests should not contain any locators, and actions executed are expected to be handled through the page object methods within the steps. They should first declare all variables needed and then execute the test steps from the steps file.

The steps file should contain all the actions that are executed on the page objects. The steps file should be structured in a way that the test file can easily call the steps needed to execute the test.

Example of a test:

```python

from steps import steps_nextcloud

@pytest.mark.user("user")
def test_nextcloud_create_file(logged_in_page: Page, url_portal: str):
  new_file_name = "test_file.txt"

  steps_nextcloud.step_open_page(logged_in_page, url_portal)
  steps_nextcloud.step_open_new_file_dropdown(logged_in_page)
  steps_nextcloud.step_click_new_text_file(logged_in_page)
  steps_nextcloud.step_fill_file_name(logged_in_page, new_file_name)
  ...
```

Example of these steps:

```python
from playwright.sync_api import Page
from pages.base.base import expect
from pages.nextcloud.page import NextcloudPage

@allure.step("Open Nextcloud page")
@screenshot_step
def step_open_page(page: Page, url_portal: str):
  NextcloudPage(page).open_page(url_portal)
  NextcloudPage(page).validate()

@allure.step("Open new file dropdown")
@screenshot_step
def step_open_new_file_dropdown(page: Page):
  nextcloud_page = NextcloudPage(page)

  expect(nextcloud_page.new_file_dropdown).to_be_visible()
  nextcloud_page.new_file_dropdown.click()

@allure.step("Click new text file")
@screenshot_step
def step_click_new_text_file(page: Page):
  nextcloud_page = NextcloudPage(page)

  expect(nextcloud_page.new_file_popper.new_text_file).to_be_visible()
  nextcloud_page.new_file_popper.new_text_file.click()

  expect(nextcloud_page.new_file_modal).to_be_visible()

@allure.step("Fill file name")
@screenshot_step
def step_fill_file_name(page: Page, file_name: str):
  nextcloud_page = NextcloudPage(page)

  expect(nextcloud_page.new_file_modal.file_name_input).to_be_visible()
  nextcloud_page.new_file_modal.file_name_input.fill(file_name)

  expect(nextcloud_page.new_file_modal.create_button).to_be_visible()
  nextcloud_page.new_file_modal.create_button.click()

...
```
