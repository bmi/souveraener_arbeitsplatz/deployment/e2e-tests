# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page, Locator, FrameLocator
from pages.base.base import BasePage, expect
import re
from .page_parts import *

class NextcloudPage(BasePage):
    
    def get_title(self) -> str | re.Pattern[str] | None:
        return re.compile(self._("page_title_re"))

    def get_title_timeout(self) -> float:
        return 1000*60*2
    
    def get_timeout(self) -> float:
        return 1000*60
    
    def open_page(self, url_portal):
        self.page.goto(url_portal.replace("portal.", "files."), timeout=60000)
    
    def __init__(self, page: Page) -> None:
        super().__init__(page)
        self.user_menu: UserMenu = UserMenu(self.page.locator("#header-menu-user-menu"))
        self.header: Header = Header(self.page.locator("#header"))
        
        self.app_body: Locator = self.page.locator("#content-vue")
        self.app_content: Locator = self.app_body.locator("#app-content-vue")
        self.current_popper: Locator = self.page.locator(".v-popper__wrapper")
        self.current_modal: Locator = self.page.locator(".modal-mask:visible")
        self.error_toast: Locator = self.page.locator(".toastify.toast-error")
        self.upload_error_toast: Locator = self.error_toast.filter(has_text=self._("upload_error_text"))

class SettingsPage(NextcloudPage):
    
    def __init__(self, page):
        super().__init__(page)
        self.content: Locator = self.page.locator("#app-content")
        self.navigation: Locator = self.page.locator("#app-navigation")
        self.personal_tiles: Locator = self.navigation.locator("nav.app-navigation-personal")
        self.security_tile: Locator = self.personal_tiles.locator("[data-section-id=\"security\"]")
        
        self.admin_tiles: Locator = self.navigation.locator("nav.app-navigation-administration")
        self.admin_rich_documents_tile: Locator = self.admin_tiles.locator("[data-section-id=\"richdocuments\"]")
        self.admin_groupfolders_tile: Locator = self.admin_tiles.locator("[data-section-id=\"groupfolders\"]")

class PersonalSettingsPage(SettingsPage):
    
    def __init__(self, page):
        super().__init__(page)
    
    def get_title(self):
        return re.compile(self._("page_title_re")) 
    
class SecuritySettingsPage(PersonalSettingsPage):
    
    def __init__(self, page):
        super().__init__(page)
        self.app_password_form: Locator = self.page.locator("form#generate-app-token-section")
        self.app_password_name_input: Locator = self.app_password_form.locator(".input-field__input")
        self.app_password_create_button: Locator = self.app_password_form.locator("button[type=submit]")
        
        self.app_password_modal: AppPasswordModal = AppPasswordModal(self.current_modal)
    
    def open_page(self, url_portal):
        url: str = url_portal
        url = url.replace("portal.", "files.")
        if not url.endswith("/"): url += "/"
        url += "settings/user/security"
        self.page.goto(url)

class OpenProjectIntegrationPage(PersonalSettingsPage):
    
    def open_page(self, url_portal):
        url: str = url_portal
        url = url.replace("portal.", "files.")
        if not url.endswith("/"): url += "/"
        url += "settings/user/openproject"
        self.page.goto(url)
        
    def __init__(self, page):
        super().__init__(page)
        self.openproject_section: Locator = self.page.locator(".openproject-prefs.section")
        self.connect_to_openproject_button: Locator = self.openproject_section.locator("button.oauth-connect--button")
        
        self.openproject_connected_section: Locator = self.page.locator(".openproject-prefs--connected")
        self.disconnect_openproject_button: Locator = self.openproject_connected_section.locator("button.openproject-prefs--disconnect")

class OpenProjectIntegrationAuthorizationPage(BasePage):
    
    def __init__(self, page):
        super().__init__(page)
        self.authorize_button: Locator = self.page.locator("input.button.-primary[type=submit]")
        
class AdminSettingsPage(SettingsPage):
    
    def __init__(self, page):
        super().__init__(page)
    
    def get_title(self):
        return re.compile(self._("page_title_re")) 
    
class RichDocumentsSettingsPage(AdminSettingsPage):
    
    def open_page(self, url_portal):
        url: str = url_portal.replace("portal.", "files.")
        if not url.endswith("/"): url += "/"
        url += "settings/admin/richdocuments"
        self.page.goto(url)
    
    def __init__(self, page):
        super().__init__(page)
        self.font_settings: Locator = self.content.locator("#font-settings")
        self.upload_font_button: Locator = self.font_settings.locator("#uploadlogo")
        self.upload_font_input: Locator = self.font_settings.locator("#settings-file-0")
        self.font_list: Locator = self.font_settings.locator(".settings-entry.font-list-settings")
        self.font_item: Locator = self.font_list.locator("> .settings-font")
    
    def find_font(self, font_file: str):
        return self.font_item.filter(has=self.page.locator(f"> [title=\"{font_file}\"]"))

    def font_delete_button(self, font_file: str):
        return self.find_font(font_file=font_file).locator("> button")

class GroupfoldersSettingsPage(AdminSettingsPage):
    
    def open_page(self, url_portal):
        url: str = url_portal.replace("portal.", "files.")
        if not url.endswith("/"): url += "/"
        url += "settings/admin/groupfolders"
        self.page.goto(url)
    
    def __init__(self, page):
        super().__init__(page)
        self.groupfolders_root: Locator = self.page.locator("#groupfolders-react-root")
        self.new_group_name_input: Locator = self.groupfolders_root.locator("form > .newgroup-name")
        self.new_group_submit: Locator = self.groupfolders_root.locator("form > input[type=submit]")
        self.group_entry: Locator = self.groupfolders_root.locator("tbody > tr", has=self.page.locator("td.remove"))
    
    def get_groupfolder_entry(self, groupfolder_name: str) -> AdminGroupfolderEntry:
        return AdminGroupfolderEntry(self.group_entry.filter(has=self.page.locator(".mountpoint > a", has_text=groupfolder_name)))

class CryptpadPage(NextcloudPage):
    
    def __init__(self, page):
        super().__init__(page)
        self.frame: FrameLocator = self.page.frame_locator("#cryptpad-editor")
        self.sbox_frame: FrameLocator = self.frame.frame_locator("#sbox-iframe")
        self.app_frame: FrameLocator = self.sbox_frame.frame_locator("#cp-app-diagram-content")
        self.app_container: Locator = self.app_frame.locator(".geDiagramContainer")
        self.close_button: Locator = self.page.locator("a#back-button")
   
class FilesPage(NextcloudPage):
    def __init__(self, page: Page) -> None:
        super().__init__(page)
        self.sidebar_left: SidebarLeft = SidebarLeft(self.app_body.locator("> .app-navigation[data-cy-files-navigation]"))
        self.toggle_sidebar_left_button: Locator = self.app_body.locator("button[aria-controls=app-navigation-vue]")
        self.file_list: FileList = FileList(self.app_content)
        self.sidebar_right: SidebarRight = SidebarRight(self.page.locator("#app-sidebar-vue"))
        
        self.file_dropdown_popper: FileDropdownPopper = FileDropdownPopper(self.current_popper)
        self.new_file_popper: NewFilePopper = NewFilePopper(self.current_popper)
        self.batch_actions_popper: BatchActionsPopper = BatchActionsPopper(self.current_popper)
        self.compress_files_modal: CompressFilesModal = CompressFilesModal(self.current_modal)
        self.move_copy_modal: MoveCopyModal = MoveCopyModal(self.current_modal)
        
        self.new_entry_modal: NewEntryModal = NewEntryModal(self.page.locator(".modal-mask:visible[data-cy-files-new-node-dialog]"))
        self.file_settings_modal: FileSettingsModal = FileSettingsModal(self.page.locator(".modal-mask:visible[data-cy-files-navigation-settings]"))
        self.create_work_package_modal: CreateWorkPackageModal = CreateWorkPackageModal(self.page.locator(".modal-mask:visible[data-test-id=\"create-workpackage-modal\"]"))
        self.file_conflict_modal: FileConflictModal = FileConflictModal(self.page.locator(".modal-mask:visible[data-cy-conflict-picker]"))
        
        self.office_viewer: OfficeViewer = OfficeViewer(self.page.locator("#viewer"))
    
    def get_upload_file_input(self) -> Locator:
        return self.file_list.upload_file_input
    
    def toggle_sidebar(self):
        self.toggle_sidebar_left_button.click()

class NextcloudPageNoPermission(BasePage):
    
    def get_timeout(self) -> float:
        return 1000*60
    
    def get_title(self) -> str | re.Pattern[str] | None:
        return self._("page_title")

    def get_title_timeout(self) -> float:
        return 1000*60
    
    def __init__(self, page: Page) -> None:
        super().__init__(page)
    
    def validate(self) -> None:
        super().validate()
        expect(self.page.locator(".wrapper > .v-align > main > div.guest-box > h2")).to_be_visible(timeout=30000)
    
    def open_page(self, url_portal):
        self.page.goto(url_portal.replace("portal.", "files."))
    