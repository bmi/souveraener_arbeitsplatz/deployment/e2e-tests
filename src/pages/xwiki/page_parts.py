# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Locator
import re
from typing import Literal
from pages.base.base import BasePagePart

class Navbar(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.nav_left: Locator = self.locator("#xwikimainmenu > .navbar-left")
        self.nav_right: Locator = self.locator("#xwikimainmenu > .navbar-right")
        
        
        self.logo: Locator = self.locator("#companylogo")
        
        self.services_button: Locator = self.nav_left.locator("#tmWorkplaceServices")
        self.services_list: Locator = self.services_button.locator("> ul.dropdown-menu")
        
        self.drawer_button: Locator = self.nav_right.locator("> li", has=self.page.locator("#tmDrawerActivator"))
        self.drawer_nav: Locator = self.drawer_button.locator("#tmDrawer")
        self.drawer_nav_close_button: Locator = self.drawer_nav.locator("button.drawer-close")
        self.avatar_button: Locator = self.nav_right.locator("> li.navbar-avatar")
        self.notifications_button: Locator = self.nav_right.locator("> li#tmNotifications")
        self.search_button: Locator = self.nav_right.locator("> li", has=self.page.locator("#globalsearch"))
        self.search_button_open: Locator = self.nav_right.locator("> li", has=self.page.locator("#globalsearch:not(.globalsearch-close)"))

class PageNavigation(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.page_list: Locator = self.locator("> .xwikipanelcontents")
        self.current_page: Locator = self.locator(".jstree-anchor.jstree-clicked")
        
    def get_page_in_navigation(self, name: str | re.Pattern[str]):
        return self.page_list.locator("a.jstree-anchor[role=\"treeitem\"]", has_text=name)
        