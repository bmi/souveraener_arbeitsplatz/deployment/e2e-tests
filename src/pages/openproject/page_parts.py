# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Locator
import re
from typing import Literal
from pages.base.base import BasePagePart

class AppHeader(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.start: Locator = self.locator("> .op-app-header--start")
        self.center: Locator = self.locator("> .op-app-header--center")
        self.end: Locator = self.locator("> .op-app-header--end")
        
        self.main_menu_toggle: Locator = self.start.locator("> opce-main-menu-toggle")
        self.logo: Locator = self.start.locator("> .op-logo.opendesk-logo")
        self.project_select_button: Locator = self.page.locator("#projects-menu")
        self.quick_add_button: Locator = self.start.locator("> ul.op-app-menu.op-app-menu_drop-left > .op-app-menu--item.op-quick-add-menu")
        self.quick_add_menu: Locator = self.page.locator("#quick-add-menu")
        self.quick_add_menu_new_task: Locator = self.quick_add_menu.locator("> li.op-menu--item > a.__hl_inline_type_1.op-menu--item-action")

class Sidebar(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.main_menu_ascend_button: Locator = self.locator("[data-action=\"menus--main#ascend\"]")

class ProjectSidebar(Sidebar):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.settings_wrapper: Locator = self.locator("#settings-wrapper")
        self.settings_button: Locator = self.settings_wrapper.locator("> a.settings-menu-item")
        self.settings_toggler: Locator = self.settings_wrapper.locator("> button.toggler")
        
        self.work_packages_wrapper: Locator = self.locator("#main-menu-work-packages-wrapper")
        self.work_packages_button: Locator = self.work_packages_wrapper.locator("> a.work-packages-menu-item")
        self.work_packages_toggler: Locator = self.work_packages_wrapper.locator("> button.toggler")
        
        self.project_storage_button: Locator = self.locator("[data-name=\"settings_project_storages\"]")

class AdminSidebar(Sidebar):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.files_wrapper: Locator = self.locator("#files-wrapper")
        self.files_button: Locator = self.files_wrapper.locator("> a.files-menu-item")
        self.files_toggler: Locator = self.files_wrapper.locator("> button.toggler")
        
        self.external_file_storages_button: Locator = self.locator("[data-name=\"external_file_storages\"]")
        
        self.authentication_wrapper: Locator = self.locator("#authentication-wrapper")
        self.authentication_button: Locator = self.authentication_wrapper.locator("> a.authentication-menu-item")
        self.authentication_toggler: Locator = self.authentication_wrapper.locator("> button.toggler")
        
        self.ldap_authentication_button: Locator = self.locator("[data-name=\"ldap_authentication\"]")
        
        self.mail_and_notifications_wrapper: Locator = self.locator("#mail_and_notifications-wrapper")
        self.mail_and_notifications_button: Locator = self.mail_and_notifications_wrapper.locator("> a.mail-and-notifications-menu-item")
        self.mail_and_notifications_toggler: Locator = self.mail_and_notifications_wrapper.locator("> button.toggler")
        
        self.mail_notifications_button: Locator = self.locator("[data-name=\"mail_notifications\"]")
        
class LdapSourceEntry(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.name: Locator = self.locator("td.name")
        self.host: Locator = self.locator("td.host")
        self.users: Locator = self.locator("td.users")
        self.buttons: Locator = self.locator("td.buttons")
        self.test_button: Locator = self.buttons.locator("a").first

class FirstLoginModal(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.submit_button: Locator = self.locator("[type=\"submit\"]")

class SpotModal(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)

class ProjectListModal(SpotModal):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.header: Locator = self.locator("> .op-project-list-modal--header")
        self.body: Locator = self.locator("> .op-project-list-modal--body")
        self.project_list: Locator = self.body.locator("> [op-header-project-select-list]")
        self.add_button: Locator = self.locator(".button.-primary.spot-action-bar--action", has=self.page.locator("> .spot-icon.spot-icon_add"))
        
    def find_project(self, project_name: str):
        return self.project_list.locator("> li", has_text=re.compile("^" + re.escape(project_name) + "$"))
    
class WorkPackageEntry(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.id: Locator = self.locator("[data-field-name=id]")
        self.subject: Locator = self.locator("[data-field-name=subject]")
        self.type: Locator = self.locator("[data-field-name=type]")
        self.status: Locator = self.locator("[data-field-name=status]")
        self.assignee: Locator = self.locator("[data-field-name=assignee]")
        self.priority: Locator = self.locator("[data-field-name=priority]")
        
        self.more_options_button: Locator = self.locator(".wp-table-context-menu-link.wp-table-context-menu-icon")        

class WorkPackageEditForm(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.subject_input: Locator = self.locator("input#wp-new-inline-edit--field-subject")
        self.project_input: Locator = self.locator("op-project-autocompleter").locator(".ng-input > input")
        self.project_autocomplete: Locator = self.page.locator(".ng-dropdown-panel-items > .scrollable-content")
        self.project_autocomplete_entry: Locator = self.project_autocomplete.locator("> .ng-option")
        self.description_wrapper: Locator = self.locator(".document-editor.ckeditor-type-full")
        self.description_toolbar: Locator = self.description_wrapper.locator(".document-editor__toolbar")
        self.description_textbox: Locator = self.description_wrapper.locator(".document-editor__editable-container")
        
        self.attachments: Locator = self.locator("op-attachments")
        self.attachments_list: Locator = self.attachments.locator("op-attachment-list > ul")
        self.attachments_entry: Locator = self.attachments_list.locator("> li")
        self.attachments_input: Locator = self.attachments.locator("#attachment_files")
        self.attachments_button: Locator = self.attachments.locator("button.op-attachments--upload-button")
        
        self.nextcloud_storage_login_button: Locator = self.locator("op-storage-login-button > button")
        
        self.save_button: Locator = self.locator("#work-packages--edit-actions-save")
        self.cancel_button: Locator = self.locator("#work-packages--edit-actions-cancel")
    
    def get_project_autocomplete(self, project_name: str):
        return self.project_autocomplete_entry.filter(has=self.page.locator(".ng-option-label", has_text=re.compile("^" + re.escape(project_name) + "$")))
    
    def get_attachment(self, name: str):
        return self.attachments_entry.filter(has=self.page.locator(f".op-file-list--item-title[title=\"{name}\"]"))