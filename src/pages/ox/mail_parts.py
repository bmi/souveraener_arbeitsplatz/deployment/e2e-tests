# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Locator, FrameLocator
import re
from typing import Literal
from pages.base.base import BasePagePart, expect
from pages.ox.page_parts import *

class Recipient(BasePagePart):

    def __init__(self, locator):
        super().__init__(locator)
        self.fullname: Locator = self.locator("span.token-label")
        self.remove_button: Locator = self.locator("a.close")

class WriteMailFloatingWindow(FloatingWindow):

    def __init__(self, locator):
        super().__init__(locator)
        self.from_block: FromBlock = FromBlock(self.body.locator("[data-extension-id=\"sender\"]"))

        self.recepient_block: RecipientBlock = RecipientBlock(self.body.locator("[data-extension-id=\"to\"]"))
        self.recepient_input: Locator = self.recepient_block.locator(".tokenfield > .twitter-typeahead > .token-input.tt-input")
        self.recepient_recommend: Locator = self.recepient_block.locator(".tt-dropdown-menu > div > .tt-suggestions[role=\"listbox\"]")
        self.contacts_button: Locator = self.recepient_block.locator(".open-addressbook-popup")

        self.subject_block: Locator = self.body.locator("[data-extension-id=\"subject\"]")
        self.subject_input: Locator = self.subject_block.locator(".mail-input > input[name=\"subject\"]")
        
        self.attachment_button: Locator = self.body.locator(".window-footer > div > .composetoolbar.list-unstyled.list-inline > .dropdown.attachments-dropdown")
        self.attachments_dropdown: Locator = self.page.locator(".dropdown.open.attachments-dropdown > .dropdown-menu")
        self.attachment_input: Locator = self.attachments_dropdown.locator("input[type=\"file\"][name=\"file\"]")
        self.link_to_files_button: Locator = self.attachments_dropdown.locator("[data-name=\"link\"]")
        self.from_files_button: Locator = self.attachments_dropdown.locator("[data-name=\"add\"]")
        self.attachments: Locator = self.body.locator("[data-extension-id=\"attachments\"]")
        self.attachment_preview: Locator = self.attachments.locator("[data-extension-id=\"attachmentPreview\"]")
        self.attachment_preview_list: Locator = self.attachment_preview.locator(".preview-container > .attachment-list")

        self.send_button: Locator = self.body.locator("[data-action=\"send\"]")
    
    def get_mail_attachment_preview(self, filename: str):
        return self.attachment_preview_list.locator("> li.attachment", has=self.page.locator(f"[data-filename=\"{filename}\"]"))
    
    def get_recipient_recommend(self, username: str):
        return self.recepient_recommend.locator(".tt-suggestion", has=self.page.locator(".participant-wrapper > .participant-email", has_text=re.compile(username)))

    def get_recipient_by_name(self, firstname: str, lastname: str) -> Recipient:
        return self.recepient_block.get_recipient_by_name(firstname, lastname)

class FromBlock(BasePagePart):
    def __init__(self, locator: Locator | FrameLocator) -> None:
        super().__init__(locator)
        self.dropdown_link: Locator = self.locator(".sender-dropdown-link")
        self.dropdown_menu: Locator = self.page.locator(".smart-dropdown-container.open ul.dropdown-menu")
        self.sender_entries: Locator = self.dropdown_menu.locator("li")

class RecipientBlock(BasePagePart):

    def __init__(self, locator):
        super().__init__(locator)

        self.recipient_entries = self.locator(".tokenfield > .token")

    def get_recipient_by_name(self, firstname: str, lastname: str) -> Recipient:
        return Recipient(self.recipient_entries.filter(has_text=f"{firstname} {lastname}"))

class AddFolderDropdown(Dropdown):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.add_subfolder_button: Locator = self.list_item.filter(has=self.page.locator("[data-name=\"add-subfolder\"]"))
        self.add_mail_account_button: Locator = self.list_item.filter(has=self.page.locator("[data-name=\"add-mail-account\"]"))

class MailSidebar(Sidebar):
    
    def __init__(self, locator):
        super().__init__(locator)
        
class MailSidepanel(Sidepanel):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.folder_tree: Locator = self.locator(".folder-tree > .tree-container")
        self.standard_folders: MailFolder = MailFolder(self.folder_tree.locator("[data-id=\"virtual/standard\"]"))
        self.remote_folders: Folder = Folder(self.folder_tree.locator("[data-id=\"virtual/remote\"]"))
        self.my_folders: Folder = Folder(self.folder_tree.locator("[data-id=\"virtual/myfolders\"]"))
        self.add_folder_button: Locator = self.my_folders.page_part_locator.locator("button[data-contextmenu=myfolders]")
        self.add_folder_dropdown: AddFolderDropdown = AddFolderDropdown(self.page.locator(".smart-dropdown-container.dropdown.open.context-dropdown"))

class NewFolderModal(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.name_input: Locator = self.locator(".modal-body > .form-group > input")
        self.add_button: Locator = self.locator(".modal-footer > button[data-action=\"add\"]")

class MailFolderSelectModal(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.folder_tree: Locator = self.locator(".modal-body > .folder-tree > .tree-container")
        self.standard_folders: Folder = Folder(self.folder_tree.locator('[data-id="virtual/standard"]'))
        self.my_folders: Folder = Folder(self.folder_tree.locator('[data-id="virtual/myfolders"]'))
        self.footer: Locator = self.locator('.modal-footer')
        self.create_folder_button: Locator = self.footer.locator('button[data-action="create"]')
        self.confirm_button: Locator = self.footer.locator('button[data-action="ok"]')


class MailList(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.folder_name: Locator = self.locator(".folder-info > .folder-name.truncate")
        self.list_view: Locator = self.locator(".list-view.mail-item")
    
    def get_mails(self, status: Literal["all", "read", "unread"] = "all"):
        if status == "all":
            return self.list_view.locator(".list-item.selectable")
        elif status == "read":
            return self.list_view.locator(".list-item.selectable:not(.unread)")
        else:
            return self.list_view.locator(".list-item.selectable.unread")
    
    def get_mail(self, subject: str | None = None, contains_subject: str | None = None, sender: str | None = None, status: Literal["all", "read", "unread"] = "all"):
        mails = self.get_mails(status=status)
        if subject:
            mails = mails.filter(has=self.page.locator(f".subject > [title=\"{subject}\"]"))
        if contains_subject:
            mails = mails.filter(has=self.page.locator(f".subject > [title*=\"{contains_subject}\"]"))        
        if sender:
            mails = mails.filter(has=self.page.locator(f".from[title=\"{sender}\"]"))
        return mails

class MailDetail(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.toolbar: MailDetailToolbar = MailDetailToolbar(self.locator(".classic-toolbar-container"))
        self.mail_content: MailContent = MailContent(self.locator(".thread-view-control > .thread-view-list > .thread-view > article").first)

class MailContent(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.header: MailContentHeader = MailContentHeader(self.locator("header.detail-view-header"))
        self.body: MailContentBody = MailContentBody(self.locator("section.body"))
        self.attachments: MailContentAttachments = MailContentAttachments(self.locator("section.attachments"))

class MailContentAttachments(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.header: Locator = self.locator("> .header")
        self.toggle_details_button: Locator = self.header.locator(".toggle-details")
        self.toolbar: Locator = self.header.locator(".inline-toolbar-container > .inline-toolbar")
        self.save_to_files_button: Locator = self.toolbar.locator("li",  has=self.page.locator("a[data-action=\"io.ox.nextcloud/file-picker/actions/save\"]"))
        self.download_button: Locator = self.toolbar.locator("li",  has=self.page.locator("a[data-action=\"io.ox/mail/attachment/actions/download\"]"))
    
        self.attachment_list: Locator = self.locator(".list-container > .attachment-list")
    
    def expand_details(self):
        expect(self.toggle_details_button).to_be_visible()
        if self.toggle_details_button.get_attribute("aria-expanded") == "false":
            self.toggle_details_button.click()
        expect(self.toggle_details_button).to_have_attribute("aria-expanded", "true")
    
    def get_attachment(self, filename: str):
        return self.attachment_list.locator(f".attachment[title=\"{filename}\"]")

class MailContentHeader(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.from_block: Locator = self.locator(".from")
        self.from_mail: Locator = self.from_block.locator(".person-from.address-only")
        self.subject_recipients: Locator = self.locator(".subject-recipients")
        self.subject: Locator = self.subject_recipients.locator("> .subject")
        self.recipients: Locator = self.subject_recipients.locator("> .recipients")

class MailContentBody(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.frame: FrameLocator = self.frame_locator("iframe.mail-detail-frame")
        self.content: Locator = self.frame.locator("body > .mail-detail-content")
        
        
class MailDetailToolbar(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.delete_button: Locator = self.locator("button[data-action=\"io.ox/mail/actions/delete\"]")
        self.archive_button: Locator = self.locator("button[data-action=\"io.ox/mail/actions/archive\"]")
        self.spam_button: Locator = self.locator("button[data-action=\"io.ox/mail/actions/spam\"]")
        self.reply_button: Locator = self.locator("button[data-action=\"io.ox/mail/actions/reply\"]")
        self.reply_all_button: Locator = self.locator("button[data-action=\"io.ox/mail/actions/reply-all\"]")
        self.forward_button: Locator = self.locator("button[data-action=\"io.ox/mail/actions/forward\"]")
        self.categories_button: Locator = self.locator("button[data-action=\"io.ox/mail/actions/categories\"]")
        self.color_button: Locator = self.locator("button[data-action=\"io.ox/mail/actions/color\"]")
        self.move_button: Locator = self.locator("button[data-action=\"io.ox/mail/actions/move\"]")
        self.more_button: Locator = self.locator("button[data-action=\"more\"]")

class PickerModal(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.file_list: Locator = self.locator("#file-browser")
        self.save_button: Locator = self.locator("#validate > .button-vue--vue-primary")
    
    def get_file(self, filename: str):
        return self.file_list.locator("tbody > tr.selectable", has=self.page.locator("td > div", has_text=filename))

class NextcloudFilePicker(PickerModal):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.link_settings_checkbox: Locator = self.locator("#link-settings")
        self.password_protect_input: Locator = self.locator("#password-protect")
        self.allow_edit_input: Locator = self.locator("#allow-edit")
        self.expiration_datepicker_input: Locator = self.locator("#expiration-datepicker")
        self.expiration_datepicker_list: Locator = self.expiration_datepicker_input.locator(".mx-datepicker-main.mx-datepicker-popup").locator(".mx-calendar-content > table > tbody")
        self.expiration_datepicker_first_valid_date: Locator = self.expiration_datepicker_list.locator("tr > td:not(.disabled)").first

class AddressBookEntry(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.checkmark: Locator = self.locator(".checkmark")
        self.contact_picture: Locator = self.locator(".contact-picture")
        self.last_name: Locator = self.locator(".name > .last_name")
        self.first_name: Locator = self.locator(".name > .first_name")
        self.department: Locator = self.locator(".department")
        self.telephone: Locator = self.locator(".telephone")
        self.position: Locator = self.locator(".position")
        self.mail: Locator = self.locator(".mail")
        self.room: Locator = self.locator(".room")
        self.show_details_button: Locator = self.locator(".show-details")
    
    def is_selected(self):
        return self.page_part_locator.get_attribute("aria-selected") == "true"

class AddressBookModal(BasePagePart):

    def __init__(self, locator):
        super().__init__(locator)
        self.modal_header: Locator = self.locator(".modal-header")
        self.topbar: Locator = self.modal_header.locator(".top-bar")
        self.address_list_select = self.topbar.locator("select[name=\"selectedList\"]")
        self.search_query_input = self.topbar.locator("input[name=\"searchQuery\"]")

        self.modal_body: Locator = self.locator(".modal-body")
        self.mailbox_list: Locator = self.modal_body.locator("ul.contact-list-view")
        self.mailbox_list_item: Locator = self.mailbox_list.locator("li")
        
        self.modal_footer: Locator = self.locator(".modal-footer")
        self.cancel_button: Locator = self.modal_footer.locator("button[data-action=\"cancel\"]")
        self.select_button: Locator = self.modal_footer.locator("button[data-action=\"select\"]")

    def get_functional_mailbox_name(self) -> str:
        return self._("functional_mailboxes")
        
    def get_first_mailbox(self):
        return self.mailbox_list_item.nth(0)

    def get_entry_by_email(self, email: str | re.Pattern[str]) -> AddressBookEntry:
        return AddressBookEntry(self.mailbox_list_item.filter(has=self.page.locator(".mail", has_text=email)))

