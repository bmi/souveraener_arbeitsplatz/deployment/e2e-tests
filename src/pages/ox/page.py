# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Page, Locator
import re
from pages.base.base import BasePage, expect
from .page_parts import *
from .mail_parts import *
from .calendar_parts import *
from .contacts_parts import *
from .tasks_parts import *
        
class OXPage(BasePage):
    
    def get_title(self) -> str | re.Pattern[str] | None:
        return re.compile("OX App Suite$")
    
    def get_url(self, url_portal: str):
        return url_portal.replace("portal.", "webmail.")
    
    def open_page(self, url_portal):
        self.page.goto(self.get_url(url_portal))
    
    def __init__(self, page: Page) -> None:
        super().__init__(page=page)
        
        self.top_app_control: Locator = self.page.locator("#io-ox-appcontrol")
        self.body_locator: Locator = self.page.locator("#io-ox-screens").locator("#io-ox-windowmanager-pane > div:visible")
        self.container: Locator = self.body_locator.locator("> .window-container-center")
        self.body: Locator = self.container.locator("> .window-body")
        
        self.top_left_bar: TopLeftBar = TopLeftBar(self.top_app_control.locator("#io-ox-topleftbar"))
        self.search_bar: SearchBar = SearchBar(self.top_app_control.locator("#io-ox-topsearch"))
        self.top_right_bar: TopRightBar = TopRightBar(self.top_app_control.locator("#io-ox-toprightbar"))
        
        self.active_modal: Modal = Modal(self.page.locator("body > .modal:visible"))
        self.active_floating_window: FloatingWindow = FloatingWindow(self.page.locator(".floating-window.active[data-app-name][data-window-nr]"))
        
        self.first_start_modal: FirstStartModal = FirstStartModal(self.active_modal.page_part_locator)
        
        self.sidebar: Sidebar = Sidebar(self.container.locator("> .window-sidebar"))
        self.sidepanel: Sidepanel = Sidepanel(self.container.locator("> .window-sidepanel"))
        
        self.message_container: Locator = self.page.locator("#io-ox-message-container")
        
        self.alert: Locator = self.page.locator("body > .io-ox-alert")
        self.error_alert: Locator = self.page.locator("body > .io-ox-alert.io-ox-alert-error")

        self.nextcloud_file_picker_modal: NextcloudFilePicker = NextcloudFilePicker(self.page.locator(".modal-wrapper > .modal-container > .picker__content"))
        
        self.address_book_modal: AddressBookModal = AddressBookModal(self.page.locator(".modal-dialog > .modal-content"))
    
    def current_new_button(self):
        button: Locator
        if self.sidebar.is_visible():
            button = self.sidebar.add_button
        else:
            button = self.sidepanel.add_button
        return button
        
    def validate(self) -> None:
        super().validate()
        self.page.locator("#background-loader").wait_for(state="hidden", timeout=1000*60*3)
        
        try:
            expect(self.first_start_modal).to_be_visible(timeout=3000)
            expect(self.first_start_modal.no_button).to_be_visible()
            self.first_start_modal.no_button.click()
        except:
            pass

class MailPage(OXPage):
    
    def get_url(self, url_portal):
        url: str = super().get_url(url_portal)
        
        if not url.endswith("/"):
            url += "/"
        
        url += "appsuite/#!!&app=io.ox/mail"
        
        return url
    
    def __init__(self, page: Page) -> None:
        super().__init__(page)
        
        self.mail_list: MailList = MailList(self.body.locator("[id=\"io.ox/mail\"]"))
        self.mail_detail: MailDetail = MailDetail(self.body.locator("[data-page-id=\"io.ox/mail/detailView\"]"))
        
        self.sidebar: MailSidebar = MailSidebar(self.sidebar.page_part_locator)
        self.sidepanel: MailSidepanel = MailSidepanel(self.sidepanel.page_part_locator)
        self.write_mail_floating_window: WriteMailFloatingWindow = WriteMailFloatingWindow(self.active_floating_window.page_part_locator)
        self.save_draft_modal: SaveDraftModal = SaveDraftModal(self.active_modal.page_part_locator)
        
        self.mail_progress_message: Locator = self.message_container.locator(".mail-send-progress:visible").first
        self.mail_progress_message_state_send: Locator = self.mail_progress_message.locator(".mail-send-progress-icon > .bi-check-circle")
        
        self.malicious_file_modal: Locator = self.page.locator("[data-point=\"io.ox/core/download/antiviruspopup\"]")
        self.picker_modal: PickerModal = PickerModal(self.page.locator(".modal-wrapper > .modal-container > .picker__content"))
        self.new_folder_modal: NewFolderModal = NewFolderModal(self.page.locator(".modal[data-point=\"io.ox/core/folder/add-popup\"]"))
        self.mail_folder_select_modal: MailFolderSelectModal = MailFolderSelectModal(self.page.locator(".modal[data-point=\"io.ox/core/folder/picker\"]"))

    def validate(self):
        super().validate()
        expect(self.sidepanel.standard_folders).to_be_visible()
        self.sidepanel.standard_folders.expand()
        expect(self.sidepanel.standard_folders.inbox).to_be_visible()

class CalendarPage(OXPage):
    
    def __init__(self, page: Page) -> None:
        super().__init__(page)
        
        self.current_calendar: Calendar = Calendar(self.body.locator(".io-ox-pagecontroller.current"))
        
        self.sidebar: CalendarSidebar = CalendarSidebar(self.sidebar.page_part_locator)
        self.sidepanel: CalendarSidepanel = CalendarSidepanel(self.sidepanel.page_part_locator)
        self.edit_appointment_floating_window: EditAppointmentFloatingWindow = EditAppointmentFloatingWindow(self.active_floating_window.page_part_locator)
        self.appointment_detail_popup: AppointmentDetailPopup = AppointmentDetailPopup(self.page.locator(".detail-popup.detail-popup-appointment"))
        
    def get_url(self, url_portal):
        url: str = super().get_url(url_portal)
        
        if not url.endswith("/"):
            url += "/"
        
        url += "appsuite/#!!&app=io.ox/calendar"
        
        return url

class ContactsPage(OXPage):

    def __init__(self, page: Page) -> None:
        super().__init__(page)

        self.sidebar: ContactsSidebar = ContactsSidebar(self.sidebar.page_part_locator)
        self.sidepanel: ContactsSidepanel = ContactsSidepanel(self.sidepanel.page_part_locator)

        self.edit_contact_modal_locator = self.page.locator('.io-ox-contacts-edit-window.floating-window')
        self.edit_contact_modal: ContactEditModal = ContactEditModal(self.edit_contact_modal_locator)

        self.contacts_main_window_locator = self.page.locator('[role="main"].window-body')
        self.contacts_main_window: ContactsMainWindow = ContactsMainWindow(self.contacts_main_window_locator)

        self.confirmation_modal_locator: Locator  = self.page.locator('.modal-dialog[role="document"]')
        self.confirmation_modal: ConfirmationModal = ConfirmationModal(self.confirmation_modal_locator)

    def get_url(self, url_portal):
        url: str = super().get_url(url_portal)

        if not url.endswith("/"):
            url += "/"

        url += "appsuite/#!!&app=io.ox/contacts"

        return url

class TasksPage(OXPage):
    def __init__(self, page: Page) -> None:
        super().__init__(page=page)

        self.new_tasks_button: Locator = self.current_new_button()
        self.create_task_dialog: CreateTaskDialog =  CreateTaskDialog(self.page.locator(".floating-window-content")) 

        self.address_book_modal: AddressBookModal = AddressBookModal(self.page.locator(".modal-dialog > .modal-content"))

    def get_url(self, url_portal: str):
        tasks_url = url_portal.replace("portal.", "webmail.")
        return tasks_url + "appsuite/#app=io.ox/tasks"

