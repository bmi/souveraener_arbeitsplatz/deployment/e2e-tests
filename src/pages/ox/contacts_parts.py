# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Locator
from pages.base.base import BasePagePart
from pages.ox.page_parts import *

CONTACT_FORM_FIELDS = [
  "file",
  "title",
  "first_name",
  "last_name",
  "display_name",
  "nickname",
  "second_name",
  "suffix",
  "marital_status",
  "number_of_children",
  "url",
  "company",
  "department",
  "position",
  "profession",
  "room_number",
  "manager_name",
  "assistant_name",
  "employee_type",
  "number_of_employees",
  "sales_volume",
  "tax_id",
  "commercial_register",
  "branches",
  "business_category",
  "info",
  "email1",
  "email2",
  "email3",
  "instant_messenger1",
  "instant_messenger2",
  "cellular_telephone1",
  "cellular_telephone2",
  "telephone_business1",
  "telephone_business2",
  "telephone_home1",
  "telephone_home2",
  "telephone_company",
  "telephone_other",
  "telephone_car",
  "telephone_isdn",
  "telephone_pager",
  "telephone_primary",
  "telephone_radio",
  "telephone_telex",
  "telephone_ttytdd",
  "telephone_ip",
  "telephone_assistant",
  "telephone_callback",
  "fax_business",
  "fax_home",
  "fax_other",
  "street_home",
  "postal_code_home",
  "city_home",
  "state_home",
  "country_home",
  "street_business",
  "postal_code_business",
  "city_business",
  "state_business",
  "country_business",
  "street_other",
  "postal_code_other",
  "city_other",
  "state_other",
  "country_other",
  "private_flag",
  "userfield01",
  "userfield02",
  "userfield03",
  "userfield04",
  "userfield05",
  "userfield06",
  "userfield07",
  "userfield08",
  "userfield09",
  "userfield10",
  "userfield11",
  "userfield12",
  "userfield13",
  "userfield14",
  "userfield15",
  "userfield16",
  "userfield17",
  "userfield18",
  "userfield19",
  "userfield20"
]

class ContactEditModal(BasePagePart):

    def __init__(self, locator):
        super().__init__(locator)
        self.modal_header: Locator = self.locator(".floating-header")
        self.controls = self.modal_header.locator('[role="toolbar"].controls')
        self.minimize_control = self.controls.locator('[data-action="minimize"].btn')
        self.maximize_control = self.controls.locator('[data-action="maximize"].btn')
        self.close_control = self.controls.locator('[data-action="close"].btn')

        self.modal_body: Locator = self.locator(".floating-body")

        self.first_name_input = self.modal_body.locator('input[name="first_name"]')
        self.last_name_input = self.modal_body.locator('input[name="last_name"]')
        self.email_input = self.modal_body.locator('input[name="email1"]')

        self.form_fields = {
            i: self.modal_body.locator(f'input[name="{i}"]')
            for i in CONTACT_FORM_FIELDS
        }

        self.save_button = self.locator('button[data-action="save"]')
        self.discard_button = self.locator('button[data-action="discard"]')

        self.new_contact_modal_title = self._("new_contact_modal_title")
        self.edit_contact_modal_title = self._("edit_contact_modal_title")


    def get_title(self):
        return self.modal_header.text_content().strip()


class ConfirmationModal(BasePagePart):

    def __init__(self, locator):
        super().__init__(locator)

        self.delete_button: Locator = self.locator('[data-action="delete"]')
        self.cancel_button: Locator = self.locator('.btn-default[data-action="cancel"]')

class ContactsMainWindow(BasePagePart):

    def __init__(self, locator):
        super().__init__(locator)

        self.contact_list = self.locator('[aria-label="Kontakte"]')
        self.detail_view = self.locator('[aria-label="Kontakt-Details"]')

        self.edit_contact_button = self.detail_view.locator('[data-action="io.ox/contacts/actions/update"]')
        self.delete_contact_button = self.detail_view.locator('[data-action="io.ox/contacts/actions/delete"]')

    def get_contact_list_entry_by_name(self, first_name: str, last_name: str):
        return self.contact_list.locator(f'[aria-label="{last_name}, {first_name}"]')

    def get_detail_view_current_contact_name(self):
        full_name_locator = self.detail_view.locator('h2.fullname')
        first_name = full_name_locator.locator('.first_name').text_content()
        last_name = full_name_locator.locator('.last_name').text_content()
        return first_name, last_name

    def get_detail_view_current_email(self):
        email_locator = self.detail_view.locator('section[data-block="communication"] a[href^="mailto"]')
        return email_locator.text_content()



class ContactsSidebar(Sidebar):

    def __init__(self, locator):
        super().__init__(locator)

class ContactsSidepanel(Sidepanel):

    def __init__(self, locator):
        super().__init__(locator)

        self.folder_tree: Locator = self.locator(".folder-tree > .tree-container")
        self.private_contacts: Folder = Folder(self.folder_tree.locator("[data-id='virtual/flat/contacts/private']"))
        self.public_contacts: Folder = Folder(self.folder_tree.locator("[data-id='virtual/flat/contacts/public']"))

        self.collected_addresses: Folder = self.private_contacts.get_subfolder(self._("collected_addresses_folder_name"))
        #self.without_organisation: Folder = self.public_contacts.get_subfolder("Ohne Organisation")

