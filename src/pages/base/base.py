# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from functools import wraps
import inspect
from typing import Optional, Union, overload
from playwright.sync_api import Page, Locator, Expect, FrameLocator, APIResponse
from playwright.sync_api._generated import PageAssertions, LocatorAssertions, APIResponseAssertions
import re
import os
from os.path import splitext

import yaml

def allow_fail(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return_value = func(*args, **kwargs)
            return return_value
        except Exception as e:
            return e
    
    return wrapper
class Base:
    
    def __init__(self) -> None:
        super().__init__()
        self.lang = None
        self._locators = dict()
        self.load_lang()
    
    def load_lang(self):
        lang = os.environ["LANGUAGE"] or "de"
        self.set_lang(lang)

    def set_lang(self, lang: str):
        if lang is None:
            return
        self.lang = lang
        self.get_locator_keys()
        
    @staticmethod
    def get_locator_yaml(cls) -> str:
        module_name = inspect.getfile(cls)
        return f"{splitext(module_name)[0]}.yml"

    def get_locator_keys(self):
        """
        get_locator_keys() iterates over all base classes of self and adds all
        locators found for that class.
        """
        for base_class in inspect.getmro(type(self)):
            # 'object' is the last class-object in the inheritance chain and
            # therefore the end-condition of the recursion
            if base_class == object:
                return

            self._get_locators(base_class)

    def _get_locators(self, cls):
        class_name = cls.__name__
        locators_yaml_file = BasePage.get_locator_yaml(cls)
        try:
            with open(locators_yaml_file, 'r', encoding="utf-8") as file:
                all_module_locators = yaml.safe_load(file)
                self._locators[class_name] = all_module_locators[class_name]
        except Exception as e:
            pass
    
    def _(self, locator_id: str, *args) -> str:
        """
        _() is named as the translate-method in gettext is usually named.
        It is used to look up and return a locator with a given locator key and
        a previously set language.
        """
        if self._locators is None:
            raise Exception(
                """Locators have not been loaded. This either indicates that
 get_locator_keys() was not called before this or that there is
 no locators-containing yaml file for this module."""
            )

        # inspect.getmro(type(self)) iterates over all base classes of self
        for base_class in inspect.getmro(type(self)):
            class_name = base_class.__name__
            if class_name not in self._locators:
                continue
            if locator_id not in self._locators[class_name]:
                continue
            return self._locators[class_name][locator_id][self.lang].format(*args)

        raise Exception(
            """No locator related to this class have been found. Most likely
  the locator-yaml file does not contain keys for this class."""
        )

class BasePage(Base):
    
    def __init__(self, page: Page) -> None:
        super().__init__()
        self.page = page
        self.locator = page.locator
        self.frame_locator = page.frame_locator
        self.check_title: bool = True
    
    def open_page(self, url_portal: str):
        raise NotImplementedException(self.open_page)

    def get_title(self) -> str | re.Pattern[str] | None:
        return None

    def get_title_timeout(self) -> float:
        return 30000
    
    def get_timeout(self) -> float:
        return 60000
    
    def validate(self) -> None:
        self.page.wait_for_load_state("domcontentloaded", timeout=self.get_timeout())
        if self.get_title() and self.check_title:
            expect(self.page).to_have_title(self.get_title(), timeout=self.get_title_timeout())
        self.page.wait_for_timeout(1000)
        
        # Remove cookie banner
        self.page.evaluate("document.querySelector('body > .cookie-banner')?.remove()")


class BasePagePart(Base):
    
    def __init__(self, locator: Locator | FrameLocator) -> None:
        super().__init__()
        self.page_part_locator: Locator | FrameLocator = locator
        self.expect_locator: Locator = locator
        if isinstance(locator, FrameLocator):
            self.page: Page = locator.owner.page
            self.expect_locator = locator.owner
        else:
            self.page: Page = locator.page
        self.locator = locator.locator
        self.frame_locator = locator.frame_locator

class HybridBasePage(BasePage):
    
    def __init__(self, page_or_locator: Page | Locator | FrameLocator) -> None:
        if isinstance(page_or_locator, Page):
            self.page = page_or_locator
        else:
            self.check_title = False
            if isinstance(page_or_locator, Locator):
                self.page = page_or_locator.page
            elif isinstance(page_or_locator, FrameLocator):
                self.page = page_or_locator.owner.page
            else:
                raise Exception("Invalid type for page_or_locator")
        
        super().__init__(self.page)
        
        self.locator = page_or_locator.locator
        self.frame_locator = page_or_locator.frame_locator
    
class NotImplementedException(Exception):
    
    def __init__(self, func, *args):
        msg = f"Function not implemented: {func.__name__}"
        args = [msg].extend(args)
        super().__init__(*args)

class POMExpect(Expect):
    """
    This class extends the playwright.sync_api.Expect class to allow for the use of the expect-method in the POM.
    """
    
    def __init__(self):
        super().__init__()
        
    @overload
    def __call__(self, actual: Page, message: str = None) -> PageAssertions:
        ...
    
    @overload
    def __call__(self, actual: Locator, message: str = None) -> LocatorAssertions:
        ...
    
    @overload
    def __call__(self, actual: APIResponse, message: str = None) -> APIResponseAssertions:
        ...
    
    @overload
    def __call__(self, actual: FrameLocator, message: str = None) -> LocatorAssertions:
        ...
    
    @overload
    def __call__(self, actual: BasePage, message: str = None) -> PageAssertions:
        ...

    @overload
    def __call__(self, actual: BasePagePart, message: str = None) -> LocatorAssertions:
        ...

    def __call__(
        self, actual: Union[Page, Locator, APIResponse, FrameLocator, BasePage, BasePagePart], message: Optional[str] = None
    ) -> Union[PageAssertions, LocatorAssertions, APIResponseAssertions]:
        if isinstance(actual, BasePage):
            return super().__call__(actual.page, message)
        elif isinstance(actual, BasePagePart):
            return super().__call__(actual.expect_locator, message)
        elif isinstance(actual, FrameLocator):
            return super().__call__(actual.owner, message)
        else:
            return super().__call__(actual, message)
    
expect = POMExpect()