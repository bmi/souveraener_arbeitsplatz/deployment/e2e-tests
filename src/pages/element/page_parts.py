# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

import re
from playwright.sync_api import Locator, FrameLocator, Page
from pages.base.base import BasePagePart

class Dialog(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.primary_button: Locator = self.locator(".mx_Dialog_buttons").locator(".mx_Dialog_primary")
        self.warning_button: Locator = self.locator(".mx_Dialog_buttons").locator(".warning")

class StaticDialog(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.primary_button: Locator = self.locator(".mx_Dialog_buttons").locator(".mx_Dialog_primary")

class SpacePanel(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.toggle_expand_button: Locator = self.locator(".mx_SpacePanel_toggleCollapse")
        self.user_menu_button: Locator = self.locator(".mx_UserMenu")
        self.threads_button: Locator = self.locator(".mx_ThreadsActivityCentre_container")
        self.quick_settings_button: Locator = self.locator(".mx_AccessibleButton.mx_QuickSettingsButton")
    
    def is_expanded(self):
        return "expanded" in self.toggle_expand_button.get_attribute("class").split(" ")

class NewRoomMenu(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.new_room_button: Locator = self.locator(".mx_IconizedContextMenu_optionList > li").first
        self.explore_public_rooms_button: Locator = self.locator(".mx_IconizedContextMenu_optionList > li").last

class RoomKnock(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.decline_button: Locator = self.locator("> .mx_RoomKnocksBar_action.mx_AccessibleButton_kind_icon_primary_outline")
        self.accept_button: Locator = self.locator("> .mx_RoomKnocksBar_action.mx_AccessibleButton_kind_icon_primary")
        
class RoomList(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.new_room_button: Locator = self.locator("[aria-labelledby=\"mx_RoomSublist_label_im.vector.fake.recent\"]").locator(".mx_AccessibleButton.mx_RoomSublist_auxButton")
        self.new_conversation_button: Locator = self.locator("[aria-labelledby=\"mx_RoomSublist_label_im.vector.fake.direct\"]").locator(".mx_AccessibleButton.mx_RoomSublist_auxButton")
        self.room_invite_list: Locator = self.locator(".mx_RoomSublist[aria-labelledby=\"mx_RoomSublist_label_im.vector.fake.invite\"]").locator(".mx_RoomSublist_tiles")
        self.people_room_list: Locator = self.locator(".mx_RoomSublist[aria-labelledby=\"mx_RoomSublist_label_im.vector.fake.direct\"]").locator(".mx_RoomSublist_tiles")
        self.room_list: Locator = self.locator(".mx_RoomSublist[aria-labelledby=\"mx_RoomSublist_label_im.vector.fake.recent\"]").locator(".mx_RoomSublist_tiles")
        self.new_room_menu: NewRoomMenu = NewRoomMenu(self.page.locator("#mx_ContextualMenu_Container"))

    def find_room_chat_invitation(self, firstname: str, lastname: str):
        return self.room_invite_list.locator(f"> .mx_RoomTile", has=self.page.locator(f"> .mx_RoomTile_titleContainer > .mx_RoomTile_title[title=\"{firstname} {lastname}\"]"))

    def find_room_invitation(self, room_name: str):
        return self.room_invite_list.locator("> .mx_RoomTile", has=self.page.locator(f"> .mx_RoomTile_titleContainer > .mx_RoomTile_title[title=\"{room_name}\"]"))

    def find_person(self, person_name: str):
        return self.people_room_list.locator("> .mx_RoomTile", has=self.page.locator(f"> .mx_RoomTile_titleContainer > .mx_RoomTile_title[title=\"{person_name}\"]"))

    def find_room(self, room_name: str):
        return self.room_list.locator("> .mx_RoomTile", has=self.page.locator(f"> .mx_RoomTile_titleContainer > .mx_RoomTile_title[title=\"{room_name}\"]"))

class RoomCallMessage(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.call: Locator = self.locator(".mx_EventTile_line > .mx_LegacyCallEvent_wrapper > .mx_LegacyCallEvent")
        self.info: Locator = self.call.locator(".mx_LegacyCallEvent_info")
        self.sender: Locator = self.info.locator(".mx_LegacyCallEvent_sender")
        self.call_type: Locator = self.info.locator(".mx_LegacyCallEvent_type")
        self.content: Locator = self.call.locator(".mx_LegacyCallEvent_content")
        
        self.primary_button: Locator = self.content.locator(".mx_LegacyCallEvent_content_button.mx_AccessibleButton_kind_primary")  
        self.danger_button: Locator = self.content.locator(".mx_LegacyCallEvent_content_button.mx_AccessibleButton_kind_danger")
        

class RoomPreview(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.textarea: Locator = self.locator(".mx_Field_textarea > textarea")
        self.action_buttons: Locator = self.locator(".mx_RoomPreviewBar_actions")
        self.join_room_button: Locator = self.action_buttons.locator(".mx_AccessibleButton_kind_primary")

class RoomCallView(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.resize_wrapper: Locator = self.locator(".mx_LegacyCallViewForRoom_ResizeWrapper")
        self.call_view: Locator = self.resize_wrapper.locator(".mx_LegacyCallView")
        self.header: Locator = self.call_view.locator(".mx_LegacyCallViewHeader")
        self.header_icon: Locator = self.header.locator(".mx_LegacyCallViewHeader_icon")
        self.header_text: Locator = self.header.locator(".mx_LegacyCallViewHeader_text")
        self.header_controls: Locator = self.header.locator(".mx_LegacyCallViewHeader_controls")
        self.fullscreen_button: Locator = self.header_controls.locator(".mx_LegacyCallViewHeader_button_fullscreen")
        
        self.content_wrapper: Locator = self.call_view.locator(".mx_LegacyCallView_content_wrapper")
        self.content: Locator = self.content_wrapper.locator(".mx_LegacyCallView_content")
        self.video_feed: Locator = self.content.locator(".mx_VideoFeed")
        self.mic_status: Locator = self.video_feed.locator(".mx_VideoFeed_mic")
        self.avatar: Locator = self.video_feed.locator("._avatar_mcap2_17")
        
        self.buttons: Locator = self.call_view.locator(".mx_LegacyCallViewButtons")
        self.mic_button: Locator = self.buttons.locator(".mx_LegacyCallViewButtons_button_mic")
        self.camera_button: Locator = self.buttons.locator(".mx_LegacyCallViewButtons_button_vid")
        self.screensharing_button: Locator = self.buttons.locator(".mx_LegacyCallViewButtons_button_screensharing")
        self.more_button: Locator = self.buttons.locator(".mx_LegacyCallViewButtons_button_more")
        self.hangup_button: Locator = self.buttons.locator(".mx_LegacyCallViewButtons_button_hangup")

    def move_mouse(self):
        box = self.content.bounding_box()
        x, y, w, h = box["x"], box["y"], box["width"], box["height"]
        x1, y1 = x + (w/2) - 10, y + (h/2) - 10
        x2, y2 = x1 + 20, y1+20
        self.page.mouse.move(x=x1, y=y1)
        self.page.mouse.move(x=x2, y=y2, steps=50)

class HomePage(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.open_meeting_scheduler_button: Locator = self.locator(".mx_HomePage_body > .mx_AccessibleButton_kind_primary")
        
class CurrentRoom(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.room_preview: RoomPreview = RoomPreview(self.locator(".mx_RoomPreviewBar_dialog"))
        self.home_page: HomePage = HomePage(self.locator(".mx_HomePage"))

        self.room_header: Locator = self.locator("header.mx_RoomHeader")
        self.room_title: Locator = self.room_header.locator(".mx_RoomHeader_info > .mx_RoomHeader_heading > .mx_RoomHeader_truncated")
        self.header_button: Locator = self.room_header.locator("button[class^=\"_icon-button\"]")
        self.video_call_button: Locator = self.header_button.nth(0)
        self.voice_call_button: Locator = self.header_button.nth(1)
        self.thread_button: Locator = self.header_button.nth(2)
        self.room_info_button: Locator = self.header_button.nth(3)
        
        self.room_call_view: RoomCallView = RoomCallView(self.locator(".mx_LegacyCallViewForRoom"))
        
        self.text_input: Locator = self.locator(".mx_BasicMessageComposer_input")
        self.send_button: Locator = self.locator(".mx_AccessibleButton.mx_MessageComposer_sendMessage")
        self.message_list: Locator = self.locator(".mx_RoomView_MessageList")
        self.invite_button: Locator = self.locator(".mx_NewRoomIntro_buttons > .mx_NewRoomIntro_inviteButton ")

    def get_knock(self, username: str) -> RoomKnock:
        return RoomKnock(self.locator(".mx_RoomKnocksBar", has=self.page.locator(f"> .mx_RoomKnocksBar_avatar [title=\"{username}\"]")))
        
    def get_knock_fullname(self, firstname: str, lastname: str) -> RoomKnock:
        return RoomKnock(self.locator(".mx_RoomKnocksBar", has=self.page.locator(f"> .mx_RoomKnocksBar_content > .mx_RoomKnocksBar_paragraph", has_text=re.compile(f"^{firstname} {lastname}"))))

    def get_knock_guest(self, guest_name: str):
        return RoomKnock(self.locator(".mx_RoomKnocksBar", has=self.page.locator(f"> .mx_RoomKnocksBar_content > .mx_RoomKnocksBar_paragraph", has_text=re.compile(f"^{guest_name}"))))    

    def get_message(self, message: str | re.Pattern[str]) -> Locator:
        return self.message_list.locator("li.mx_EventTile", has=self.page.locator(".mx_EventTile_line > .mx_MTextBody > .mx_EventTile_body", has_text=message))

    def get_latest_call_message(self) -> RoomCallMessage:
        return RoomCallMessage(self.message_list.locator("li.mx_EventTile", has=self.page.locator(".mx_EventTile_line > .mx_LegacyCallEvent_wrapper > .mx_LegacyCallEvent")).last)
    
    def get_room_header_with_internal_name(self, firstname: str, lastname: str) -> Locator:
        return self.get_room_header(room_name=f"{firstname} {lastname}")
        
    def get_room_header_with_name(self, username: str) -> Locator:
        name_truncated: str = re.search("@([a-zA-Z-_.0-9]*):(.*)", username).groups()[0]
        return self.get_room_header(room_name=name_truncated)
    
    def get_room_header(self, room_name: str) -> Locator:
        return self.room_header.filter(has_text=room_name)
    
class Navbar(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.navbar_logo: Locator = self.locator("> a[href]").first
        self.menu_button: Locator = self.locator("> button[aria-haspopup=\"true\"]")
        self.menu_list: Locator = self.locator("[data-testid=\"menu-list\"]")
        self.menu_list_items_header: Locator = self.menu_list.locator("> li > span")
        self.menu_list_items: Locator = self.menu_list.locator("> li > ul")

class SpotlightEntry(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.end_adornment: Locator = self.locator(".mx_SpotlightDialog_option--endAdornment")
        self.enter_prompt_button: Locator = self.end_adornment.locator("> .mx_SpotlightDialog_enterPrompt")
        self.primary_button: Locator = self.end_adornment.locator("> .mx_AccessibleButton_kind_primary")

class PublicRoomSpotlightEntry(SpotlightEntry):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.join_button: Locator = self.primary_button
        self.details: Locator = self.locator(".mx_SpotlightDialog_result_publicRoomDetails")

class SpotlightDialog(StaticDialog):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.search_area: Locator = self.locator(".mx_SpotlightDialog_searchBox")
        self.search_input: Locator = self.search_area.locator("> input")
        self.content: Locator = self.locator("#mx_SpotlightDialog_content")
        self.result: Locator = self.content.locator("li.mx_SpotlightDialog_option")
    
    def get_public_room_result_by_id(self, room_id: str) -> PublicRoomSpotlightEntry:
        return PublicRoomSpotlightEntry(self.result.filter(has=self.page.locator(".mx_SpotlightDialog_result_publicRoomDetails > .mx_SpotlightDialog_result_publicRoomHeader > .mx_SpotlightDialog_result_publicRoomAlias", has_text=room_id)))

    def get_public_room_result_by_name(self, room_name: str) -> PublicRoomSpotlightEntry:
        return PublicRoomSpotlightEntry(self.result.filter(has=self.page.locator(".mx_SpotlightDialog_result_publicRoomDetails > .mx_SpotlightDialog_result_publicRoomHeader > .mx_SpotlightDialog_result_publicRoomName", has_text=room_name)))

class NewRoomDialog(Dialog):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.name_input: Locator = self.locator(".mx_CreateRoomDialog_name > input")
        self.topic_input: Locator = self.locator(".mx_CreateRoomDialog_topic > input")
        
        self.join_rule_dropdown: Locator = self.locator("#mx_JoinRuleDropdown_value")
        self.join_rule_knock: Locator = self.locator("#mx_JoinRuleDropdown__knock")
        self.join_rule_invite: Locator = self.locator("#mx_JoinRuleDropdown__invite")
        self.join_rule_public: Locator = self.locator("#mx_JoinRuleDropdown__public")
        
        self.room_address_input: Locator = self.locator(".mx_RoomAliasField > input")
        self.advanced_settings: Locator = self.locator("details.mx_CreateRoomDialog_details")
        self.advanced_settings_summary: Locator = self.advanced_settings.locator("> summary")
        self.block_extern_toggle: Locator = self.advanced_settings.locator(".mx_SettingsFlag").first.locator("> .mx_ToggleSwitch")
        self.block_extern_toggle_on: Locator = self.advanced_settings.locator(".mx_SettingsFlag").first.locator("> .mx_ToggleSwitch_on")
        
        self.create_room_button: Locator = self.primary_button
    
class RequestRoomAccessDialog(Dialog):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.name_input: Locator = self.locator(".mx_Field.mx_Field_input > input[type=text]")
        self.continue_as_guest_button: Locator = self.locator(".mx_CompoundDialog_footer > .mx_AccessibleButton_kind_primary")

class ErrorDialog(Dialog):
    
    def __init__(self, locator):
        super().__init__(locator)

class InviteUserDialog(StaticDialog):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.address_bar: Locator = self.locator(".mx_InviteDialog_addressBar")
        self.text_input: Locator = self.address_bar.locator(".mx_InviteDialog_editor > input")
        self.invite_button: Locator = self.address_bar.locator(".mx_InviteDialog_buttonAndSpinner > [role=\"button\"]")
        self.users_lists: Locator = self.locator(".mx_InviteDialog_userSections > .mx_InviteDialog_section")
    
    def find_user_by_name(self, firstname: str, lastname: str):
        return self.users_lists.locator(".mx_InviteDialog_tile--room", has=self.page.locator(".mx_InviteDialog_tile_nameStack_name", has_text=f"{firstname} {lastname}")).first
    
    def find_user_by_username(self, username: str):
        return self.users_lists.locator(".mx_InviteDialog_tile--room", has=self.page.locator(".mx_InviteDialog_tile_nameStack_userId", has_text=username)).first

class UserMenu(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.homepage_button: Locator = self.locator("li[role=\"menuitem\"]", has=self.page.locator("span.mx_UserMenu_iconHome"))
        self.notifications_button: Locator = self.locator("li[role=\"menuitem\"]", has=self.page.locator("span.mx_UserMenu_iconBell"))
        self.security_button: Locator = self.locator("li[role=\"menuitem\"]", has=self.page.locator("span.mx_UserMenu_iconLock"))
        self.all_settings_button: Locator = self.locator("li[role=\"menuitem\"]", has=self.page.locator("span.mx_UserMenu_iconSettings"))
        self.logout_button: Locator = self.locator("li[role=\"menuitem\"]", has=self.page.locator("span.mx_UserMenu_iconSignOut"))
        
class UserSettingsDialog(StaticDialog):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.close_button: Locator = self.locator(".mx_AccessibleButton.mx_Dialog_cancelButton")
        self.secure_backup_buttons: Locator = self.locator(".mx_SecureBackupPanel_buttonRow")
        self.secure_backup_setup_button: Locator = self.secure_backup_buttons.locator(".mx_AccessibleButton_kind_primary_outline")
        self.secure_backup_delete_button: Locator = self.secure_backup_buttons.locator(".mx_AccessibleButton_kind_danger_outline")

        self.account_settings_button: Locator = self.locator("#mx_tabpanel_USER_ACCOUNT_TAB_label")
        self.account_settings_page: AccountSettingsPage = AccountSettingsPage(locator)

class ConfirmBackupDeletionDialog(Dialog):
    def __init__(self, locator):
        super().__init__(locator)
        self.confirm_deletion_button: Locator = self.locator(".danger")

class AccountSettingsPage(StaticDialog):

    def __init__(self,locator):
        super().__init__(locator)
        self.diplayname_settings: DisplaynameSettings = DisplaynameSettings(self.locator("form.mx_UserProfileSettings_profile_displayName"))
        self.matrix_id_label: Locator = self.locator(".mx_UserProfileSettings_profile_controls_userId > .mx_CopyableText")

class DisplaynameSettings(BasePagePart):
    def __init__(self, locator):
        super().__init__(locator)
        self.displayname_input: Locator = self.locator("input")
        self.accept_button: Locator = self.locator('[type="submit"]')


class CreateSecretDialog(StaticDialog):

    def __init__(self, locator):
        super().__init__(locator)
        self.container: Locator = self.locator(".mx_CreateSecretStorageDialog_primaryContainer")
        self.selected = self.container.locator("> lable.mx_StyledRadioButton_checked")
        self.option_key = self.container.locator("> label", has=self.page.locator("input[value=\"key\"]"))
        self.option_passphrase = self.container.locator("> label", has=self.page.locator("input[value=\"passphrase\"]"))
        self.recovery_key_dialog: RecoveryKeyDialog = RecoveryKeyDialog(self.locator(".mx_CreateSecretStorageDialog_primaryContainer"))

class SecretCreatedSuccessDialog(StaticDialog):
    
    def __init__(self, locator):
        super().__init__(locator)

class RecoveryKeyDialog(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.recovery_key_buttons: Locator = self.locator(".mx_CreateSecretStorageDialog_recoveryKeyButtons")
        self.download_key_button: Locator = self.recovery_key_buttons.locator("> .mx_Dialog_primary:not(.mx_CreateSecretStorageDialog_recoveryKeyButtons_copyBtn)")
        
class Toast(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.title: Locator = self.locator(".mx_Toast_title > h2")
        self.count_indicator: Locator = self.locator(".mx_Toast_title > .mx_Toast_title_countIndicator")
        self.body: Locator = self.locator(".mx_Toast_body")
        self.description: Locator = self.body.locator(".mx_Toast_description")
        self.buttons: Locator = self.body.locator(".mx_Toast_buttons")
        self.secondary_button: Locator = self.buttons.locator("[data-kind=secondary]")
        self.secondary_warning_button: Locator = self.buttons.locator("[data-kind=secondary][class*=_destructive]")
        self.primary_button: Locator = self.buttons.locator("[data-kind=primary]")

class ScheduleMeetingDialog(StaticDialog):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.meeting_frame: FrameLocator = self.frame_locator("> div > iframe")
        self.meeting_page: Page = self.meeting_frame.locator("html")
        self.container: Locator = self.meeting_frame.locator("#root > .MuiBox-root > .MuiStack-root")
        self.title_container: Locator = self.container.locator("> .MuiTextField-root").nth(0)
        self.title_input: Locator = self.title_container.locator("input")
        self.description_container: Locator = self.container.locator("> .MuiTextField-root").nth(1)
        self.description_input: Locator = self.description_container.locator("textarea:visible")
        
        self.create_meeting_button: Locator = self.locator(".mx_ModalWidgetDialog_buttons > .mx_AccessibleButton_kind_primary")

class MeetingRangePicker(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)

class MeetingEntry(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.header: Locator = self.locator(".MuiCardHeader-root")
        self.title: Locator = self.header.locator("> .MuiCardHeader-content > .MuiCardHeader-title")
        self.timeframe: Locator = self.header.locator("> .MuiCardHeader-content > .MuiCardHeader-subheader")
        self.actions: Locator = self.header.locator("> .MuiCardHeader-action")
        self.open_meeting_room_button: Locator = self.actions.locator(".MuiButtonBase-root.MuiIconButton-colorPrimary")
        self.settings_button: Locator = self.actions.locator(".MuiButtonBase-root:not(.MuiIconButton-colorPrimary)")

class MeetingScheduler(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.root: Locator = self.locator("#root")
        self.current_dialog: Locator = self.locator("body > .MuiPopper-root.MuiPickersPopper-root")
        self.navigation: Locator = self.locator("nav.MuiBox-root")
        
        self.schedule_button: Locator = self.navigation.locator("button.MuiButtonBase-root.MuiButton-containedPrimary")
        self.range_select_button: Locator = self.navigation.locator("button.MuiButtonBase-root.MuiButton-textPrimary")
        self.meeting_range_picker: MeetingRangePicker = MeetingRangePicker(self.current_dialog)
        
        self.meeting_board: Locator = self.locator("section", has=self.page.locator("> h3"))
        
        self.meeting_entry: Locator = self.meeting_board.locator("ul > li > ul > li")
    
    def get_meeting_entry(self, title: str) -> MeetingEntry:
        return MeetingEntry(self.meeting_entry.filter(has=self.page.locator(".MuiCardHeader-content > .MuiCardHeader-title", has_text=title)))

class ConferenceRoom(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        
        self.jitsi_frame: FrameLocator = self.frame_locator("#jitsiContainer > iframe")