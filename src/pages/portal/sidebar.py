# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

import re
from playwright.sync_api import Locator
from pages.base.base import BasePagePart

class Sidebar(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        
        self.change_language_button: Locator = self.locator(".portal-sidenavigation__menu > .portal-sidenavigation__menu-item", has_text=self._("change_language_button"))
        self.change_language_menu: LanguageMenu = LanguageMenu(self.change_language_button.locator("#portal-sidenavigation-sub"), name=self._("change_language_button"))
        self.privacy_statement_button: Locator = self.locator(".portal-sidenavigation__menu > .portal-sidenavigation__menu-item", has_text=self._("privacy_statement_button"))
        self.legal_notice_button: Locator = self.locator(".portal-sidenavigation__menu > .portal-sidenavigation__menu-item", has_text=self._("legal_notice_button"))

    def is_visible(self):
        return self.page_part_locator.is_visible()

class SidebarNotLoggedIn(Sidebar):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.login_button: Locator = self.locator("#loginButton")
        
class SidebarLoggedIn(Sidebar):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.user_settings_button: Locator = self.locator(".portal-sidenavigation__menu > .portal-sidenavigation__menu-item", has_text=self._("user_settings_button"))
        self.user_settings_menu: UserSettingsMenu = UserSettingsMenu(self.locator("#portal-sidenavigation-sub"), name=self._("user_settings_button"))
        
        self.logout_button: Locator = self.locator("#logoutButton")
    
class SidebarAdminLoggedIn(SidebarLoggedIn):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.user_settings_menu: AdminUserSettingsMenu = AdminUserSettingsMenu(self.locator("#portal-sidenavigation-sub"), name=self._("user_settings_button"))
        self.systeminformation_button: Locator = self.locator(".portal-sidenavigation__menu > .portal-sidenavigation__menu-item", has_text=self._("systeminformation_button"))
        self.systeminformation_menu: SysteminformationMenu = SysteminformationMenu(self.locator("#portal-sidenavigation-sub"), name=self._("systeminformation_button"))
        
        self.edit_portal_button: Locator = self.locator(".portal-sidenavigation__edit-mode[data-test=\"openEditmodeButton\"]")


class SidebarSubMenu(BasePagePart):
    
    def __init__(self, locator: Locator, name: str):
        super().__init__(locator.filter(has=locator.page.locator(".portal-sidenavigation__menu-subItem--parent").filter(has_text=name)))
        self.back_button: Locator = self.locator(".portal-sidenavigation__menu-subItem--parent")
    
    def close(self):
        self.back_button.click()

class LanguageMenu(SidebarSubMenu):
    
    def __init__(self, locator, name):
        super().__init__(locator, name)
        self.german_button: Locator = self.locator("#menu-item-language-de-DE")
        self.english_button: Locator = self.locator("#menu-item-language-en-US")

class AdminUserSettingsMenu(SidebarSubMenu):
    
    def __init__(self, locator, name):
        super().__init__(locator, name)
        self.update_password_button: Locator = self.locator(".portal-sidenavigation__menu-item--show", has_text=self._("update_password_button"))

class UserSettingsMenu(AdminUserSettingsMenu):
    
    def __init__(self, locator, name):
        super().__init__(locator, name)
        self.manage_profile_button: Locator = self.locator(".portal-sidenavigation__menu-item--show", has_text=self._("manage_profile_button"))
        self.recovery_options_button: Locator = self.locator(".portal-sidenavigation__menu-item--show", has_text=self._("recovery_options_button"))

class SysteminformationMenu(SidebarSubMenu):
    
    def __init__(self, locator, name):
        super().__init__(locator, name)
        self.release_button: Locator = self.locator(".portal-sidenavigation__menu-item--show", has_text=re.compile(self._("release_button_regex")))
        self.deployed_button: Locator = self.locator(".portal-sidenavigation__menu-item--show", has_text=re.compile(self._("deployed_button_regex")))

class EditModeSidebar(BasePagePart):
    def __init__(self, locator):
        super().__init__(locator)

        self.headline: Locator = self.locator(".edit-mode-side-navigation__headline")
        self.form: Locator = self.locator(".edit-mode-side-navigation__form")
        self.portal_logo_upload: Locator = self.form.locator("[data-test=\"imageUploadFileInput--Portal logo\"]")
        self.portal_logo_upload_button: Locator = self.form.locator("[data-test=\"imageUploadButton--Portal logo\"]")
        self.portal_logo_remove_button: Locator = self.form.locator("[data-test=\"imageRemoveButton--Portal logo\"]")
        self.name_input: Locator = self.form.locator("[data-test=\"localeInput--Name\"]")
        self.name_edit_button: Locator = self.form.locator("[data-test=\"iconButton--Name\"]")
        self.background_upload: Locator = self.form.locator("[data-test=\"imageUploadFileInput--Background\"]")
        self.background_upload_button: Locator = self.form.locator("[data-test=\"imageUploadButton--Background\"]")
        self.background_remove_button: Locator = self.form.locator("[data-test=\"imageRemoveButton--Background\"]")
        self.default_link_target_select: Locator = self.form.locator("[data-test=\"form-element-component\"]")
        self.ensure_login_checkbox: Locator = self.form.locator("[data-test=\"form-element-component\"]")
        self.show_umc_checkbox: Locator = self.form.locator("[data-test=\"form-element-component\"]")
        self.save_button: Locator = self.form.locator("[data-test=\"editModeSideNavigation--Save\"]")