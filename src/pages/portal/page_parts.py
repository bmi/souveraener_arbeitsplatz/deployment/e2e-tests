# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from re import Pattern
import re
from playwright.sync_api import Page, Locator
from pages.base.base import expect
from pages.base.base import BasePagePart

class Modal(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.dialog: Locator = self.locator("section.dialog")
        self.header: Locator = self.dialog.locator("header.dialog__header")
        self.sub_header: Locator = self.dialog.locator("div[id$=describedby]")
        self.title: Locator = self.header.locator("h3")
        self.close_button: Locator = self.header.locator("button.icon-button")

class QuickDraft(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.toggle_button: Locator = self.locator("> .portal-quick-draft__toggle")
        self.menu: Locator = self.locator("> .portal-quick-draft__menu")
        self.items: Locator = self.menu.locator("> ul.portal-quick-draft__items")

class CreateNewInFilesQuickDraft(QuickDraft):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.create_document_button: Locator = self.items.locator("a.portal-quick-draft__item[href$=\"/odt\"]")
        self.create_spreadsheet_button: Locator = self.items.locator("a.portal-quick-draft__item[href$=\"/ods\"]")
        self.create_presentation_button: Locator = self.items.locator("a.portal-quick-draft__item[href$=\"/odp\"]")

class Notification(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.title: Locator = self.locator(".notification__title")
        self.description: Locator = self.locator(".notification__description")
        self.close_button: Locator = self.locator(".notification__closing-button")

class SuccessNotification(Notification):
    def __init__(self, locator):
        super().__init__(locator)
    
    def get_profile_changed_notification(self):
        return self.page_part_locator.filter(has=self.page.locator(".notification__title", has_text=re.compile(self._("profile_changed_title_re"))))

class NewsfeedEntry(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.link: Locator = self.locator("> a")
        self.title: Locator = self.locator("h3.newsfeed-item__title")
        self.date: Locator = self.locator("time.newsfeed-item__date")
        self.excerpt: Locator = self.locator("div.newsfeed-item__excerpt")

class Newsfeed(BasePagePart):
    def __init__(self, locator):
        super().__init__(locator)
        self.view_all_button: Locator = self.locator(".newsfeed-meta__btn")
        self.no_content_message: Locator = self.locator("p", has_text=self._("newsfeed_no_content_message"))
        self.could_not_load_message: Locator = self.locator("p", has_text=self._("newsfeed_could_not_load_message"))
        
        self.newsfeed_list: Locator = self.locator("ul.newsfeed-list")
        self.newsfeed_item: Locator = self.newsfeed_list.locator("li.newsfeed-list__item")
    
    def get_newsfeed_entry(self, title: str | Pattern[str], content: str | Pattern[str] | None = None) -> NewsfeedEntry:
        locator: Locator = self.newsfeed_item.filter(has=self.page.locator("h3.newsfeed-item__title", has_text=title))
        if content:
            locator = locator.filter(has=self.page.locator("div.newsfeed-item__excerpt", has_text=content))
        return NewsfeedEntry(locator)

class AddEntryModal(Modal):
    
    def __init__(self, locator):
        super().__init__(locator)
        
        self.create_entry_button: Locator = self.locator("#tile-add-modal-button-create-entry")
        self.add_existing_entry_button: Locator = self.locator("#tile-add-modal-button-existing-entry")
        self.create_folder_button: Locator = self.locator("#tile-add-modal-button-create-folder")
        self.add_existing_folder_button: Locator = self.locator("#tile-add-modal-button-existing-folder")

class CreateNewEntryModal(Modal):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.internal_name_input: Locator = self.locator("input[name=name]")
        self.name_input: Locator = self.locator("input[name=title]")
        self.name_locale_button: Locator = self.locator("button[data-test=\"iconButton--Name\"]")
        self.description_input: Locator = self.locator("input[name=description]")
        self.description_locale_button: Locator = self.locator("button[data-test=\"iconButton--Description\"]")
        self.keywords_input: Locator = self.locator("input[name=keywords]")
        self.activated_checkbox: Locator = self.locator("input[name=activated]")
        self.links_input: Locator = self.locator("input[name=links]")
        self.links_locale_button: Locator = self.locator("button[data-test=\"iconButton--Links\"]")
        
        self.link_target_select: Locator = self.locator("select[name=linkTarget]")
        self.link_target_option_default: Locator = self.link_target_select.locator("option[value=\"useportaldefault\"]")
        self.link_target_option_same_window: Locator = self.link_target_select.locator("option[value=\"samewindow\"]")
        self.link_target_option_new_window: Locator = self.link_target_select.locator("option[value=\"newwindow\"]")
        self.link_target_option_embedded: Locator = self.link_target_select.locator("option[value=\"embedded\"]")
        
        self.icon_upload: Locator = self.locator("input.image-upload__file-input")
        self.background_color_input: Locator = self.locator("input[name=backgroundColor]")
        self.allowed_groups_multiselect: Locator = self.locator("div.multi-select")
        self.anonymous_checkbox: Locator = self.locator("input[name=anonymous]")
        self.cancel_button: Locator = self.locator("footer button[type=button]")
        self.save_button: Locator = self.locator("footer button.primary[type=submit]")

class TranslationModal(Modal):
    
    def __init__(self, locator):
        super().__init__(locator)
        
        self.input_en_us: Locator = self.locator("input[name=\"en_US\"]")
        self.input_de_de: Locator = self.locator("input[name=\"de_DE\"]")
        
        self.cancel_button: Locator = self.locator("footer button[type=button]")
        self.save_button: Locator = self.locator("footer button.primary[type=submit]")