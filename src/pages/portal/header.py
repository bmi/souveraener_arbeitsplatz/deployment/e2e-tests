# SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0

from playwright.sync_api import Locator
from pages.base.base import BasePagePart

class Header(BasePagePart):
    
    def __init__(self, locator):
        super().__init__(locator)
        self.portal_title: Locator = self.locator("#portalTitle")
        self.notifications_button: Locator = self.locator("#header-button-bell")
        self.sidebar_button: Locator = self.locator('#header-button-user')

class EditModeHeader(Header):
    
    def __init__(self, locator):
        super().__init__(locator)
        
        self.close_edit_mode_button: Locator = self.locator(".portal-header__edit-mode-label > button")
        self.sidebar_button: Locator = self.locator("#header-button-settings")