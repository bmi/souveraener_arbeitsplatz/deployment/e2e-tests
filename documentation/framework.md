<!---
SPDX-FileCopyrightText: 2024-2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->

# Introduction into the Test Framework

Although this repository is based on the pytest test-framework, we use a lot of custom fixtures, marks and decorators to make it faster to write tests. 
An example is the fixture `logged_in_page` that uses the mark *`user`* to create a logged-in context with the user account in the *`user`* mark that can be used for testing and avoids having to manually login for each testcase.
These fixtures and marks are being documented in this file, to improve the speed of writing tests.

## Behind the scenes

For future developers a quick introduction into what happens behind the scenes when starting the tests:

In [base.py](../tests/fixtures/base.py):
When first starting the tests the method `pytest_sessionstart` will be called. It makes sure the `tmp` folder to make sure a new Configuation is used for the current run.

The `_get_config` method returns the current configuration. It uses a file lock to make sure only one Config will be created. The first call will create a "config.bin.lock" file and create the Config object. The Config object will be saved in the "config.bin" file. The next calls will wait until the lock is released and then load the Config object from the file. This is to make sure mutliple threads can access the same Config.

After the session start, the method `pytest_collection_modifyitems` will be called. It will get the config and deselect tests from the current run, if:
- option "--deselect-env-tests" is set
- the test has a `env_setting` mark and was not selected by the environment settings

At the end of the session the method `pytest_sessionfinish` will be called. It will load the Config once more and execute the `cleanup` method of the Config. This will run the cleanup method of all settings.

## Users and Settings

### Settings

All settings used for the testcases are being defined in the `settings` list in the [settings.py](../../tests/config/settings.py) file.

Settings can be set via arguments in the command line or in a *config.ini* file. The path of the config file can be set with `--config=<config.ini path>`. 
- If set, it will load all values from the file. These values will be overwritten by values set via command arguments, if set.
- If not set, it will only use values given by the command arguments.

The `Setting` class contains all information about a setting.

To create a new `Setting` you need to set:
- `name`: This name will be used internally. You can access this settings value with `config.<name>`.
- `cli_name` (optional): If this is set, you can set this setting when starting the tests with the command line, by setting `<cli_name>=<value>` in the run arguments.
- `ini_section`, `ini_key` (optional): If both of these are set, it will read the value from the config file.
- `default` (optional): This will set the default value of this Setting. It can be any object or a callable that takes the `Config` as a positional parameter and returns any value.
If using the callable, be aware that only Settings from the list before the current item will be available.
- `choices` (optional): Sets a list of choices that can be picked from.
- `required` (optional): If true, raises an Exception when value is not set.
- `sanitize` (optional): It will take a callable that takes the Settings value as a positional parameter and returns a sanitized value. This can be used to parse a number or to normalize a path to a file.
- `cleanup` (optional): It will take a callable that takes the `Config` as a positional parameter. This will be run at the end of the testrun.

Example for a Setting for saving the path to a file:
```python
settings: list[Setting] = [
    ...,
    Setting(
        name="file_path",
        cli_name="--file-path",
        ini_section="Paths",
        ini_key="file_path",
        default="./file.txt",
        sanitize=lambda path: os.path.normpath(path),
        required=True
    )
]
```

### Environment Settings

Some settings can be different between deployments. Some settings change the behavior of the environment and should therefore be read out and checked before running specific tests, for example if external sharing is enabled or if 2FA is enforced for admin accounts.

These settings are being defined in the `environment_settings` list in the [settings.py](../../tests/config/settings.py) file.

Depending on the value of *config.profile* (*--profile* via cli, or [Environment]->profile in the config.ini) it will set the settings differently.
- Default: Will use the default value defined in each EnvironmentSetting.
- Custom: Will use the values from the cli or config file.
- Namespace: Will load the values from the namespace from the opendesk-env repository.

The `EnvironmentSetting` class contains all information about an environment setting.

To create a new `EnvironmentSetting` you need to set:
- `name`: This name will be used internally. You can access this settings value with *config.\<name>*. This is also the name used in the config file under the section "Environment".
- `cli_name`: If this is set, you can set this setting when starting the tests with the command line, by setting *\<cli_name>=\<value>* in the run arguments. This will only be used if the selected profile is custom.
- `default_value`: This will set the default value of this Setting.
- `env_path`: The path in the opendesk-env repository where the value can be found.
- `expected_value`: This can be a value or a callable. If it's a variable, it will compare both variables; if it's a callable, it will call it with the environment as a positional parameter and expects a boolean as the return value.

Example for an `EnvironmentSetting` that is true when the environment allows external sharing for Nextcloud:
```python
environment_settings: list[EnvironmentSetting] = [
    ...,
    EnvironmentSetting(
        name="env_external_sharing",
        cli_name="--env-external-sharing",
        default_value=True,
        env_path="functional.filestore.sharing.external.enabled",
        expected_value=True
    )
]
```

### Users
Before the tests start, the user importer will import a set of users that are being used for the testrun. The users will be stored in the `test_users` list in the [settings.py](../../tests/config/settings.py) file. When the Config gets created, it executes the `register_test_users` function, which populates the `test_users` list.

The *Config.User* class contains all information about a user. 
To create a new user you need to set:
- `key` (user-key): This will later be used to find that user. 
- `firstname` and `lastname`: Should contain some random number to ensure user can be found from just first- and lastname. 
- `username`: Should always end with the current run id (*config.run_id*), to ensure users from a testrun can be identfied when debugging.
- `email_domain`: Should to be the base_domain (*config.base_domain*), which will generate the full email address automatically.
- `password`: Can be static or randomly generated with *config.generate_password()*.
- `disable_components`: If user should not have access to any component (e.g. for guest users).
- `component_admin`: If user should have admin privileges for components.
- `enable_2fa`: Add user to "2FA Testing" group, which is forced to use 2fa.
- `skip_import`: If user should not be imported, for example to just have the data to create a user via the admin user interface.

After creating a user, you can copy it with .copy(key, username_suffix).
To create an admin user, you need to create a normal user, set the attribute `create_admin` to True and add the `user` and *user.to_admin()* to the `test_users` list.

Example adding a new user:
```python
def register_test_users(config: Config):
    ...
    test_users.append(
        Config.User(
            key="user",
            firstname="Test",
            lastname=f"User{random.randint(1000, 9999)}",
            username=f"tu{config.run_id}",
            email_domain=config.base_domain,
            password=config.generate_password()
        ))
```

## Fixtures and Marks

### Config values

Most values saved into the Config are also available as fixtures. Those usually have the same name as the field in the Config. It also creates a fixture for the Config itself, which is named `config` and fixtures for all users in the `test_users` list. These will have to be set manuallly. Implemented fixtures are:

- `config` -> Config
- `test_start_timestamp` -> datetime.datetime
- `autotest_mail` -> str
- `autotest_server` -> str
- `autotest_imap_port` -> str
- `autotest_smtp_port` -> str
- `autotest_password` -> str
- `autotest_element_username` -> str
- `demo_files_folder` -> str
- `run_id` -> str
- `locale` -> str
- `mailbox` -> imap_tools.MailBox (Created from autotest values)
- `smtp` -> smtplib.SMTP (Created from autotest values)

OpenDesk specific fixtures are:
- `url_portal` -> str
- `base_domain` -> str
- `user` -> Config.User
- `user_admin` -> Config.User
- `user_2fa` -> Config.User
- `guest` -> Config.User
- `component_admin` -> Config.User
- `new_user` -> Config.User
- `fresh_user` -> Config.User
- `pod_restart_thresholds` -> dict[str, int] (Reads csv file from Setting `pod_restart_threshold_file`)

OpenTalk specific fixtures are:
- `opentalk_url` -> str
- `opentalk_username` -> Config.User
- `opentalk_password` -> str
- `opentalk_firstname` -> str
- `opentalk_lastname` -> str
- `opentalk_email` -> str
- `opentalk_context` -> BrowserContext (Logins in user from `opentalk_username` and `opentalk_password` and returns the context)
- `opentalk_page` -> Page (Creates a page from the `opentalk_context`)

### `logged_in_context`, `logged_in_page` and *`user`* mark

When using the `logged_in_context` fixture it will read a string which is a user-key from the *`user`* mark and creates a new context where this user is logged in. It uses the [login steps](../../tests/steps/steps_login.py) that are also used within the testcases.
The `logged_in_page` fixture uses the `logged_in_context` and creates a new page from that context. After the test run, it will automatically close the page.

Usage:
```python
@pytest.mark.user("user")
def test_logged_in_page(logged_in_page: Page, url_portal: str):
    logged_in_page.goto(url_portal)
    assert logged_in_page.title() == get_logged_in_title()
```

### `logged_in_contexts`, `logged_in_pages` and *`users`* mark

Just like the `logged_in_context` it creates logged in contexts from the users. The *`users`* mark does not take the user-key but a list of user-keys to generate a list of contexts from in the same order as the user-keys.
The `logged_in_pages` fixtures creates a list of pages from the `logged_in_contexts`. After the test run, it will automatically close all pages.

Usage:
```python
@pytest.mark.users(["user", "admin"])
def test_logged_in_pages(logged_in_pages: List[Page], url_portal: str):
    for page in logged_in_pages:
        page.goto(url_portal)
        assert page.title() == get_logged_in_title()
```

### `temp_folder`

It creates a temporary folder that is being deleted after the test.

Usage:
```python
def test_temp_folder(temp_folder: str):
    assert os.path.exists(temp_folder)
```

### `temp_files` and *`files`* mark

The `temp_files` fixture creates a list of files from the `demo_files_folder` fixture (whos path is set in the settings) into the temporary folder created by the `temp_folder` fixture.

The *`files`* mark takes on a dictionary with the keys being the file names and the values being the amount of files that should be created. The key can be a:
- `str` (string), which will filter the demo files by checking if the file ends with that string.
- *re.Pattern*, which will filter the demo files by checking if the file matches the pattern.

It will return a tuple with temp folder path and the list of files created.

Usage:
```python
# Will create 3 files, 1 .odt file and 2 files that match regex "file\d\.txt"
@pytest.mark.files({".odt": 1, re.compile("file\d.txt"): 2})
def test_temp_files(temp_files: tuple[str, list[str]]):
    folder, files = temp_files
    assert len(files) == 3
    for file in files:
        assert os.path.exists(os.path.join(folder, file))
```

### `webdav_client`

It creates a webdav client that is connected to the Nextcloud instance. It uses the *`user`* mark to login with the user-key. It creates a new App Password for the user and uses that to login. Additionally, the webdav client can perform operations such as uploading, downloading, and deleting files on the Nextcloud server.

Usage (with combination of the `temp_files` fixture):
```python
@pytest.mark.user("user")
@pytest.mark.files({".odt": 1})
def test_webdav_client(webdav_client: WebDAVClient, temp_files: tuple[str, list[str]]):
    folder, files = temp_files
    for file in files:
        webdav_client.upload_file(remote_path=file, local_path=os.path.join(folder, file))
        assert webdav_client.check(file)
```

## `nextcloud_files`

This fixture uses the `temp_files` fixture to create temporary files and the `webdav_client` fixture to upload them to the Nextcloud instance. It will return the same value as the `temp_files` fixture. It will also delete the files on Nextcloud after the test run. Because it uses the `temp_files` fixture and the `webdav_client` fixture, you will have to set the `user` and the *`files`* mark.

Usage:
```python
@pytest.mark.user("user")
@pytest.mark.files({".odt": 1})
def test_nextcloud_files(logged_in_page: Page, url_portal: str, nextcloud_files: tuple[str, list[str]]):
    folder, files = nextcloud_files
    
    step_open_nextcloud(logged_in_page, url_portal)
    for file in files:
        assert step_check_file_exists(logged_in_page, file)
```

## *`users_dependency`* mark

When a subset of the tests, some users are not required and will therefore be skipped by the user importer. When a test requires a user that is not imported, it can use the *`users_dependency`* mark to import the user before the test run. The mark takes a list of user-keys that should be imported before the test run.

Usage:
```python
@pytest.mark.user("user")
@pytest.mark.users_dependency(["component_admin"])
def test_users_dependency(logged_in_page: Page, url_portal: str):
    # Test that requires the component_admin user, e.g. to chat with
```

## `temp_user` and *`temp_user`* mark

The `temp_user` fixture creates a temporary user that is being deleted after the test. The *`temp_user`* mark takes a dictionary with the keys being the user-keys and the values being the user attributes. The user attributes are the same as the `Config.User` class, but without the `key` attribute. No attributes are required. Default values are: `key=temp_user{RandomNumber}`, `firstname=Temp`, `lastname=User{RandomNumber}`, `username=tu{run_id}-{RandomNumber}`, `email_domain={config.base_domain}`, `password={GeneratedPassword}`.	

Usage:
```python
@pytest.mark.temp_user({"enable_2fa": True})
def test_temp_user(temp_user: Config.User):
    assert temp_user.enable_2fa
    # Temp User requires 2FA for login
```